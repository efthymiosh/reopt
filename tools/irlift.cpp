#include <dr_api.h>
#include <map>
#include <list>
#include <vector>
#include <algorithm>
#include <dr_ir_instr.h>
#include "../common/instr_funct.hh"
#include "../common/ExecutionContext.hh"

#define LIFTER_BITFILE "./binarydump.bin"
#define LIFTER_HEXFILE "./asciidump.hex"
#define MAX_X86_INSTR_SIZE 17

using ::std::list;
using ::std::pair;
using ::std::vector;

typedef struct tls_data {
    list<pair<byte*, byte *> *> *rawdata_list;
    int instr_count;
    file_t bin_f;
    file_t hex_f;
} Tls_data;

static dr_emit_flags_t on_bblock_create(void *, void *, instrlist_t *, bool, bool);
static void on_app_exit();
static void on_thread_init(void *);
static void on_thread_exit(void *);

static int ctr = 0;

static client_id_t my_id;
static void *dr_mutex;

/**
 * Clean Call added after every first instruction in each basic block.
 * Records the instruction lists' raw bits.
 */
void add_rawbits(void *drctx, pair<byte *, byte *> *data) {
    Tls_data *tlstruct = (Tls_data *) dr_get_tls_field(drctx);
    tlstruct->rawdata_list->push_back(data);
}

DR_EXPORT void dr_init(client_id_t client_id) {
    my_id = client_id;
    dr_mutex = dr_mutex_create();
    dr_register_bb_event(on_bblock_create);
    dr_register_exit_event(on_app_exit);
    dr_register_thread_init_event(on_thread_init);
    dr_register_thread_exit_event(on_thread_exit);
}

static void on_thread_init(void *drcontext) {
    char logname[512];
    int len;
    Tls_data *tlstruct = (Tls_data *) dr_thread_alloc(drcontext, sizeof(Tls_data));
    tlstruct->rawdata_list = new list<pair <byte *, byte *> *>();
    tlstruct->instr_count = 0;

    dr_mutex_lock(dr_mutex);
    if (ctr) {
        dr_mutex_unlock(dr_mutex);
        dr_fprintf(STDERR, "Error: This application is multi-threaded.. Aborting..\n");
        dr_abort();
    }
    ++ctr;
    dr_mutex_unlock(dr_mutex);

    /* Open binary output file */
    len = dr_snprintf(logname, sizeof(logname)/sizeof(logname[0]), LIFTER_BITFILE);
    DR_ASSERT(len > 0);
    tlstruct->bin_f = dr_open_file(logname, DR_FILE_WRITE_OVERWRITE);
    DR_ASSERT(tlstruct->bin_f != INVALID_FILE);

    /* Open ASCII output file */
    len = dr_snprintf(logname, sizeof(logname)/sizeof(logname[0]), LIFTER_HEXFILE);
    DR_ASSERT(len > 0);
    tlstruct->hex_f = dr_open_file(logname, DR_FILE_WRITE_OVERWRITE);
    DR_ASSERT(tlstruct->hex_f != INVALID_FILE);

    disassemble_set_syntax(DR_DISASM_ATT); //needed for output.
    /* store it in the slot provided in the drcontext */
    dr_set_tls_field(drcontext, (void *)tlstruct);
}

static void on_app_exit(void) {
}

static void on_thread_exit(void *drcontext) {
    list<pair<byte *, byte*> *>::iterator lpit;
    int i;
    dr_mutex_lock(dr_mutex);
    --ctr;
    dr_mutex_unlock(dr_mutex);
    Tls_data *tlstruct = (Tls_data *) dr_get_tls_field(drcontext);
    for (lpit = tlstruct->rawdata_list->begin(); lpit != tlstruct->rawdata_list->end(); lpit++) {
        /* convert the raw bits to hexadecimal numbers */
        byte *datPtr;
        unsigned char ch1, ch2;
        i = 0;

        for (datPtr = (*lpit)->first; datPtr < (*lpit)->second; datPtr++ , ++i) {
            ch1 = ((*datPtr) >> 4);
            ch1 += (ch1 > 9) ? 'a' - 10 : '0';
            ch2 = ((*datPtr) & 0xf);
            ch2 += (ch2 > 9) ? 'a' - 10 : '0';
            dr_fprintf(tlstruct->hex_f, "%c%c", ch1, ch2);
        }
        dr_fprintf(tlstruct->hex_f, "\n");
        dr_write_file(tlstruct->bin_f, (*lpit)->first, i);
    }
}

static dr_emit_flags_t on_bblock_create(void *drcontext, void *tag, instrlist_t *bb, bool for_trace, bool translating) {
    int amt_instr = 0;
    int bytearr_size;
    byte *raw_block, *end;
    instr_t *instr;
    instr_t *first;
    if (debug_instrument_ok(bb)) {
            //parameters: internal context, instruction list, next instruction, function,
            //save fp state boolean, amount of parameters, params ...
        instrlist_t *ilist = instrlist_clone(drcontext, bb);
        instrlist_remove(ilist, instrlist_last(ilist));
        for(instr = instrlist_first(ilist); instr != NULL; instr = instr_get_next(instr))
            ++amt_instr;
        raw_block = (byte *) malloc(amt_instr * MAX_X86_INSTR_SIZE * sizeof(byte));
        for(instr = instrlist_first(ilist); instr != NULL; instr = instr_get_next(instr))
            disassemble_with_info(dr_get_current_drcontext(), (byte*)instr_get_app_pc(instr), STDERR, false, false);
        if ((end = instrlist_encode_to_copy(drcontext, ilist, raw_block, instr_get_app_pc(instrlist_first(bb)), NULL, true)) == NULL) {
            dr_fprintf(STDERR, "Unable to encode basic block:\n");
        }
        pair<byte *, byte *> *pr = new pair<byte *, byte *>(raw_block, end);
        first = instrlist_first(bb);
        dr_insert_clean_call_ex(drcontext, bb, first, (void *) add_rawbits, DR_CLEANCALL_INDIRECT, 2,
                                OPND_CREATE_INTPTR(drcontext), OPND_CREATE_INTPTR(pr));
    }
    return DR_EMIT_DEFAULT;
}

