#include <dr_api.h>
#include <map>
#include <vector>
#include <algorithm>
#include "../common/instr_funct.hh"
#include "../common/ExecutionContext.hh"

using ::std::map;
using ::std::map;
using ::std::pair;
using ::std::vector;

typedef struct tls_counters {
    map<map<int, int> *, int> *counters;
    list<int> *syscalls_per;
    int instr_count;
    int amt_instr_ctr;
    int system_calls;
} Tls_data;

static dr_emit_flags_t on_bblock_create(void *, void *, instrlist_t *, bool, bool);
static void on_app_exit();
static void on_thread_init(void *);
static void on_thread_exit(void *);

static int ctr = 0;

static client_id_t my_id;
static void *dr_mutex;

bool pairComparator(pair<int, int> *p1, pair<int, int> *p2) {
    return p1->first > p2->first;
}

/**
 * Clean Call that is added after every first instruction in each basic block.
 */
void add_instr(void *drctx, int amt_instr, map<int, int> *observedSet) {

    /* Update Structures */
    Tls_data *tlstruct = (Tls_data *) dr_get_tls_field(drctx);
    (*tlstruct->counters)[observedSet] = (*tlstruct->counters)[observedSet] + 1;
    tlstruct->instr_count += amt_instr;
    
    tlstruct->amt_instr_ctr += amt_instr;
    tlstruct->system_calls += (*observedSet)[OP_syscall] +
                    (*observedSet)[OP_sysenter] +
                    (*observedSet)[OP_int];
    if (tlstruct->amt_instr_ctr > 1000) {
        tlstruct->amt_instr_ctr = 0;
        tlstruct->syscalls_per->push_back(tlstruct->system_calls);
        tlstruct->system_calls = 0;
    }
}

DR_EXPORT void dr_init(client_id_t client_id) {
    my_id = client_id;
    dr_mutex = dr_mutex_create();
    dr_register_bb_event(on_bblock_create);
    dr_register_exit_event(on_app_exit);
    dr_register_thread_init_event(on_thread_init);
    dr_register_thread_exit_event(on_thread_exit);
}

static void on_thread_init(void *drcontext) {
    char logname[512];
    int len;
    byte *apc;
    dr_syms_init(NULL);
    apc = drsym_lookup_symbol
    dr_syms_exit();
    Tls_data *tlstruct = (Tls_data *) dr_thread_alloc(drcontext, sizeof(Tls_data));
    tlstruct->counters = new map<map<int, int> *, int>();
    tlstruct->syscalls_per = new list<int>();
    tlstruct->instr_count = 0;
    dr_mutex_lock(dr_mutex);
    if (ctr) {
        dr_mutex_unlock(dr_mutex);
        dr_fprintf(STDERR, "Error: This application is multi-threaded.. Aborting..\n");
        dr_abort();
        /* UNREACHABLE */
        return;
    }
    ++ctr;
    dr_mutex_unlock(dr_mutex);
    disassemble_set_syntax(DR_DISASM_ATT); //needed for output.
    /* store it in the slot provided in the drcontext */
    dr_set_tls_field(drcontext, (void *)tlstruct);
}

static void on_app_exit(void) {
}

static void on_thread_exit(void *drcontext) {
    map<int, int>::iterator mit;
    map<map<int, int> *, int>::iterator mmit;
    map<int, int> *instrMap = new map<int, int>();

    dr_mutex_lock(dr_mutex);
    --ctr;
    dr_mutex_unlock(dr_mutex);
    Tls_data *tlstruct = (Tls_data *) dr_get_tls_field(drcontext);
    
    tlstruct->syscalls_per->push_back(tlstruct->system_calls);
    vector<pair<int, int>*> vec;
    for (mmit = tlstruct->counters->begin(); mmit != tlstruct->counters->end(); mmit ++) {
        map<int, int> *observedSet = mmit->first;
        int amt = mmit->second; //how many times the set had been observed.
        for (mit = observedSet->begin(); mit != observedSet->end(); mit++) {
            int key = mit->first, val = mit->second;
            (*instrMap)[key] = (*instrMap)[key] + val * amt;
        }
    }
    for(mit = instrMap->begin(); mit != instrMap->end(); mit++)
        vec.push_back(new pair<int, int>(mit->second, mit->first));
    std::sort(vec.begin(), vec.end(), pairComparator);
    vector<pair<int, int>*>::iterator vit = vec.begin();
    dr_printf("Instructions by order of times executed:\n");

    for ( ; vit != vec.end(); vit++)
        dr_printf("%15s: %5d\n", get_opcode_string((*vit)->second), (*vit)->first);
    dr_printf("Thread %d: executed %d discrete instructions.\n", dr_get_thread_id(drcontext), instrMap->size());
    dr_printf("Thread %d: executed %d instructions in total.\n", dr_get_thread_id(drcontext), tlstruct->instr_count);

    int per = tlstruct->instr_count >> 4;
    per = per - per % 1000 + 1000;
    int times = per / 1000;
    int prev = 0;
    int ctr = 0;
    int instr_cnt;
}

static dr_emit_flags_t on_bblock_create(void *drcontext, void *tag, instrlist_t *bb, bool for_trace, bool translating) {
    int amt_instr = 0;
    map <int, int> *instr_map = new map<int, int>();
    instr_t *instr;
    instr_t *first;
    for (first = instr = instrlist_first(bb); instr != NULL; instr = instr_get_next(instr)) {
        ++amt_instr;
        int opcode = instr_get_opcode(instr);
        (*instr_map)[opcode] = (*instr_map)[opcode] + 1;
    }
    //parameters: internal context, instruction list, next instruction, function,
    //save fp state boolean, amount of parameters, params ...
    dr_insert_clean_call_ex(drcontext, bb, first, (void *) add_instr, DR_CLEANCALL_INDIRECT, 3,
            OPND_CREATE_INTPTR(drcontext), OPND_CREATE_INT32(amt_instr),
            OPND_CREATE_INTPTR(instr_map));
    return DR_EMIT_DEFAULT;
}
