#!/bin/bash
tail -n+16 x86.csv | cut -f2-14 | while read ENTRY
do
    if [[ -z `echo $ENTRY | grep "[^ 	]"` ]] ; then
        continue
    fi
    PAPA=$ENTRY
    echo "${PAPA[*]}"
    PAPA=`echo ${PAPA[*]} | sed 's/\t/\txxx/g'`
    echo "${PAPA[*]}"
    ENTRY=$(echo "$ENTRY" | tr ' \t' '\t ' | sed 's/\ /\ xxx/g')
    ENTRY=($ENTRY)
    NAME=${ENTRY[0]}
    OPERANDS=${ENTRY[1]}
    PROPERTIES=${ENTRY[2]}
    IMPLICIT_READ=${ENTRY[3]}
    IMPLICIT_WRITE=${ENTRY[4]}
    IMPLICIT_UNDEF=${ENTRY[5]}
    USEFUL=${ENTRY[6]}
    PROTECTED=${ENTRY[7]}
    MOD64=${ENTRY[8]}
    MOD32=${ENTRY[9]}
    CPUIDFLAGS=${ENTRY[10]}
    ATTMNEMONIC=${ENTRY[11]}
    PREFERRED=${ENTRY[12]}
    DESCRIPTION=${ENTRY[13]}
    echo "OPERANDS $OPERANDS"
    echo "DESCRIPTION $DESCRIPTION"
    sleep 1
done

