#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parser.h"
#include "instruction.h"

void handle(Instruction *instr, FILE *fout) {
    int i;
    for (i = 0; instr->name[i] != '\0'; ++i)
        instr->name[i] += 'a' - 'A';
    if (instr->description) {
        fprintf(fout, "/* %s */\n", instr->description);
    }
    fprintf(fout, "void fn_%s(ExecutionContext *ectx, instr_t *instr, fn_data *params) {\n" , instr->name);
}

int main(void) {
    FILE *fp = fopen("x86.csv", "r");
    char *line = NULL;
    size_t i = 0;
    Instruction instr;
    if (fp == NULL)
        fprintf(stderr, "Unable to open file\n");
    getline(&line, &i, fp); /* consumer first line, it's just the column identifiers */
    free(line);
    while ((line = parse_line(fp, &instr)) != NULL) {
        if (instr.name == NULL)
            continue;
        handle(&instr, stdout);
        getchar();
        free(line);
    }
    return EXIT_SUCCESS;
}
