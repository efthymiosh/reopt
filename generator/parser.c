#include "parser.h"
#include "instruction.h"

char *my_tokenizer(char *str, char *delim, char **saveptr) {
    char *retval;
    int i, j;
    if (!str) {
        if (!(*saveptr))
            return NULL;
        retval = *saveptr;
    }
    else {
        *saveptr = str;
        retval = str;
    }
    for (i = 0, j = 0; (*saveptr)[i] != '\0' ; ++i) {
        if ((*saveptr)[i] == delim[j]) {
            ++j;
            if (delim[j] == '\0') {
                (*saveptr)[i - j + 1] = '\0';
                *saveptr = *saveptr + i + 1;
                return retval;
            }
        }
        else
            j = ((*saveptr)[i] == delim[0]);
    }
    *saveptr = NULL;
    return retval;
}

char *parse_line(FILE *fin, Instruction *instr) {
    char *init, *textualinstr, *op1, *op2, *opencoding, *properties, *implicit, *useful;
    char *protected, *mod64, *mod32, *cpuidflags, *attmnemonic, *preferred, *description;
    char *saveptr, *saveptr_lvl2;
    size_t n = 0;
    char *lineptr = NULL;
    instr->name = NULL;
    instr->attname = NULL;
    description = NULL;
    if ((getline(&lineptr, &n, fin)) != -1) {
        lineptr[strlen(lineptr) - 1] = '\0';
        my_tokenizer(lineptr, "\t", &saveptr); /* consume opcode */
        textualinstr = my_tokenizer(NULL, "\t", &saveptr);
        if (*textualinstr == '\0'  || textualinstr == NULL)
            return lineptr;
        instr->name = my_tokenizer(textualinstr, " ", &saveptr_lvl2);
        op1 = my_tokenizer(NULL, ", ", &saveptr_lvl2);
        if (op1 == NULL || *op1 == '\0') {
            instr->op1 = op_nil;
        }
        else {
            /* parse operand string here */
            
            op2 = my_tokenizer(NULL, ", ", &saveptr_lvl2);
            if (op2 == NULL || *op2 == '\0') {
                instr->op2 = op_nil;
            }
            else {
                /*parse operand2 string here */
            }
        }
        opencoding = my_tokenizer(NULL, "\t", &saveptr);
        properties = my_tokenizer(NULL, "\t", &saveptr);
        if (*properties != '\0') {
            char *op1_prop, *op2_prop;
            op1_prop = my_tokenizer(properties, ", ", &saveptr_lvl2);
            op2_prop = my_tokenizer(NULL, ", ", &saveptr_lvl2);
            if (op1_prop != NULL && *op1_prop != '\0') {
                if (!strcmp(op1_prop, "R"))
                    instr->op1_prop = prop_mustbe_r;
                else if (!strcmp(op1_prop, "W"))
                    instr->op1_prop = prop_mustbe_w;
                else if (!strcmp(op1_prop, "RW"))
                    instr->op1_prop = prop_mustbe_rw;
                else if (!strcmp(op1_prop, "r"))
                    instr->op1_prop = prop_maybe_r;
                else if (!strcmp(op1_prop, "w"))
                    instr->op1_prop = prop_maybe_w;
                else if (!strcmp(op1_prop, "rw"))
                    instr->op1_prop = prop_maybe_rw;
                else if (!strcmp(op1_prop, "Z"))
                    instr->op1_prop = prop_extension_cleared;
                else if (!strcmp(op1_prop, "z"))
                    instr->op1_prop = prop_extension_cleared;
                else
                    fprintf(stderr, "Unknown property symbol: %s\n", op1_prop);
                op2_prop = my_tokenizer(NULL, ", ", &saveptr_lvl2);
                if (op2_prop != NULL && *op2_prop != '\0') {
                    if (!strcmp(op2_prop, "R"))
                        instr->op2_prop = prop_mustbe_r;
                    else if (!strcmp(op2_prop, "W"))
                        instr->op2_prop = prop_mustbe_w;
                    else if (!strcmp(op2_prop, "RW"))
                        instr->op2_prop = prop_mustbe_rw;
                    else if (!strcmp(op2_prop, "r"))
                        instr->op2_prop = prop_maybe_r;
                    else if (!strcmp(op2_prop, "w"))
                        instr->op2_prop = prop_maybe_w;
                    else if (!strcmp(op2_prop, "rw"))
                        instr->op2_prop = prop_maybe_rw;
                    else if (!strcmp(op2_prop, "z"))
                        instr->op2_prop = prop_extension_cleared;
                    else
                        fprintf(stderr, "Unknown property symbol: %s\n", op1_prop);
                }
            }
        }
        /* i == 0: implicit read, i == 1 implicit write, i == 2 implicit undefined */
        for (int i = 0; i < 3; i++) {
            int pos[2];
            pos[MUST] = 0;
            pos[MAY] = 0;
            implicit = my_tokenizer(NULL, "\t", &saveptr);
            implicit = my_tokenizer(implicit, " ", &saveptr_lvl2);
            if (implicit != NULL && *implicit != '\0') {
                do {
                    if (pos[MAY] == 8 || pos[MUST] == 8) {
                        fprintf(stderr, "Instruction has more implicit operands than supported\n");
                        break;
                    }
                    if (!strncasecmp(implicit, "E.", 2) && !strcasecmp(implicit + 3, "F")) {
                        switch (implicit[2]) {
                            case 'O': instr->impl_op[MUST][i][pos[MUST]++] = rf_E_OF; break;
                            case 'o': instr->impl_op[MAY][i][pos[MAY]++] = rf_E_OF; break;
                            case 'S': instr->impl_op[MUST][i][pos[MUST]++] = rf_E_SF; break;
                            case 's': instr->impl_op[MAY][i][pos[MAY]++] = rf_E_SF; break;
                            case 'Z': instr->impl_op[MUST][i][pos[MUST]++] = rf_E_ZF; break;
                            case 'z': instr->impl_op[MAY][i][pos[MAY]++] = rf_E_ZF; break;
                            case 'A': instr->impl_op[MUST][i][pos[MUST]++] = rf_E_AF; break;
                            case 'a': instr->impl_op[MAY][i][pos[MAY]++] = rf_E_AF; break;
                            case 'C': instr->impl_op[MUST][i][pos[MUST]++] = rf_E_CF; break;
                            case 'c': instr->impl_op[MAY][i][pos[MAY]++] = rf_E_CF; break;
                            case 'P': instr->impl_op[MUST][i][pos[MUST]++] = rf_E_PF; break;
                            case 'p': instr->impl_op[MAY][i][pos[MAY]++] = rf_E_PF; break;
                            default: fprintf(stderr, "Unhandled flag: %s\n", implicit);
                        }
                    }
                    else if (implicit[0] == 'R' || implicit[0] == 'r') {
                        if (!strcasecmp(implicit + 2, "X")) {
                            switch (implicit[1]) {
                                case 'A': instr->impl_op[MUST][i][pos[MUST]++] = rf_RAX; break;
                                case 'a': instr->impl_op[MAY][i][pos[MAY]++] = rf_RAX; break;
                                case 'B': instr->impl_op[MUST][i][pos[MUST]++] = rf_RBX; break;
                                case 'b': instr->impl_op[MAY][i][pos[MAY]++] = rf_RBX; break;
                                case 'C': instr->impl_op[MUST][i][pos[MUST]++] = rf_RCX; break;
                                case 'c': instr->impl_op[MAY][i][pos[MAY]++] = rf_RCX; break;
                                case 'D': instr->impl_op[MUST][i][pos[MUST]++] = rf_RDX; break;
                                case 'd': instr->impl_op[MAY][i][pos[MAY]++] = rf_RDX; break;
                            }
                        }
                        else if(!strcmp(implicit + 1, "SI")) {
                            instr->impl_op[MUST][i][pos[MUST]++] = rf_RSI;
                        }
                        else if(!strcmp(implicit + 1, "si")) {
                            instr->impl_op[MAY][i][pos[MAY]++] = rf_RSI;
                        }
                        else if(!strcmp(implicit + 1, "DI")) {
                            instr->impl_op[MUST][i][pos[MUST]++] = rf_RDI;
                        }
                        else if(!strcmp(implicit + 1, "di")) {
                            instr->impl_op[MAY][i][pos[MAY]++] = rf_RDI;
                        }
                        else if(!strcmp(implicit + 1, "SP")) {
                            instr->impl_op[MUST][i][pos[MUST]++] = rf_RSP;
                        }
                        else if(!strcmp(implicit + 1, "sp")) {
                            instr->impl_op[MAY][i][pos[MAY]++] = rf_RSP;
                        }
                        else if(!strcmp(implicit + 1, "BP")) {
                            instr->impl_op[MUST][i][pos[MUST]++] = rf_RBP;
                        }
                        else if(!strcmp(implicit + 1, "bp")) {
                            instr->impl_op[MAY][i][pos[MAY]++] = rf_RBP;
                        }
                        else if (implicit[2] == '\0') {
                            switch (implicit[1]) {
                                case '8': instr->impl_op[implicit[0] == 'R' ? MUST : MAY][i][pos[implicit[0] == 'R' ? MUST : MAY]++] = rf_R8; break;
                                case '9': instr->impl_op[implicit[0] == 'R' ? MUST : MAY][i][pos[implicit[0] == 'R' ? MUST : MAY]++] = rf_R9; break;
                                default: fprintf(stderr, "Unsupported implicit operand: %s\n", implicit);
                                         break;
                            }
                        }
                        else if (implicit[3] == '\0' && implicit[1] == '1') {
                            switch (implicit[2]) {
                                case '0': instr->impl_op[implicit[0] == 'R' ? MUST : MAY][i][pos[implicit[0] == 'R' ? MUST : MAY]++] = rf_R10; break;
                                case '1': instr->impl_op[implicit[0] == 'R' ? MUST : MAY][i][pos[implicit[0] == 'R' ? MUST : MAY]++] = rf_R11; break;
                                case '2': instr->impl_op[implicit[0] == 'R' ? MUST : MAY][i][pos[implicit[0] == 'R' ? MUST : MAY]++] = rf_R12; break;
                                case '3': instr->impl_op[implicit[0] == 'R' ? MUST : MAY][i][pos[implicit[0] == 'R' ? MUST : MAY]++] = rf_R13; break;
                                case '4': instr->impl_op[implicit[0] == 'R' ? MUST : MAY][i][pos[implicit[0] == 'R' ? MUST : MAY]++] = rf_R14; break;
                                case '5': instr->impl_op[implicit[0] == 'R' ? MUST : MAY][i][pos[implicit[0] == 'R' ? MUST : MAY]++] = rf_R15; break;
                            }
                        }
                        else
                            fprintf(stderr, "Unsupported implicit operand: %s\n", implicit);
                    }
                    else if (strncasecmp(implicit, "XMM", 3)) {
                        if (implicit[4] == '\0') {
                            switch (implicit[3]) {
                                case '1': instr->impl_op[implicit[0] == 'X' ? MUST : MAY][i][pos[implicit[0] == 'X' ? MUST : MAY]++] = rf_XMM1; break;
                                case '2': instr->impl_op[implicit[0] == 'X' ? MUST : MAY][i][pos[implicit[0] == 'X' ? MUST : MAY]++] = rf_XMM2; break;
                                case '3': instr->impl_op[implicit[0] == 'X' ? MUST : MAY][i][pos[implicit[0] == 'X' ? MUST : MAY]++] = rf_XMM3; break;
                                case '4': instr->impl_op[implicit[0] == 'X' ? MUST : MAY][i][pos[implicit[0] == 'X' ? MUST : MAY]++] = rf_XMM4; break;
                                case '5': instr->impl_op[implicit[0] == 'X' ? MUST : MAY][i][pos[implicit[0] == 'X' ? MUST : MAY]++] = rf_XMM5; break;
                                case '6': instr->impl_op[implicit[0] == 'X' ? MUST : MAY][i][pos[implicit[0] == 'X' ? MUST : MAY]++] = rf_XMM6; break;
                                case '7': instr->impl_op[implicit[0] == 'X' ? MUST : MAY][i][pos[implicit[0] == 'X' ? MUST : MAY]++] = rf_XMM7; break;
                                case '8': instr->impl_op[implicit[0] == 'X' ? MUST : MAY][i][pos[implicit[0] == 'X' ? MUST : MAY]++] = rf_XMM8; break;
                                case '9': instr->impl_op[implicit[0] == 'X' ? MUST : MAY][i][pos[implicit[0] == 'X' ? MUST : MAY]++] = rf_XMM9; break;
                                default: fprintf(stderr, "Unsupported implicit operand: %s\n", implicit);
                                         break;
                            }
                        }
                        else if (implicit[5] == '\0' && implicit[3] == '\1') {
                            switch (implicit[4]) {
                                case '0': instr->impl_op[implicit[0] == 'X' ? MUST : MAY][i][pos[implicit[0] == 'X' ? MUST : MAY]++] = rf_XMM10; break;
                                case '1': instr->impl_op[implicit[0] == 'X' ? MUST : MAY][i][pos[implicit[0] == 'X' ? MUST : MAY]++] = rf_XMM11; break;
                                case '2': instr->impl_op[implicit[0] == 'X' ? MUST : MAY][i][pos[implicit[0] == 'X' ? MUST : MAY]++] = rf_XMM12; break;
                                case '3': instr->impl_op[implicit[0] == 'X' ? MUST : MAY][i][pos[implicit[0] == 'X' ? MUST : MAY]++] = rf_XMM13; break;
                                case '4': instr->impl_op[implicit[0] == 'X' ? MUST : MAY][i][pos[implicit[0] == 'X' ? MUST : MAY]++] = rf_XMM14; break;
                                case '5': instr->impl_op[implicit[0] == 'X' ? MUST : MAY][i][pos[implicit[0] == 'X' ? MUST : MAY]++] = rf_XMM15; break;
                            }
                        }
                        else
                            fprintf(stderr, "Unsupported implicit operand: %s\n", implicit);
                    }
                    else if (strncasecmp(implicit, "YMM", 3)) {
                        if (implicit[4] == '\0') {
                            switch (implicit[3]) {
                                case '1': instr->impl_op[implicit[0] == 'Y' ? MUST : MAY][i][pos[implicit[0] == 'Y' ? MUST : MAY]++] = rf_YMM1; break;
                                case '2': instr->impl_op[implicit[0] == 'Y' ? MUST : MAY][i][pos[implicit[0] == 'Y' ? MUST : MAY]++] = rf_YMM2; break;
                                case '3': instr->impl_op[implicit[0] == 'Y' ? MUST : MAY][i][pos[implicit[0] == 'Y' ? MUST : MAY]++] = rf_YMM3; break;
                                case '4': instr->impl_op[implicit[0] == 'Y' ? MUST : MAY][i][pos[implicit[0] == 'Y' ? MUST : MAY]++] = rf_YMM4; break;
                                case '5': instr->impl_op[implicit[0] == 'Y' ? MUST : MAY][i][pos[implicit[0] == 'Y' ? MUST : MAY]++] = rf_YMM5; break;
                                case '6': instr->impl_op[implicit[0] == 'Y' ? MUST : MAY][i][pos[implicit[0] == 'Y' ? MUST : MAY]++] = rf_YMM6; break;
                                case '7': instr->impl_op[implicit[0] == 'Y' ? MUST : MAY][i][pos[implicit[0] == 'Y' ? MUST : MAY]++] = rf_YMM7; break;
                                case '8': instr->impl_op[implicit[0] == 'Y' ? MUST : MAY][i][pos[implicit[0] == 'Y' ? MUST : MAY]++] = rf_YMM8; break;
                                case '9': instr->impl_op[implicit[0] == 'Y' ? MUST : MAY][i][pos[implicit[0] == 'Y' ? MUST : MAY]++] = rf_YMM9; break;
                                default: fprintf(stderr, "Unsupported implicit operand: %s\n", implicit);
                                         break;
                            }
                        }
                        else if (implicit[5] == '\0' && implicit[3] == '\1') {
                            switch (implicit[4]) {
                                case '0': instr->impl_op[implicit[0] == 'Y' ? MUST : MAY][i][pos[implicit[0] == 'Y' ? MUST : MAY]++] = rf_YMM10; break;
                                case '1': instr->impl_op[implicit[0] == 'Y' ? MUST : MAY][i][pos[implicit[0] == 'Y' ? MUST : MAY]++] = rf_YMM11; break;
                                case '2': instr->impl_op[implicit[0] == 'Y' ? MUST : MAY][i][pos[implicit[0] == 'Y' ? MUST : MAY]++] = rf_YMM12; break;
                                case '3': instr->impl_op[implicit[0] == 'Y' ? MUST : MAY][i][pos[implicit[0] == 'Y' ? MUST : MAY]++] = rf_YMM13; break;
                                case '4': instr->impl_op[implicit[0] == 'Y' ? MUST : MAY][i][pos[implicit[0] == 'Y' ? MUST : MAY]++] = rf_YMM14; break;
                                case '5': instr->impl_op[implicit[0] == 'Y' ? MUST : MAY][i][pos[implicit[0] == 'Y' ? MUST : MAY]++] = rf_YMM15; break;
                            }
                        }
                        else
                            fprintf(stderr, "Unsupported implicit operand: %s\n", implicit);
                    }
                    else
                        fprintf(stderr, "Unsupported implicit operand: %s\n", implicit);
                } while ((implicit = my_tokenizer(NULL, " ", &saveptr_lvl2)) != NULL);
            }
            if (pos[MUST] != 8)
                instr->impl_op[MUST][i][pos[MUST]] = rf_nil;
            if (pos[MAY] != 8)
                instr->impl_op[MAY][i][pos[MAY]] = rf_nil;
        }
        useful = my_tokenizer(NULL, "\t", &saveptr);
        protected = my_tokenizer(NULL, "\t", &saveptr);
        mod64 = my_tokenizer(NULL, "\t", &saveptr);
        mod32 = my_tokenizer(NULL, "\t", &saveptr);
        cpuidflags = my_tokenizer(NULL, "\t", &saveptr);
        instr->attname = my_tokenizer(NULL, "\t", &saveptr);
        preferred = my_tokenizer(NULL, "\t", &saveptr);
        instr->description = my_tokenizer(NULL, "\t", &saveptr);
        return lineptr;
    }
    return NULL;
}
