#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>

char *my_tokenizer(char *str, char *delim, char **saveptr) {
    char *retval;
    int i, j;
    if (!str) {
        if (!(*saveptr))
            return NULL;
        retval = *saveptr;
    }
    else {
        *saveptr = str;
        retval = str;
    }
    for (i = 0, j = 0; (*saveptr)[i] != '\0' ; ++i) {
        if ((*saveptr)[i] == delim[j]) {
            ++j;
            if (delim[j] == '\0') {
                (*saveptr)[i - j + 1] = '\0';
                *saveptr = *saveptr + i + 1;
                return retval;
            }
        }
        else
            j = ((*saveptr)[i] == delim[0]);
    }
    *saveptr = NULL;
    return retval;
}

int main(void) {
    char *lineptr;
    char *saveptr;
    char *token;
    size_t n = 0;
    while (getline(&lineptr, &n, stdin)) {
        token = my_tokenizer(lineptr, "|||", &saveptr);
        printf("First token: %s\n", token);
        while ((token = my_tokenizer(NULL, "|||", &saveptr)) != NULL) {
            printf("Token: \"%s\"\n", token);
        }
        n = 0;
        lineptr = NULL;
    }
    return 0;
}
