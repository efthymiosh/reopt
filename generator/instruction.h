#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#define MUST 0
#define MAY 1

enum operand {
    op_nil,
    op_mem,
    op_reg,
    op_memreg,
    op_AL,
    op_AX,
    op_EAX,
    op_RAX,
    op_CL,
    op_DX,
    op_XMM0
};

enum properties {
    prop_mustbe_r,
    prop_mustbe_w,
    prop_mustbe_rw,
    prop_maybe_r,
    prop_maybe_w,
    prop_maybe_rw,
    prop_extension_cleared
};

enum regflags {
    rf_nil,
    rf_E_OF, rf_E_SF, rf_E_ZF, rf_E_AF, rf_E_CF, rf_E_PF,
    rf_RAX, rf_RDX, rf_RCX, rf_RBX, rf_RDI, rf_RSI, rf_RBP, rf_RSP,
    rf_R8, rf_R9, rf_R10, rf_R11, rf_R12, rf_R13, rf_R14, rf_R15,
    rf_XMM0, rf_XMM1, rf_XMM2, rf_XMM3, rf_XMM4, rf_XMM5, rf_XMM6, rf_XMM7,
    rf_XMM8, rf_XMM9, rf_XMM10, rf_XMM11, rf_XMM12, rf_XMM13, rf_XMM14, rf_XMM15,
    rf_YMM0, rf_YMM1, rf_YMM2, rf_YMM3, rf_YMM4, rf_YMM5, rf_YMM6, rf_YMM7,
    rf_YMM8, rf_YMM9, rf_YMM10, rf_YMM11, rf_YMM12, rf_YMM13, rf_YMM14, rf_YMM15
};

typedef struct Instruction {
    char *name;
    char *attname;
    enum operand op1;
    enum operand op2;
    int opsize;
    enum properties op1_prop;
    enum properties op2_prop;
    enum regflags impl_op[2][3][8];
    char *description;
} Instruction;

#endif
