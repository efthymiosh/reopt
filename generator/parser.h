#ifndef PARSER_H
#define PARSER_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "instruction.h"

char *parse_line(FILE *fin, Instruction *instr);

#endif
