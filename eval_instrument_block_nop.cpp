#include <stdint.h>
#include <string.h>
#include <limits.h>

#include "./common/instr_funct.hh"
#include "./common/sym_tree/sym_tree.hh"
#include "./common/ExecutionContext.hh"

#define INSTRUMENT_OFF 0
#define INSTRUMENT_ON 1

static dr_emit_flags_t on_bblock_create(void *, void *, instrlist_t *, bool, bool);
static void on_app_exit();
static void on_thread_init(void *);
static void on_thread_exit(void *);

static client_id_t my_id;
static void *dr_mutex;
static int ctr = 0;

/**
 * Clean Call that is added at the beginning of every basic block. After a set amount of blocks,
 * it starts iterating over the instructions in blocks and deduces the initial symbols for symbolic execution.
 */
static int global_stop_ctr = 1;
void process_instr_nop(void *drctx, instr_t *instr) {
    static int status = INSTRUMENT_OFF;
    static int stopctr = 0;
    ExecutionContext *ectx;
    switch (status) {
        case INSTRUMENT_OFF:
            if ((instr_get_opcode(instr) == OP_nop_modrm)
                    && (opnd_get_addr(instr_get_src(instr, 0)) == (void *) 0x11111111)) {
                status = INSTRUMENT_ON;
                return;
            }
            return;
        case INSTRUMENT_ON:
            if (instr_get_opcode(instr) == OP_nop_modrm) {
                if (opnd_get_addr(instr_get_src(instr, 0)) == (void *) 0x22222222) {
                    status = INSTRUMENT_OFF;
                    ectx = (ExecutionContext *) dr_get_tls_field(drctx);
                    ectx->enabled = false;
                    return;
                }
                else if (opnd_get_addr(instr_get_src(instr, 0)) == (void *) 0x11111111) {
                    if (++stopctr == global_stop_ctr) {
                        status = INSTRUMENT_OFF;
                        ectx = (ExecutionContext *) dr_get_tls_field(drctx);
                        ectx->enabled = false;
                        on_thread_exit(drctx);
                        dr_abort();
                    }
                    return;
                }
            }
            break;
    }
    /* normal operation: */
    ectx = (ExecutionContext *) dr_get_tls_field(drctx);
    ectx->printInstruction(drctx, instr);
    fn_common(ectx, instr);
}

static int process_instr_cnt_max = INT_MAX;
static int process_instr_cnt_min = 0;
void process_block(void *drctx, instrlist_t *bb) {
    static int cnt = 0;
    instr_t *instr;
    ExecutionContext *ectx;
    ectx = (ExecutionContext *) dr_get_tls_field(drctx);
    /* normal operation: */
    for (instr = instrlist_first(bb); instr != NULL; instr = instr_get_next(instr))
        process_instr_nop(drctx, instr);
}



DR_EXPORT void dr_init(client_id_t client_id) {
    my_id = client_id;
    dr_mutex = dr_mutex_create();
    dr_register_bb_event(on_bblock_create);
    dr_register_exit_event(on_app_exit);
    dr_register_thread_init_event(on_thread_init);
    dr_register_thread_exit_event(on_thread_exit);
}

static void on_thread_init(void *drcontext) {
    ExecutionContext *ectx;
    char logname[512];
    int len;
    dr_mutex_lock(dr_mutex);
    if (ctr) {
        dr_mutex_unlock(dr_mutex);
        dr_fprintf(STDERR, "Symbolic Evaluation init error: This application is multi-threaded.. Aborting..\n");
        dr_abort();
        /* UNREACHABLE */
        return;
    }
    ++ctr;
    dr_mutex_unlock(dr_mutex);
    const char *constopt = dr_get_options(my_id);
    char *options = (char *) malloc(strlen(constopt) + 1);
    strcpy(options, constopt);
    if (options[0] != '\0') {
        char *ctr;
        ctr = strtok(options, " ");
        global_stop_ctr = atoi(ctr);
    }
//    ectx = (ExecutionContext *) dr_thread_alloc(drcontext, sizeof(ExecutionContext));
    ectx = new ExecutionContext();
    ectx->initObject();
    disassemble_set_syntax(DR_DISASM_ATT); //needed for output.
    /* store it in the slot provided in the drcontext */
    dr_set_tls_field(drcontext, (void *)(ptr_uint_t)ectx);
}

static void on_app_exit(void) {
}

static void on_thread_exit(void *drcontext) {
    ExecutionContext *ectx = (ExecutionContext *)(ptr_uint_t) dr_get_tls_field(drcontext);

    ectx->calculate_alias_stats();
#ifdef DEBUG_GRAPH
    ectx->printGraph();
#endif
#ifdef DEBUG_SYM
    ectx->printExpressions();
#endif
    ectx->calcFinalTrace(drcontext);
    ectx->finiObject();
    dr_thread_free(drcontext, ectx, sizeof(ExecutionContext));
    dr_mutex_lock(dr_mutex);
    --ctr;
    dr_mutex_unlock(dr_mutex);
}



static dr_emit_flags_t on_bblock_create(void *drcontext, void *tag, instrlist_t *bb, bool for_trace, bool translating) {
    ExecutionContext *ectx = (ExecutionContext *) dr_get_tls_field(drcontext);
    if (!ectx->enabled)
        return DR_EMIT_DEFAULT;
    instrlist_t *bb_clone;
    bb_clone = instrlist_clone(drcontext, bb);
    //parameters: internal context, instruction list, next instruction, function,
    //save fp state boolean, amount of parameters, params ...
    instr_t *firstinstr = instrlist_first(bb);
    dr_insert_clean_call_ex(drcontext, bb, firstinstr, (void *) process_block, DR_CLEANCALL_INDIRECT, 2,
            OPND_CREATE_INTPTR(drcontext), OPND_CREATE_INTPTR(bb_clone));
    return DR_EMIT_DEFAULT;
}
