#ifndef HASHCONSOBJECT_HH
#define HASHCONSOBJECT_HH

#include <stdint.h>
#include <map>
#include <set>

using ::std::map;
using ::std::set;

template<class T>
class FlyweightObject {
protected:
    uint64_t uid;
    uint64_t hash;

    static inline uint64_t genHash(uint64_t a, uint64_t b) {
        uint64_t hash_result = a;
        asm (
                "crc32 %1, %0"
                : "=r" (hash_result)
                : "r" (b), "0" (hash_result)
            );
        return hash_result;
    }

public:
    FlyweightObject(void) {
        //FIXME: hacky way to get address of vtable as a non-zero and unique value for each class..
        uid = (uint64_t) *((uint64_t *) this);
    }

    uint64_t getUID() {
        return uid;
    }

    void setUID(uint64_t uid) {
        this->uid = uid;
    }

    bool equals(FlyweightObject<T> *other) {
        return other == this;
    }

    /**
     * Subclasses must extend FlyweightObject parameterized by themselves, and implement the same_as
     * function as a function that returns true(bool) when its parameter is structurally equal to
     * the object itself, and false otherwise.
     */
    virtual bool same_as(T *) = 0;

    /** Following a successful evalution of equality in function same_as, the classes may opt to perform an
     * operation over their objects' possible shadow data -- data that is not relevant to same_as.
     * This function is executed during the flyweight operation (see FlyWeightFactory Class), every time
     * two objects are found structurally equal, therefore the object given as parameter is to be deleted.
     */
    virtual void absorbShadowData(T *) = 0;

    /**
     * Classes must override getHashCode in the case they have any fields.
     * The overriding functions must take advantage of the genHash protected
     * function to generate hash codes, where the functions must satisfy the
     * following formula:
     *          f(x) = f(1) # f(2) # ... # f(n) # d(x) # uid
     * where:
     *  - f() is getHashCode(),
     *  - x is an object,
     *  - '#' is a binary operator equivalent to the application of genHash() on two
     *    operands,
     *  - {1, 2, ..., n} is the set of FlyweightObjects referenced as fields
     *    by the subclass,
     *  - d() is a function guaranteeing that if i, j are both instances of class N
     *    and i, j have equal non-FlyweightObject fields, then d(i) is equal to d(j).
     * Ideally, getHashCode() does not recalculate the hash on every call.
     */
    virtual uint64_t getHashCode(void) {
        if (!uid)
            return hash;

        hash = uid; /* f(x) calculation here */

        uid = 0;
        return hash;
    }

};

#endif
