#ifndef SYMBOLIC_CONTEXT_HH
#define SYMBOLIC_CONTEXT_HH

#include <map>
#include <list>
#include <set>
#include <utility>

#include "dr_api.h"

#include "./sym_tree/sym_tree.hh"
#include "instr_funct.hh"
#include "compile_mode.hh"
#include "./FlyweightFactory.hh"
#include "TreeNode.hh"
#include "./visitor/AddressMapperVisitor.hh"

/*Location of Prints */
#define OUTPATH(X) "./" X

#define SYM_OP_SIGNEXTEND "__SIGNEXTEND"
#define SYM_OP_TRUNC "__TRUNC"
#define SYM_OP_ZEROEXTEND "__ZEROEXTEND"
#define SYM_OP_LSBIT "__LSBIT"
#define SYM_OP_MSBIT "__MSBIT"
#define SYM_OP_1LS1BYTES "__1LS1BYTES"
#define SYM_OP_2LS1BYTES "__2LS1BYTES"
#define SYM_OP_1LS2BYTES "__1LS2BYTES"
#define SYM_OP_1LS4BYTES "__1LS4BYTES"
#define SYM_OP_1MS4BYTES "__1MS4BYTES"
#define SYM_OP_1MS8BYTES "__1MS8BYTES"
#define SYM_OP_1MS2BYTES "__1MS2BYTES"
#define SYM_OP_1MS1BYTES "__1MS1BYTES"

#define SYM_OP_ISZERO "__isZERO"
#define SYM_OP_PARITY_LSBYTE "__PARITY_LSBYTE"
#define SYM_OP_NTH_MSBIT "__nTH_MSBIT"
#define SYM_OP_NTH_LSBIT "__nTH_LSBIT"

#define SYM_OPCODE_SIGNEXTEND -1
#define SYM_OPCODE_TRUNC -2
#define SYM_OPCODE_ZEROEXTEND -3
#define SYM_OPCODE_LSBIT -4
#define SYM_OPCODE_MSBIT -5
#define SYM_OPCODE_1LS1BYTES -6
#define SYM_OPCODE_2LS1BYTES -7
#define SYM_OPCODE_1LS2BYTES -8
#define SYM_OPCODE_1LS4BYTES -9
#define SYM_OPCODE_1MS4BYTES -10
#define SYM_OPCODE_1MS8BYTES -11
#define SYM_OPCODE_1MS2BYTES -12
#define SYM_OPCODE_1MS1BYTES -13

#define SYM_OPCODE_ISZERO -14
#define SYM_OPCODE_PARITY_LSBYTE -15
#define SYM_OPCODE_NTH_MSBIT -16
#define SYM_OPCODE_NTH_LSBIT -17

using ::std::map;
using ::std::list;
using ::std::pair;
using ::std::set;

typedef struct alias_addrstats_t {
    list<Expression*> *li;
    int discoverable;
    int maybe_discoverable;
    int not_discoverable;
    int waw, raw, war, rar;
    bool prev_was_dst;

} alias_addrstats_t;

/**
 * Used by clean calls inspecting the instructions of the basic block
 */
class ExecutionContext {
private:
    int insCount;
    file_t sym_f;
    file_t alias_f;
    file_t asm_f;
    map<reg_id_t, int64_t> *initial_reg_symbols;
    map<byte*, int64_t> *initial_addr_symbols;
    map<Expression*, int64_t> *initial_expr_symbols; //comparator needed here
    map<FlagsBitExpr *, Edge*> *final_flag_symbols;
    map<int, void (*)(InstrData &instrData, instr_t *instr)> *instr_fn_map;

    list<instr_t *> *trace;

#ifdef DEBUG_GRAPH
    file_t graph_sym_f;
    file_t graph_expronly_f;
    file_t graph_flagsonly_f;
#endif

    /* alias analysis specific */
    map<byte*, alias_addrstats_t *> *addr_aliases;
    map<byte*, alias_addrstats_t *> *dstaddr_aliases;
    uint64_t mem_reads;
    uint64_t mem_writes;
    uint64_t aliases;
    double avg_active_read_aliases_before_new_write;

protected:
    map<Expression *, Edge *> *final_expr_symbols;
public:
    bool enabled;

    FlyweightFactory<AddrExpr> *fwfAddrExpr;
    FlyweightFactory<DerefExpr> *fwfDerefExpr;
    FlyweightFactory<ImmedExpr> *fwfImmedExpr;
    FlyweightFactory<ITEExpr> *fwfITEExpr;
    FlyweightFactory<MemRefExpr> *fwfMemRefExpr;
    FlyweightFactory<OperExpr> *fwfOperExpr;
    FlyweightFactory<RegExpr> *fwfRegExpr;
    FlyweightFactory<RegNameExpr> *fwfRegNameExpr;
    FlyweightFactory<FlagsBitExpr> *fwfFlagsBitExpr;
    FlyweightFactory<Edge> *fwfEdge;

    AddressMapperVisitor *amv;

    virtual void initObject(void);
    virtual void finiObject(void);
    void printInstruction(void* drctx, instr_t* instr);
    void printTrace(void *drctx);
    void calcFinalTrace(void *drctx);
    uint64_t addTraceInstr(instr_t *);
    virtual void calculate_alias_stats();
#ifdef DEBUG_SYM
    void printExpressions(void);
#endif
#ifdef DEBUG_GRAPH
    void printGraph(void);
#endif 
#ifdef DEBUG_BRANCH
    set<Edge *> *final_jump_symbols;
#endif

    AddrExpr *createAbsAddrOperand(opnd_t concrete_opnd, uint size);
    MemRefExpr *createBaseDisp(opnd_t concrete_opnd, uint size);

    /**
     * Add the expression corresponding to the source operand src to the alias map
     * WARNING: does not work with vector-addressing (AVX2) instructions
     */
    virtual void countMemoryRead(opnd_t, Expression *);

    /**
     * Add the expression corresponding to the destination operand dst to the alias map
     * WARNING: does not work with vector-addressing (AVX2) instructions
     */
    virtual void countMemoryWrite(opnd_t, Expression *);

    Edge *currentSymExprForReg(opnd_t registerOpnd);
    Edge *currentSymExprForReg(reg_id_t regg);
    virtual Edge *currentSymExprForMemRef(Expression *expr, uint size);
    Edge *currentSymExprForFlag(FlagsBitExpr*);
    inline void addSymExprForMemRef(Expression *memRef, Edge *ed);

    RegNameExpr *getRegNameObj(opnd_t opnd);

    RegNameExpr *getRegNameObj(reg_id_t regg);
    /**
     * Adds (or replaces) a mapping to the appropriate maps.
     */
    void addFlagExpr(int flags_bit, unsigned long instr, Edge *ed);
    void addSymAddrExpr(MemRefExpr *exprMemRef, unsigned long instr, Edge *ed);
    void addSymRegExpr(RegNameExpr *exprReg, unsigned long instr, Edge *ed);
    void addAbsAddrExpr(AddrExpr *exprAddr, unsigned long instr, Edge *ed);


    /**
     * Creates and fills the instruction-function map with key-value pairs of <OPcode,function address>
     */
    void build_instr_fn_map(void);

    /**
     * Fills the flags symbol map with the key-value pairs of <flagbits, bit value>
     */
    void build_symbol_map_flags();

    /**
     * Creates and fills the register symbol map with key-value pairs of <reg#,register value>
     */
    void build_symbol_map_reg(dr_mcontext_t mctx);

    /**
     * Creates the expression symbol map with key-value paris of <Expression*, Expression*>
     */
    void build_symbol_map_expr(void);

    /**
     * Creates the map for examination of aliases
     */
    void build_addr_aliases_map(void);

    /**
     * Reads size bytes of data from an absolute address which corresponds to an expression
     * and adds it to the address symbol map mapped as a key-value pair of <address, value>
     * and the expression symbol map mapped as a key-value pair of <Expression, value>
     */
    void add_init_symbol_expr(Expression *expr, byte *addr, size_t size);

    /**
     * Reads size bytes of data from an absolute address and adds it to the address symbol map mapped
     * as a key-value pair of <address, value>
     */
    void add_init_symbol_addr(byte *addr, size_t size);

};

/**
 * Helper struct and functions for alias analysis
 */

alias_addrstats_t *init_alias_addrstats(void);
void alias_addrstats_add(alias_addrstats_t *stats, Expression *expr, bool is_dst);

void calculate_alias_stats(ExecutionContext *ectx);
alias_addrstats_t *init_alias_addrstats(void);
void alias_addrstats_add(alias_addrstats_t *stats, Expression *expr, bool is_dst);
bool debug_instrument_ok(instrlist_t *bb);
bool debug_instrument_ok_nop(instrlist_t *bb);

#endif
