#ifndef HASHCONS_HH
#define HASHCONS_HH

#include <map>
#include <set>
#include "FlyweightObject.hh"

using ::std::map;
using ::std::set;

template<class T>
class FlyweightFactory {
private:
    map<uint64_t, set<T *> *> *idMap;
public:
    FlyweightFactory(void) {
        idMap = new map<uint64_t, set<T *> *>();
    }

    ~FlyweightFactory(void) {
        delete idMap;
    }

    T *getFWObj(T *object) {
        uint64_t hashcode = object->getHashCode();
        set<T *> *hcs = (*idMap)[hashcode];
        if (hcs == NULL) {
            hcs = (*idMap)[hashcode] = new set<T *>();
        }
        else {
            hcs = (*idMap)[hashcode];
            typename set<T *>::iterator sit;
            for (sit = hcs->begin(); sit != hcs->end(); sit++) {
                if (*sit == object)
                    return *sit;
                if ((*sit)->same_as(object)) {
                    (*sit)->absorbShadowData(object);
                    delete object;
                    return *sit;
                }
            }
        }
        hcs->insert(object);
        return object;
    }
};

#endif
