#include <climits>
#include <set>
#include <string.h>
#include <stdint.h>

#include "sym_tree.hh"
#include "../ExecutionContext.hh"
#include "../instr_funct.hh"

AddrExpr::AddrExpr(byte *address, int size) : addr(address) {
    relative_to = NULL;
    this->size = size;
    far_segm = UNAVAILABLE_SEGMENT;
}

AddrExpr::AddrExpr(byte *address, byte *from, int size) : addr(address) {
    relative_to = from;
    this->size = size;
    far_segm = UNAVAILABLE_SEGMENT;
}

AddrExpr::AddrExpr(reg_id_t segment, byte *address, int size) : addr(address) {
    relative_to = NULL;
    this->size = size;
    far_segm = (int)segment;
}

AddrExpr::~AddrExpr() {
}

void AddrExpr::print(file_t fd, int depth) {
    if (relative_to == NULL) {
        if (far_segm != UNAVAILABLE_SEGMENT)
            dr_fprintf(fd, "ADDR(%llx : SEG%d)", addr, far_segm);
        else
            dr_fprintf(fd, "ADDR(%llx)", addr);
        if (!depth)
            dr_fprintf(fd, "\n");
    }
    else {
        dr_fprintf(fd, "ADDR(from: %llx: %llx)", relative_to, addr);
        if (!depth)
            dr_fprintf(fd, "\n");
    }
}

void AddrExpr::add_self(ExecutionContext *ectx, unsigned long instr, Edge* edge) {
    ectx->addAbsAddrExpr(this, instr, edge);
}

byte *AddrExpr::get_val(void) {
    return addr;
}

uint64_t AddrExpr::getHashCode(void) {
    if (!uid)
        return hash;
    uint64_t h = genHash((uint64_t)addr, (uint64_t)relative_to);
    h = genHash(h, far_segm);
    hash = genHash(h, this->uid);
    hash = uid; /* f(x) calculation here */

    uid = 0;
    return hash;
}

bool AddrExpr::same_as(Expression *other) {
    if (other == NULL)
        return false;
    if (typeid(*this) != typeid(*other))
        return false;
    AddrExpr *ae_other = (AddrExpr *) other;
    if (this == ae_other)
        return true;
    return addr == ae_other->addr
           && relative_to == ae_other->relative_to
           && far_segm == ae_other->far_segm;
}

void AddrExpr::absorbShadowData(Expression *other) {
}

bool AddrExpr::reachable_through_oper(Expression *other, int *allowed_operations) {
    return ((Expression*)this) == other;
}

void AddrExpr::accept(Visitor *v) {
    v->visit(this);
}

void AddrExpr::accept(ArguVisitor *av, void *arg) {
    av->visit(this, arg);
}

void *AddrExpr::accept(RetArguVisitor *av, void *arg) {
    return av->visit(this, arg);
}

void *AddrExpr::accept(RetVisitor *rv) {
    return rv->visit(this);
}
