#include <climits>
#include <set>
#include <string.h>
#include <stdint.h>

#include "sym_tree.hh"
#include "../ExecutionContext.hh"
#include "../instr_funct.hh"

using ::std::set;

/** Expression Class functions */

Expression::Expression() {
#ifdef DEBUG_GRAPH
    printed = false;
    printed_final = false;
#endif
}

void Expression::print(file_t fd) {
    this->print(fd, 0);
}

int Expression::get_size() {
    return size;
}

Expression::~Expression() {
}

StateNode::StateNode() {
}

StateNode::~StateNode() {
}
