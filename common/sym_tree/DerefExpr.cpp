#include <climits>
#include <set>
#include <stdint.h>

#include "sym_tree.hh"
#include "../ExecutionContext.hh"
#include "../instr_funct.hh"

DerefExpr::DerefExpr(Edge *data, int size) : data(data) {
    this->size = size;
}

DerefExpr::~DerefExpr() {
}

void DerefExpr::print(file_t fd, int depth) {
    dr_fprintf(fd, "MEM", size);
    ++depth;
    if (data != NULL)
        data->print(fd, depth);
    if (depth == 1) /* increased by one, essentially checking for zero in param depth */
        dr_fprintf(fd, "\n");
}

void DerefExpr::add_self(ExecutionContext *ectx, unsigned long instr, Edge* my_expr) {
    dr_fprintf(STDERR, "Error, attempting to write Deref Expression to final symbols:\n");
    this->print(STDERR, 0);
    dr_abort();
}

uint64_t DerefExpr::getHashCode(void) {
    if (!uid)
        return hash;
    uint64_t h = 0;
    if (data != NULL)
        h = data->getHashCode();
    hash = genHash(h, this->uid);
    uid = 0;
    return hash;
}

bool DerefExpr::same_as(Expression *other) {
    if (other == NULL)
        return false;
    if (typeid(*this) != typeid(*other))
        return false;
    DerefExpr *de_other = (DerefExpr *) other;
    if (this == de_other)
        return true;
    return data != NULL && data->same_as(de_other->data);
}

void DerefExpr::absorbShadowData(Expression *other) {
}

bool DerefExpr::reachable_through_oper(Expression *other, int *allowed_operations) {
    return (Expression*)this == other;
}

Expression * DerefExpr::getMemRef(void) {
    return data->data;
}

void DerefExpr::accept(Visitor *v) {
    v->visit(this);
}

void DerefExpr::accept(ArguVisitor *av, void *arg) {
    av->visit(this, arg);
}

void *DerefExpr::accept(RetArguVisitor *av, void *arg) {
    return av->visit(this, arg);
}

void *DerefExpr::accept(RetVisitor *rv) {
    return rv->visit(this);
}
