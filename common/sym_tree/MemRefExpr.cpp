#include <climits>
#include <set>
#include <stdint.h>

#include "sym_tree.hh"
#include "../ExecutionContext.hh"
#include "../instr_funct.hh"

MemRefExpr::MemRefExpr(Edge *base, Edge *index, Expression *disp, Expression *scale, uint size)
        : disp(disp), base(base), index(index), scale(scale) {
    this->size = size;
    far_segm = UNAVAILABLE_SEGMENT;
}

MemRefExpr::MemRefExpr(reg_id_t segment, Edge *base, Edge *index, Expression *disp, Expression *scale, uint size)
        : disp(disp), base(base), index(index), scale(scale) {
    this->size = size;
    far_segm = (int)segment;
}

MemRefExpr::~MemRefExpr() {
}

void MemRefExpr::print(file_t fd, int depth) {
    dr_fprintf(fd, "[", size);
    ++depth;
    if (base != NULL) {
        base->print(fd, depth);
    }
    if (disp != NULL) {
        dr_fprintf(fd, " + ");
        disp->print(fd, depth);
    }
    if (index != NULL) {
        dr_fprintf(fd, " + ");
        index->print(fd, depth);
        dr_fprintf(fd, " * ");
    }
    if (scale != NULL) {
        scale->print(fd, depth);
    }
    if (depth == 1) /* increased by one, essentially checking for zero in param depth */
        dr_fprintf(fd, "]\n");
    else
        dr_fprintf(fd, "]");
}

void MemRefExpr::add_self(ExecutionContext *ectx, unsigned long instr, Edge* edge) {
    ectx->addSymAddrExpr(this, instr, edge);
#ifdef DEBUG_CYCLIC_GRAPH_CHECK
    dr_fprintf(STDERR, "Checking tree cycle: ");
    if (my_expr->examine_cycle()) {
        dr_fprintf(STDERR, "true..\n");
        dr_abort();
    }
    else
        dr_fprintf(STDERR, "false..\n");
#endif
}

uint64_t MemRefExpr::getHashCode(void) {
    uint64_t h, bh, dh, ih, sh;
    if (!uid)
        return hash;
    bh = (base == NULL) ? 0 : base->getHashCode();
    dh = (disp == NULL) ? 0 : disp->getHashCode();
    ih = (index == NULL) ? 0 : index->getHashCode();
    sh = (scale == NULL) ? 0 : scale->getHashCode();

    h = genHash(bh, dh);
    h = genHash(h, ih);
    h = genHash(h, sh);
    h = genHash(h, uid);

    hash = h;
    uid = 0;
    return hash;
}

bool MemRefExpr::same_as(Expression *other) {
    if (other == NULL)
        return false;
    if (typeid(*this) != typeid(*other))
        return false;
    MemRefExpr *de_other = (MemRefExpr *) other;

    if (de_other == this)
        return true;
    if (!((base == NULL) ? (de_other->base == NULL) : (base->same_as(de_other->base))))
        return false;
    if (!((disp == NULL) ? (de_other->disp == NULL) : (disp->same_as(de_other->disp))))
        return false;
    if (!((index == NULL) ? (de_other->index == NULL) : (index->same_as(de_other->index))))
        return false;
    if (!((scale == NULL) ? (de_other->scale == NULL) : (scale->same_as(de_other->scale))))
        return false;

    return true;
}

void MemRefExpr::absorbShadowData(Expression *other) {
}

bool MemRefExpr::reachable_through_oper(Expression *other, int *allowed_operations) {
    return (Expression*)this == other;
}

void MemRefExpr::accept(Visitor *v) {
    v->visit(this);
}

void *MemRefExpr::accept(RetArguVisitor *av, void *arg) {
    return av->visit(this, arg);
}

void MemRefExpr::accept(ArguVisitor *av, void *arg) {
    av->visit(this, arg);
}

void *MemRefExpr::accept(RetVisitor *rv) {
    return rv->visit(this);
}
