#include <climits>
#include <set>
#include <string.h>
#include <stdint.h>

#include "sym_tree.hh"
#include "../ExecutionContext.hh"
#include "../instr_funct.hh"

ITEExpr::ITEExpr(const char *condi, Edge **condi_operands, int condi_opnd_amount,
                     Edge *valid, Edge *invalid)
        : condition(condi), condition_opnds(condi_operands), condition_opnd_amt(condi_opnd_amount) {
    this->valid = valid;
    this->invalid = invalid;
    size = valid->data->get_size();
}

ITEExpr::~ITEExpr() {
    delete condition_opnds;
}

void ITEExpr::print(file_t fd, int depth) {
    int i = 0, temp_amt = condition_opnd_amt;
    dr_fprintf(fd, "{ (");
    while (temp_amt > 1) {
        if (condition_opnds[i] != NULL) {
            condition_opnds[i]->print(fd, depth + 1);
            dr_fprintf(fd, " %s ", condition);
        }
        ++i;
        --temp_amt;
    }
    condition_opnds[i]->print(fd, depth + 1);
    dr_fprintf(fd, ") ? (");
    if (valid != NULL)
        valid->print(fd, depth + 1);
    dr_fprintf(fd, ") : (");
    if (invalid != NULL)
        invalid->print(fd, depth + 1);
    if (!depth)
        dr_fprintf(fd, ") }\n");
    else
        dr_fprintf(fd, ") }");
}
void ITEExpr::add_self(ExecutionContext *ectx, unsigned long instr, Edge* my_expr) {
    dr_fprintf(STDERR, "Error, attempting to write Condition Expression to final symbols..\n");
    dr_abort();
}

uint64_t ITEExpr::getHashCode(void) {
    if (!uid)
        return hash;
    int i = 0;
    uint64_t h = condition_opnds[0]->getHashCode();
    for (i = 1; i < condition_opnd_amt; ++i)
        h = genHash(h, condition_opnds[i]->getHashCode());
    h = genHash(h, valid->getHashCode());
    h = genHash(h, invalid->getHashCode());
    h = genHash(h, uid);
    hash = h;
    uid = 0;
    return hash;
}

bool ITEExpr::same_as(Expression *other) {
    int i;
    if (other == NULL)
        return false;
    if (typeid(*this) != typeid(*other))
        return false;
    ITEExpr *ie_other = (ITEExpr *) other;
    if (ie_other == this)
        return true;

    if (!valid->same_as(ie_other->valid) || !invalid->same_as(ie_other->invalid)) {
        return false;
    }
    for (i = 0; i < condition_opnd_amt; ++i)
        if (condition_opnds[i] != ie_other->condition_opnds[i])
           return false; 

    return false;
}

void ITEExpr::absorbShadowData(Expression *other) {
}

bool ITEExpr::reachable_through_oper(Expression *other, int *allowed_operations) {
    return (Expression*)this == other;
}

void ITEExpr::accept(Visitor *v) {
    v->visit(this);
}

void ITEExpr::accept(ArguVisitor *av, void *arg) {
    av->visit(this, arg);
}

void *ITEExpr::accept(RetArguVisitor *av, void *arg) {
    return av->visit(this, arg);
}

void *ITEExpr::accept(RetVisitor *rv) {
    return rv->visit(this);
}
