#ifndef SYM_TREE_HH
#define SYM_TREE_HH

#include <set>
#include <stdint.h>
#include <list>
#include <typeinfo>

#include "dr_api.h"

#include "../compile_mode.hh"
#include "../FlyweightObject.hh"
#include "../visitor/Visitor.hh"
#include "References.hh"

#define UNAVAILABLE_SEGMENT -1337

#define REG_64BIT_SIZE 8
#define REG_32BIT_SIZE 4
#define REG_16BIT_SIZE 2
#define REG_8BIT_HIGH_SIZE 3
#define REG_8BIT_LOW_SIZE 1

#define CONDITION_EQ "=="
#define CONDITION_GT ">"
#define CONDITION_GE ">="
#define CONDITION_NE "!="
#define CONDITION_LT "<"
#define CONDITION_LE "<="
#define CONDITION_Z  "==0"
#define CONDITION_NZ "!=0"

#define DEFAULT_DISP_SIZE 4
#define DEFAULT_SCALE_SIZE 0

using ::std::list;

struct Edge;
class Symbol;
class Expression;
class OperExpr;
class RegExpr;
class FlagsBitExpr;
class RegNameExpr;
class ImmedExpr;
class MemRefExpr;
class AddrExpr;
class ITEExpr;
class DerefExpr;

class ExecutionContext;

enum itype {
    INT8_TYPE,
    INT16_TYPE,
    INT32_TYPE,
    INT64_TYPE,
    FLOAT_TYPE,
    DOUBLE_TYPE,
    LODOUBLE_TYPE,
    SCALE_TYPE,
    UNKNOWN_TYPE
};

typedef union ival {
    int64_t iint;
    float ifloat;
    double idouble;
    long double ilodouble;
} ival;

class Expression : public FlyweightObject<Expression> {
protected:
    bool addFinalSymbol(Symbol *sym);
    bool removeFinalSymbol(Symbol *sym);
#ifdef DEBUG_GRAPH
    bool printed;
    bool printed_final;
#endif
public:
    int size;

    Expression();
    ~Expression();

    virtual bool same_as(Expression *other) = 0;
    virtual void absorbShadowData(Expression *other) = 0;
    virtual void print(file_t fd, int depth) = 0;
    virtual void add_self(ExecutionContext *exCtx, unsigned long instr, Edge* my_expr) = 0;
    virtual bool reachable_through_oper(Expression * check, int * allowed_operations) = 0;

    void print(file_t fd);
    int get_size();
#ifdef DEBUG_DATA
    void print_debug(file_t fd);
    virtual void print_debug(file_t fd, int depth) = 0;
#endif
#ifdef DEBUG_GRAPH
    virtual void print_graph(file_t fd) = 0;
    virtual void print_graph_as_final(file_t fd);
#endif
    virtual void accept(Visitor *v) = 0;
    virtual void *accept(RetVisitor *rv) = 0;
    virtual void accept(ArguVisitor *av, void *arg) = 0;
    virtual void *accept(RetArguVisitor *av, void *arg) = 0;
};

struct StateNode : public Expression, public RefHolder<unsigned long> {
    StateNode();
    ~StateNode();
    virtual bool same_as(Expression *other) = 0;
    virtual void absorbShadowData(Expression *other) = 0;
    virtual void print(file_t fd, int depth) = 0;
    virtual void add_self(ExecutionContext *exCtx, unsigned long instr, Edge* my_expr) = 0;
    virtual bool reachable_through_oper(Expression * check, int * allowed_operations) = 0;

#ifdef DEBUG_DATA
    virtual void print_debug(file_t fd, int depth) = 0;
#endif
#ifdef DEBUG_GRAPH
    virtual void print_graph(file_t fd) = 0;
    virtual void print_graph_as_final(file_t fd);
#endif
    virtual void accept(Visitor *v) = 0;
    virtual void *accept(RetVisitor *rv) = 0;
    virtual void accept(ArguVisitor *av, void *arg) = 0;
    virtual void *accept(RetArguVisitor *av, void *arg) = 0;
};

class OperExpr : public Expression {
private:
    void print_flag_opnd(file_t, int, Edge *);
protected:
    const char *operation;
public:
    Edge **opnds;
    int opnd_amt;
    int opcode;

    OperExpr(int opcode, const char *oper, Edge **operands, int opnd_amount, int result_size);
    OperExpr();
    ~OperExpr();
#ifdef DEBUG_DATA
    virtual void print_debug(file_t fd, int depth);
#endif
#ifdef DEBUG_GRAPH
    virtual void print_graph(file_t fd);
#endif

    virtual bool same_as(Expression *other);
    virtual void absorbShadowData(Expression *other);
    void add_self(ExecutionContext *exCtx, unsigned long instr, Edge* my_expr);
    virtual void print(file_t fd, int depth);
    virtual uint64_t getHashCode();
    virtual bool reachable_through_oper(Expression *check, int *allowed_operations);
    virtual void accept(Visitor *v);
    virtual void *accept(RetVisitor *rv);
    virtual void accept(ArguVisitor *av, void *arg);
    virtual void *accept(RetArguVisitor *av, void *arg);

};

class RegExpr : public Expression {
private:

    reg_id_t regg;
public:
    RegExpr(RegNameExpr *pExpr);
    RegExpr(reg_id_t reg);
    RegExpr(reg_id_t reg, int size);
    ~RegExpr();
    virtual void print(file_t fd, int depth);
#ifdef DEBUG_GRAPH
    virtual void print_graph(file_t fd);
#endif

#ifdef DEBUG_DATA
    virtual void print_debug(file_t fd, int depth);
#endif


    virtual bool same_as(Expression *other);
    virtual void absorbShadowData(Expression *other);
    void add_self(ExecutionContext *exCtx, unsigned long instr, Edge* my_expr);
    virtual uint64_t getHashCode();
    virtual bool reachable_through_oper(Expression * check, int * allowed_operations);

    virtual void accept(Visitor *v);
    virtual void *accept(RetVisitor *rv);
    virtual void accept(ArguVisitor *av, void *arg);
    virtual void *accept(RetArguVisitor *av, void *arg);
};

class ITEExpr : public Expression {
private:
    const char *condition;
    Edge **condition_opnds;
    int condition_opnd_amt;
    Edge *valid, *invalid;
public:
    ITEExpr(const char *condi, Edge **condi_operands, int condi_opnd_amount,
             Edge *valid, Edge *invalid);
    ~ITEExpr();
    virtual void print(file_t fd, int depth);
#ifdef DEBUG_GRAPH
    virtual void print_graph(file_t fd);
#endif
#ifdef DEBUG_DATA
    virtual void print_debug(file_t fd, int depth);
#endif


    virtual bool same_as(Expression *other);
    virtual void absorbShadowData(Expression *other);
    void add_self(ExecutionContext *exCtx, unsigned long instr, Edge* my_expr);
    virtual uint64_t getHashCode();
    virtual bool reachable_through_oper(Expression * check, int * allowed_operations);

    virtual void accept(Visitor *v);
    virtual void *accept(RetVisitor *rv);
    virtual void accept(ArguVisitor *av, void *arg);
    virtual void *accept(RetArguVisitor *av, void *arg);
};

class ImmedExpr : public Expression {
private:
    itype type;
public:
    ival value;
    ImmedExpr(int64_t immediate, int size);
    ImmedExpr(float immediate, int size);
    ImmedExpr(double immediate, int size);
    ImmedExpr(long double immediate, int size);
    ~ImmedExpr();
    virtual void print(file_t fd, int depth);

#ifdef DEBUG_GRAPH
    virtual void print_graph(file_t fd);
#endif

#ifdef DEBUG_DATA
    virtual void print_debug(file_t fd, int depth);
#endif


    virtual bool same_as(Expression *other);
    virtual void absorbShadowData(Expression *other);
    void add_self(ExecutionContext *exCtx, unsigned long instr, Edge* my_expr);
    virtual uint64_t getHashCode();
    virtual bool reachable_through_oper(Expression * check, int * allowed_operations);

    virtual void accept(Visitor *v);
    virtual void *accept(RetVisitor *rv);
    virtual void accept(ArguVisitor *av, void *arg);
    virtual void *accept(RetArguVisitor *av, void *arg);
};

class AddrExpr : public StateNode {
private:
    byte *addr;
    byte *relative_to;
    int far_segm;
public:
    AddrExpr(byte *address, int size);
    AddrExpr(byte *address, byte *from, int size);
    AddrExpr(reg_id_t segment, byte *address, int size);
    ~AddrExpr();
    virtual void print(file_t fd, int depth);
#ifdef DEBUG_GRAPH
    virtual void print_graph(file_t fd);
    virtual void print_graph_as_final(file_t fd);
#endif
#ifdef DEBUG_DATA
    virtual void print_debug(file_t fd, int depth);
#endif



    virtual bool same_as(Expression *other);
    virtual void absorbShadowData(Expression *other);
    void add_self(ExecutionContext *exCtx, unsigned long instr, Edge* my_expr);
    virtual uint64_t getHashCode();
    byte *get_val(void);
    virtual bool reachable_through_oper(Expression * check, int * allowed_operations);

    virtual void accept(Visitor *v);
    virtual void *accept(RetVisitor *rv);
    virtual void accept(ArguVisitor *av, void *arg);
    virtual void *accept(RetArguVisitor *av, void *arg);
};

class MemRefExpr : public StateNode {
public:
    int far_segm;
    Edge *base;
    Expression *disp;
    Edge *index;
    Expression *scale;

    MemRefExpr(Edge *base, Edge *index, Expression *disp, Expression *scale, uint size);
    MemRefExpr(reg_id_t segment, Edge *base, Edge *index, Expression *disp, Expression *scale, uint size);
    ~MemRefExpr();
    virtual void print(file_t fd, int depth);
#ifdef DEBUG_GRAPH
    virtual void print_graph(file_t fd);
    virtual void print_graph_as_final(file_t fd);
#endif
#ifdef DEBUG_DATA
    virtual void print_debug(file_t fd, int depth);
#endif


    virtual bool same_as(Expression *other);
    virtual void absorbShadowData(Expression *other);
    void add_self(ExecutionContext *exCtx, unsigned long instr, Edge* my_expr);
    virtual uint64_t getHashCode();
    virtual bool reachable_through_oper(Expression * check, int * allowed_operations);

    virtual void accept(Visitor *v);
    virtual void *accept(RetVisitor *rv);
    virtual void accept(ArguVisitor *av, void *arg);
    virtual void *accept(RetArguVisitor *av, void *arg);
};

class DerefExpr : public Expression {
public:
    Edge *data;
    DerefExpr(Edge *data, int size);
    ~DerefExpr();
    virtual void print(file_t fd, int depth);
#ifdef DEBUG_GRAPH
    virtual void print_graph(file_t fd);
#endif
#ifdef DEBUG_DATA
    virtual void print_debug(file_t fd, int depth);
#endif


    virtual bool same_as(Expression *other);
    virtual void absorbShadowData(Expression *other);
    void add_self(ExecutionContext *exCtx, unsigned long instr, Edge* my_expr);
    virtual uint64_t getHashCode();
    Expression * getMemRef(void);
    virtual bool reachable_through_oper(Expression * check, int * allowed_operations);

    virtual void accept(Visitor *v);
    virtual void *accept(RetVisitor *rv);
    virtual void accept(ArguVisitor *av, void *arg);
    virtual void *accept(RetArguVisitor *av, void *arg);
};

class RegNameExpr : public StateNode {
private:
    reg_id_t regg;
public:
    RegNameExpr(reg_id_t reg);
    RegNameExpr(reg_id_t reg, int size);
    ~RegNameExpr();
    virtual void print(file_t fd, int depth);
#ifdef DEBUG_GRAPH
    virtual void print_graph(file_t fd);
    virtual void print_graph_as_final(file_t fd);
#endif

#ifdef DEBUG_DATA
    virtual void print_debug(file_t fd, int depth);
#endif


    virtual bool same_as(Expression *other);
    virtual void absorbShadowData(Expression *other);
    void add_self(ExecutionContext *exCtx, unsigned long instr, Edge* my_expr);
    virtual uint64_t getHashCode();
    virtual bool reachable_through_oper(Expression * check, int * allowed_operations);

    reg_id_t getRegg();

    virtual void accept(Visitor *v);
    virtual void *accept(RetVisitor *rv);
    virtual void accept(ArguVisitor *av, void *arg);
    virtual void *accept(RetArguVisitor *av, void *arg);
};

class FlagsBitExpr : public StateNode {
private:
    int flags_bit;
public:
    FlagsBitExpr(int flags_bit);
    ~FlagsBitExpr();
    virtual void print(file_t fd, int depth);
#ifdef DEBUG_GRAPH
    virtual void print_graph(file_t fd);
    virtual void print_graph_as_final(file_t fd);
#endif

#ifdef DEBUG_DATA
    virtual void print_debug(file_t fd, int depth);
#endif


    virtual bool same_as(Expression *other);
    virtual void absorbShadowData(Expression *other);
    void add_self(ExecutionContext *exCtx, unsigned long instr, Edge* my_expr);
    virtual uint64_t getHashCode();
    virtual bool reachable_through_oper(Expression * check, int * allowed_operations);

    virtual void accept(Visitor *v);
    virtual void *accept(RetVisitor *rv);
    virtual void accept(ArguVisitor *av, void *arg);
    virtual void *accept(RetArguVisitor *av, void *arg);
};

#define EDGE_DEST_INITIAL -1
#define EDGE_DEST_ANY -2
#define EDGE_DEST_CF -110
#define EDGE_DEST_PF -111
#define EDGE_DEST_AF -112
#define EDGE_DEST_ZF -113
#define EDGE_DEST_SF -114
#define EDGE_DEST_TF -115
#define EDGE_DEST_IF -116
#define EDGE_DEST_DF -117
#define EDGE_DEST_OF -118
#define EDGE_DEST_NT -119
#define EDGE_DEST_RF -120

struct Edge : public FlyweightObject<Edge> {
    int destnum;
    Expression *data;

    Edge(int destnum, Expression *data);
    virtual bool same_as(Edge *other);
    virtual void absorbShadowData(Edge *other);
    virtual uint64_t getHashCode(void);

    void print(file_t fd, int depth);
    void print(file_t fd);
#ifdef DEBUG_GRAPH
    void print_graph(file_t fd);
#endif

#ifdef DEBUG_DATA
    void print_debug(file_t fd, int depth);
    void print_deps(file_t fd);
#endif
    virtual void accept(Visitor *v);
    virtual void *accept(RetVisitor *rv);
    virtual void accept(ArguVisitor *av, void *arg);
    virtual void *accept(RetArguVisitor *av, void *arg);

    void insertRef(RefHolder<unsigned long> *rh);
    TreeNode<unsigned long> *getTree(unsigned long refId);

    void linkDependency(Edge *other, unsigned long instr, unsigned long dep);
    void linkMovDependency(Edge *other, unsigned long instr, unsigned long dep);
    void linkOpndCalcDependency(Edge *other, unsigned long instr, unsigned long dep);
    bool evalDependsOn(unsigned long instr, unsigned long dependency);
    bool containsRefOf(RefHolder<unsigned long> *rh);

    int id;
    bool printed;

    map<unsigned long, TreeNode<unsigned long> *> trees;
    set<unsigned long> movDeps;
};

#endif
