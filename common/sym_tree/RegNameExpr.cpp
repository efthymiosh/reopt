#include <climits>
#include <set>
#include <string.h>
#include <stdint.h>

#include "sym_tree.hh"
#include "../ExecutionContext.hh"
#include "../instr_funct.hh"

RegNameExpr::RegNameExpr(reg_id_t reg) : regg(reg) {
    size = opnd_size_in_bytes(regg);
}

RegNameExpr::~RegNameExpr() {
}

void RegNameExpr::print(file_t fd, int depth) {
    dr_fprintf(fd, "%s", get_reg_name(regg));
    if (!depth)
        dr_fprintf(fd, "\n");
}

void RegNameExpr::add_self(ExecutionContext *ectx, unsigned long instr, Edge* edge) {
    ectx->addSymRegExpr(this, instr, edge);
}

uint64_t RegNameExpr::getHashCode() {
    uint64_t h = genHash((uint64_t) regg, (uint64_t) get_size());
    return hash = genHash(h, uid);
}

reg_id_t RegNameExpr::getRegg() {
    return regg;
}

bool RegNameExpr::same_as(Expression *other) {
    if (other == NULL)
        return false;
    if (typeid(*this) != typeid(*other))
        return false;
    RegNameExpr *re_other = (RegNameExpr *)other;
    if (re_other == this)
        return true;
    return regg == re_other->regg && size == re_other->size;
}

void RegNameExpr::absorbShadowData(Expression *other) {
}

bool RegNameExpr::reachable_through_oper(Expression *other, int *allowed_operations) {
    return (Expression*)this == other;
}

void RegNameExpr::accept(Visitor *v) {
    v->visit(this);
}

void *RegNameExpr::accept(RetArguVisitor *av, void *arg) {
    return av->visit(this, arg);
}

void RegNameExpr::accept(ArguVisitor *av, void *arg) {
    av->visit(this, arg);
}

void *RegNameExpr::accept(RetVisitor *rv) {
    return rv->visit(this);
}
