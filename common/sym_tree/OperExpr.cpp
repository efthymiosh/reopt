#include <climits>
#include <set>
#include <string.h>
#include <stdint.h>

#include "sym_tree.hh"
#include "../ExecutionContext.hh"
#include "../instr_funct.hh"

OperExpr::OperExpr(int opcode, const char* oper, Edge** operands, int opnd_amount, int result_size) :
        operation(oper), opnds(operands), opnd_amt(opnd_amount) {
    size = result_size;
    this->opcode = opcode;
}

OperExpr::~OperExpr() {
   delete opnds;
}

void OperExpr::print(file_t fd, int depth) {
    int i;
    dr_fprintf(fd, "%s(", operation, size, this);
    for (i = 0; i < opnd_amt - 1; i++) {
        if (opnds[i] != NULL) {
            opnds[i]->print(fd, depth + 1);
            dr_fprintf(fd, ", ");
        }
    }
    opnds[i]->print(fd, depth + 1);
    if (!depth)
        dr_fprintf(fd, ")\n");
    else
        dr_fprintf(fd, ")");
}

void OperExpr::add_self(ExecutionContext *ectx, unsigned long instr, Edge* my_expr) {
    dr_fprintf(STDERR, "Error, attempting to write Operation Expression to final symbols:\n");
    this->print(STDERR, 0);
    dr_abort();
}

uint64_t OperExpr::getHashCode(void) {
    uint64_t h;
    if (!uid)
        return hash;

    if (!opnd_amt) {
        hash = genHash((uint64_t)opcode, uid);
    }
    else {
        int i;
        uint64_t h = opnds[0]->getHashCode();
        for (i = 1; i < opnd_amt; ++i)
            h = genHash(h, opnds[i]->getHashCode());
        h = genHash(h, (uint64_t)opcode);
        hash = genHash(h, uid);
    }

    uid = 0;
    return hash;
}

bool OperExpr::same_as(Expression *other) {
    if (other == NULL)
        return false;
    if (typeid(*this) != typeid(*other))
        return false;
    OperExpr *oe_other = (OperExpr *) other;
    if (oe_other == this)
        return true;
    if (size == oe_other->size
        && (opcode == oe_other->opcode)
        && opnd_amt == oe_other->opnd_amt) {
        int i;
        for (i = 0; i < opnd_amt; i++)
            if (!opnds[i]->same_as(oe_other->opnds[i]))
                return false;
        return true;
    }
    return false;
}

void OperExpr::absorbShadowData(Expression *other) {
}

bool OperExpr::reachable_through_oper(Expression *other, int *allowed_operations) {
    if ((Expression*)this == other)
        return true;
    for (int i = 0; opcode != allowed_operations[i]; i++)
        if (allowed_operations[i] == OP_AFTER_LAST)
            return false;
    for (int i = 0; i < opnd_amt; i++)
        if (opnds[i]->data->reachable_through_oper(other, allowed_operations))
            return true;
}

void OperExpr::accept(Visitor *v) {
    v->visit(this);
}

void *OperExpr::accept(RetArguVisitor *av, void *arg) {
    return av->visit(this, arg);
}

void OperExpr::accept(ArguVisitor *av, void *arg) {
    av->visit(this, arg);
}

void *OperExpr::accept(RetVisitor *rv) {
    return rv->visit(this);
}
