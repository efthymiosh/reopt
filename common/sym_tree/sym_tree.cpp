#include <climits>
#include <set>
#include <string.h>

#include "sym_tree.hh"
#include "References.hh"
#include "../ExecutionContext.hh"
#include "../instr_funct.hh"

#ifdef DEBUG_CYCLIC_GRAPH_CHECK
bool OperExpr::examine_cycle() {
    clear_forefathers();
    forefathers->insert(this);
    int i;
    for (i = 0; i < opnd_amt; i++) {
        if (opnds[i]->check_forefathers())
            return true;
    }
    forefathers->erase(this);
    return false;
}

bool MemRefExpr::examine_cycle() {
    clear_forefathers();
    forefathers->insert(this);
    if (base != NULL && base->check_forefathers())
        return true;
    if (disp != NULL && disp->check_forefathers())
        return true;
    if (index != NULL && index->check_forefathers())
        return true;
    if (scale != NULL && scale->check_forefathers())
        return true;
    forefathers->erase(this);
    return false;
}

bool ITEExpr::examine_cycle() {
    clear_forefathers();
    forefathers->insert(this);
    int i;
    if (valid->check_forefathers() || invalid->check_forefathers())
        return true;
    for (i = 0; i < condition_opnd_amt; i++) {
        if (condition_opnds[i]->check_forefathers())
            return true;
    }
    forefathers->erase(this);
    return false;
}

bool AddrExpr::examine_cycle() {
    return false;
}

bool ImmedExpr::examine_cycle() {
    return false;
}

bool RegExpr::examine_cycle() {
    return false;
}

bool OperExpr::check_forefathers() {
    int i;
    for (set<Expression *>::iterator it = forefathers->begin(); it != forefathers->end(); it++)
        if (*it == this)
            return true;
    forefathers->insert(this);
    for (i = 0; i < opnd_amt; i++) {
        if (opnds[i]->check_forefathers())
            return true;
    }
    forefathers->erase(this);
    return false;
}

bool MemRefExpr::check_forefathers() {
    for (set<Expression *>::iterator it = forefathers->begin(); it != forefathers->end(); it++)
        if (*it == this)
            return true;
    forefathers->insert(this);
    if (base != NULL && base->check_forefathers())
        return true;
    if (disp != NULL && disp->check_forefathers())
        return true;
    if (index != NULL && index->check_forefathers())
        return true;
    if (scale != NULL && scale->check_forefathers())
        return true;
    forefathers->erase(this);
    return false;
}

bool ITEExpr::check_forefathers() {
    int i;
    for (set<Expression *>::iterator it = forefathers->begin(); it != forefathers->end(); it++)
        if (*it == this)
            return true;
    forefathers->insert(this);
    if (valid->check_forefathers() || invalid->check_forefathers())
        return true;
    for (i = 0; i < condition_opnd_amt; i++) {
        if (condition_opnds[i]->check_forefathers())
            return true;
    }
    forefathers->erase(this);
    return false;
}

bool AddrExpr::check_forefathers() {
    return false;
}

bool ImmedExpr::check_forefathers() {
    return false;
}

bool RegExpr::check_forefathers() {
    return false;
}
#endif

/** Debugging Print Functions */
#ifdef DEBUG_DATA
void Expression::print_debug(file_t fd) {
    this->print_debug(fd, 0);
}

void OperExpr::print_debug(file_t fd, int depth) {
    int i = 0, temp_amt = opnd_amt;
    int dpth = depth;

    dr_fprintf(fd, "%d\n", depth);
    while (dpth--)
        dr_fprintf(fd, "  ");
    dr_fprintf(fd, "%s %p:\n", operation, this);
    while (temp_amt > 1) {
        if (opnds[i] != NULL) {
            dpth = depth;
            while (dpth--)
                dr_fprintf(fd, "  ");
            dr_fprintf(fd, "OPERAND%d:\n", i);
            opnds[i]->print_debug(fd, depth + 1);
        }
        ++i;
        --temp_amt;
    }
    dpth = depth;
    while (dpth--)
        dr_fprintf(fd, "  ");
    dr_fprintf(fd, "OPERAND%d:\n", i);
    opnds[i]->print_debug(fd, depth + 1);
    dr_fprintf(fd, "\n");
}

void RegExpr::print_debug(file_t fd, int depth) {
    while (depth--)
        dr_fprintf(fd, "  ");
    dr_fprintf(fd, "REG %s of size %d\n", get_reg_name(regg), size);
}

void RegNameExpr::print_debug(file_t fd, int depth) {
    while (depth--)
        dr_fprintf(fd, "  ");
    dr_fprintf(fd, "REG %s of size %d\n", get_reg_name(regg), size);
}


void ITEExpr::print_debug(file_t fd, int depth) {
    int dpth = depth, i = 0, temp_amt = condition_opnd_amt;
    dr_fprintf(fd, "CONDITION %s %p:\n", condition, this);
    while (dpth--)
        dr_fprintf(fd, "  ");
    dr_fprintf(fd, "CONDITION %s %p:\n", condition, this);
    while (temp_amt > 1) {
        if (condition_opnds[i] != NULL) {
            dpth = depth;
            while (dpth--)
                dr_fprintf(fd, "  ");
            dr_fprintf(fd, "CONDI_OPND%d:\n", i);
            condition_opnds[i]->print_debug(fd, depth + 1);
            dr_fprintf(fd, ", ");
        }
        ++i;
        --temp_amt;
    }
    dpth = depth;
    while (dpth--)
        dr_fprintf(fd, "  ");
    dr_fprintf(fd, "CONDI_OPND%d:\n", i);
    condition_opnds[i]->print_debug(fd, depth + 1);
    dr_fprintf(fd, "] ? (");
    if (valid != NULL) {
        dpth = depth;
        while (dpth--)
            dr_fprintf(fd, "  ");
        valid->print_debug(fd, depth + 1);
    }
    dr_fprintf(fd, "\n");
    if (invalid != NULL) {
        dpth = depth;
        while (dpth--)
            dr_fprintf(fd, "  ");
        invalid->print_debug(fd, depth + 1);
    }
}

void ImmedExpr::print_debug(file_t fd, int depth) {
    while (depth--)
        dr_fprintf(fd, "  ");
    switch (type) {
        default:
            dr_fprintf(fd, "UNKNOWN_");
        case INT8_TYPE:
        case INT16_TYPE:
        case INT32_TYPE:
        case INT64_TYPE:
            dr_fprintf(fd, "IMMED(%lld : SZ%d)", value.iint, size);
            break;
        case FLOAT_TYPE:
            dr_fprintf(fd, "IMMED(%f : SZ%d)", value.ifloat, size);
            break;
        case DOUBLE_TYPE:
            dr_fprintf(fd, "IMMED(%f : SZ%d)", value.idouble, size);
            break;
        case LODOUBLE_TYPE:
            dr_fprintf(fd, "IMMED(%llf : SZ%d)", value.ilodouble, size);
            break;
    }
    dr_fprintf(fd, "\n");
}

void AddrExpr::print_debug(file_t fd, int depth) {
    while(depth--)
        dr_fprintf(STDERR, "  ");
    if (relative_to == NULL) {
        if (far_segm != UNAVAILABLE_SEGMENT)
            dr_fprintf(fd, "ADDRESS %llx in SEGMENT %d\n", addr, far_segm);
        else
            dr_fprintf(fd, "ADDRESS %llx\n", addr);
        if (!depth)
            dr_fprintf(fd, "\n");
    }
    else {
        dr_fprintf(fd, "ADDRESS NEXTPC: %llx + rel:%llx\n", relative_to, addr);
        if (!depth)
            dr_fprintf(fd, "\n");
    }
}

void MemRefExpr::print_debug(file_t fd, int depth) {
    int dp = depth, dpth;
    dr_fprintf(fd, "BASE-DISP %p, SIZE %d", this, size);
    ++depth;
    if (base != NULL) {
        dpth = dp;
        while (dpth--)
            dr_fprintf(fd, "  ");
        dr_fprintf(fd, "BASE:\n");
        base->print_debug(fd, depth);
    }
    if (disp != NULL) {
        dpth = dp;
        while (dpth--)
            dr_fprintf(fd, "  ");
        dr_fprintf(fd, "DISPLACEMENT:\n");
        disp->print_debug(fd, depth);
    }
    if (index != NULL) {
        dpth = dp;
        while (dpth--)
            dr_fprintf(fd, "  ");
        dr_fprintf(fd, "INDEX:\n");
        index->print_debug(fd, depth);
    }
    if (scale != NULL) {
        dpth = dp;
        while (dpth--)
            dr_fprintf(fd, "  ");
        dr_fprintf(fd, "SCALE:\n");
        scale->print_debug(fd, depth);
    }
}

void FlagsBitExpr::print_debug(file_t fd, int depth) {
    dr_fprintf(fd, "%s", get_flags_name(flags_bit));
    if (!depth)
        dr_fprintf(fd, "\n");
}

void DerefExpr::print_debug(file_t fd, int depth) {
    int dp = depth;
    ++depth;
    while(dp--)
        dr_fprintf(STDERR, "  ");
    dr_fprintf(fd, "DEREFENCE %p, SIZE %d", this, size);
        data->print_debug(fd, depth);
}
#endif

/** Graph Print Functions */
#ifdef DEBUG_GRAPH
void Expression::print_graph_as_final(file_t fd) {
    dr_fprintf(STDERR, "Attempting to print non-final object as final in .dot: ");
#ifdef DEBUG_DATA
    print_debug(STDERR, 0);
#endif
}

void StateNode::print_graph(file_t fd) {
    dr_fprintf(STDERR, "Statenode object does not override, aborting\n");
    dr_abort();
}

void StateNode::print_graph_as_final(file_t fd) {
    dr_fprintf(STDERR, "Statenode object does not override as final, aborting\n");
}

void OperExpr::print_flag_opnd(file_t fd, int edge_dest_fl, Edge *opnd) {
    if (opnd != NULL) {
        dr_fprintf(fd, "a%p->a%p [label=\"%s\" dir=none]\n", this, opnd, get_flags_name(edge_dest_fl));
        opnd->print_graph(fd);
    }
}

void OperExpr::print_graph(file_t fd) {
    if (printed)
        return;
    printed = true;
    int i;
    dr_fprintf(fd, "a%p [label=\"%s\"]\n", this, operation);
    for (i = 0; i < opnd_amt; i++) {
        dr_fprintf(fd, "a%p->a%p [label=\"%d\" dir=none]\n", this, opnds[i], i);
        opnds[i]->print_graph(fd);
    }
    print_flag_opnd(fd, EDGE_DEST_CF, opnds[i + POS_CF]);
    print_flag_opnd(fd, EDGE_DEST_PF, opnds[i + POS_PF]);
    print_flag_opnd(fd, EDGE_DEST_AF, opnds[i + POS_AF]);
    print_flag_opnd(fd, EDGE_DEST_ZF, opnds[i + POS_ZF]);
    print_flag_opnd(fd, EDGE_DEST_SF, opnds[i + POS_SF]);
    print_flag_opnd(fd, EDGE_DEST_DF, opnds[i + POS_DF]);
    print_flag_opnd(fd, EDGE_DEST_OF, opnds[i + POS_OF]);
    print_flag_opnd(fd, EDGE_DEST_TF, opnds[i + POS_TF]);
    print_flag_opnd(fd, EDGE_DEST_IF, opnds[i + POS_IF]);
    print_flag_opnd(fd, EDGE_DEST_NT, opnds[i + POS_NT]);
    print_flag_opnd(fd, EDGE_DEST_RF, opnds[i + POS_RF]);
}

void RegExpr::print_graph(file_t fd) {
    if (printed)
        return;
    printed = true;
    dr_fprintf(fd, "a%p [label=\"%s\"]\n", this, get_reg_name(regg));
}

void RegNameExpr::print_graph(file_t fd) {
    if (printed)
        return;
    printed = true;
    dr_fprintf(fd, "a%p [label=\"%s\"]\n", this, get_reg_name(regg));
}

void RegNameExpr::print_graph_as_final(file_t fd) {
    if (printed_final)
        return;
    printed_final = true;
    dr_fprintf(fd, "a%pfin [label=\"%s i%lu\" style=\"filled\" fillcolor=\"#2676F2\"]\n", this, get_reg_name(regg), getRefId());
}

void FlagsBitExpr::print_graph(file_t fd) {
    if (printed)
        return;
    printed = true;
    dr_fprintf(fd, "a%p [label=\"%s\"]\n", this, get_flags_name(flags_bit));
}

void FlagsBitExpr::print_graph_as_final(file_t fd) {
    if (printed_final)
        return;
    printed_final = true;
    dr_fprintf(fd, "a%pfin [label=\"%s i%lu\" style=\"filled\" fillcolor=\"#76F622\"]\n", this, get_flags_name(flags_bit), getRefId());
}

void ITEExpr::print_graph(file_t fd) {
    int i;
    if (printed)
        return;
    printed = true;
    dr_fprintf(fd, "a%p [label=\"%s %dB\" color=\"blue\"]\n", this, condition, size);
    for (i = 0; i < condition_opnd_amt; i++) {
        dr_fprintf(fd, "a%p->a%p [label=\"%d\" color=\"blue\"]\n", this, condition_opnds[i], i);
        condition_opnds[i]->print_graph(fd);
    }
    if (valid != NULL) {
        dr_fprintf(fd, "a%p->a%p [label=\"valid\" color=\"green\"]\n", this, valid);
        valid->print_graph(fd);
    }
    if (invalid != NULL) {
        dr_fprintf(fd, "a%p->a%p [label=\"invalid\" color=\"red\"]\n", this, invalid);
        invalid->print_graph(fd);
    }
}

void ImmedExpr::print_graph(file_t fd) {
    if (printed)
        return;
    printed = true;
    switch (type) {
        default:
        case INT8_TYPE:
        case INT16_TYPE:
        case INT32_TYPE:
        case INT64_TYPE:
            dr_fprintf(fd, "a%p [label=\"%lld\"]\n", this, value.iint);
            break;
        case FLOAT_TYPE: //FIXME: print bits.
            dr_fprintf(fd, "a%p [label=\"%f\"]\n", this, value.ifloat);
            break;
        case DOUBLE_TYPE: //FIXME: print bits.
            dr_fprintf(fd, "a%p [label=\"%f\"]\n", this, value.idouble);
            break;
        case LODOUBLE_TYPE: //FIXME: print bits.
            dr_fprintf(fd, "a%p [label=\"%llf\"]\n", this, value.ilodouble);
            break;
    }
}

void AddrExpr::print_graph(file_t fd) {
    if (printed)
        return;
    printed = true;
    if (relative_to == NULL)
        dr_fprintf(fd, "a%p [label=\"%p %dB\"]\n", this, addr, size);
    else
        dr_fprintf(fd, "a%p [label=\"From %p: %p %dB\"]\n", this, relative_to, addr, size);
}

void AddrExpr::print_graph_as_final(file_t fd) {
    if (printed)
        return;
    printed = true;
    if (relative_to == NULL)
        dr_fprintf(fd, "a%pfin [label=\"%p %dB\"]\n", this, addr, size);
    else
        dr_fprintf(fd, "a%pfin [label=\"From %p: %p %dB\"]\n", this, relative_to, addr, size);
}

void MemRefExpr::print_graph(file_t fd) {
    if (printed)
        return;
    printed = true;
    dr_fprintf(fd, "a%p [label=\"addr %dB\"]\n", this, size);
    if (base != NULL) {
        dr_fprintf(fd, "a%p->a%p [dir=both label=\"base\" style=dashed]\n", this, base);
        base->print_graph(fd);
    }
    if (scale != NULL) {
        dr_fprintf(fd, "a%p->a%p [dir=both label=\"scale\" style=dashed]\n", this, scale);
        scale->print_graph(fd);
    }
    if (disp != NULL) {
        dr_fprintf(fd, "a%p->a%p [dir=both label=\"disp\" style=dashed]\n", this, disp);
        disp->print_graph(fd);
    }
    if (index != NULL) {
        dr_fprintf(fd, "a%p->a%p [dir=both label=\"index\" style=dashed]\n", this, index);
        index->print_graph(fd);
    }
}

void MemRefExpr::print_graph_as_final(file_t fd) {
    if (printed_final)
        return;
    printed_final = true;
    dr_fprintf(fd, "a%pfin [label=\"addr i%lu\" style=\"filled\" fillcolor=\"#2676F2\"]\n", this, getRefId());
    if (base != NULL) {
        dr_fprintf(fd, "a%pfin->a%p [dir=both label=\"base\" style=dashed]\n", this, base);
        base->print_graph(fd);
    }
    if (scale != NULL) {
        dr_fprintf(fd, "a%pfin->a%p [dir=both label=\"scale\" style=dashed]\n", this, scale);
        scale->print_graph(fd);
    }
    if (disp != NULL) {
        dr_fprintf(fd, "a%pfin->a%p [dir=both label=\"disp\" style=dashed]\n", this, disp);
        disp->print_graph(fd);
    }
    if (index != NULL) {
        dr_fprintf(fd, "a%pfin->a%p [dir=both label=\"index\" style=dashed]\n", this, index);
        index->print_graph(fd);
    }
}

void DerefExpr::print_graph(file_t fd) {
    if (printed)
        return;
    printed = true;
    dr_fprintf(fd, "a%p [label=\"deref %dB\"]\n", this, size);
    if (data != NULL) {
        dr_fprintf(fd, "a%p->a%p\n", this, data);
        data->print_graph(fd);
    }
}

#endif
