#ifndef REFINSTRMAP_HH
#define REFINSTRMAP_HH

#include <list>
#include <set>
#include <map>
#include <stdint.h>
#include <cstdlib>

#include "dr_api.h"
#include "../TreeNode.hh"

using ::std::set;
using ::std::map;
using ::std::list;

class Expression;

template <typename InstrType>
class RefHolder {
    InstrType myrefId;
    public:
    RefHolder() { myrefId = 0; }

    ~RefHolder() {}

    void setRefId(InstrType id) {
        myrefId = id;
    }
    InstrType getRefId() {
        return myrefId;
    }
};

template <class InstrType>
class RefInstrMapper {
};


#endif
