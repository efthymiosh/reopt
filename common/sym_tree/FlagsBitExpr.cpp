#include <climits>
#include <set>
#include <string.h>
#include <stdint.h>

#include "sym_tree.hh"
#include "../ExecutionContext.hh"
#include "../instr_funct.hh"

FlagsBitExpr::FlagsBitExpr(int flags_bit) : flags_bit(flags_bit) {}

FlagsBitExpr::~FlagsBitExpr() {}

void FlagsBitExpr::print(file_t fd, int depth) {
    dr_fprintf(fd, "%s", get_flags_name(flags_bit));
    if (!depth)
        dr_fprintf(fd, "\n");
}

void FlagsBitExpr::add_self(ExecutionContext *ectx, unsigned long instr, Edge* edge) {
    dr_fprintf(STDERR, "Error, attempting to write Initial Register Symbol to final symbols:\n");
    this->print(STDERR, 0);
    dr_abort();
}

uint64_t FlagsBitExpr::getHashCode() {
    return hash = genHash(flags_bit, uid);
}

bool FlagsBitExpr::same_as(Expression *other) {
    if (other == NULL)
        return false;
    if (typeid(*this) != typeid(*other))
        return false;
    FlagsBitExpr *fe_other = (FlagsBitExpr *)other;
    if (fe_other == this)
        return true;
    return fe_other->flags_bit == flags_bit;
}

void FlagsBitExpr::absorbShadowData(Expression *other) {
}

bool FlagsBitExpr::reachable_through_oper(Expression *other, int *allowed_operations) {
    return (Expression*)this == other;
}

void FlagsBitExpr::accept(Visitor *v) {
    v->visit(this);
}

void FlagsBitExpr::accept(ArguVisitor *v, void *arg) {
    v->visit(this, arg);
}

void *FlagsBitExpr::accept(RetArguVisitor *av, void *arg) {
    return av->visit(this, arg);
}

void *FlagsBitExpr::accept(RetVisitor *rv) {
    return rv->visit(this);
}

