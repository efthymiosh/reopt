#include <typeinfo>
#include <inttypes.h>
#include "../FlyweightObject.hh"
#include "sym_tree.hh"
#include "../compile_mode.hh"
#include "../TreeNode.hh"
#include "dr_api.h"

Edge::Edge(int destnum, Expression *data) : destnum(destnum), data(data) {
#ifdef DEBUG_SYM
    if (data == NULL) {
        dr_fprintf(STDERR, "CREATED edge with null data\n");
        dr_abort();
    }
#endif
    printed = false;
}

bool Edge::same_as(Edge *other) {
    if (other == NULL)
        return false;
    if (typeid(*this) != typeid(*other))
        return false;
    if (this == other)
        return true;
    if (destnum != other->destnum) {
        if ((destnum != EDGE_DEST_ANY) && (other->destnum != EDGE_DEST_ANY))
            return false;
    }
    if (!data->same_as(other->data))
        return false;
    return true;
}

void Edge::absorbShadowData(Edge *other) {
    if (other->destnum == EDGE_DEST_ANY) {
        map<unsigned long, TreeNode<unsigned long> *>::iterator mtit = trees.begin();
        for ( ; mtit != trees.end(); mtit++)
            trees[mtit->first] = mtit->second;
    }
}

uint64_t Edge::getHashCode(void) {
    if (!uid)
        return hash;

    //hash = genHash((uint64_t)instr, uid);
    hash = genHash((data == NULL) ? 0 : data->getHashCode(), uid);

    uid = 0;
    return hash;
}

void Edge::print(file_t fd, int depth) {
    if (data != NULL)
        data->print(fd, depth);
    else {
        if (!depth)
            dr_fprintf(fd, "NULL\n");
        else
            dr_fprintf(fd, "NULL");
    }
}

void Edge::print(file_t fd) {
    if (data != NULL)
        data->print(fd);
    else
            dr_fprintf(fd, "NULL\n");
}

#ifdef DEBUG_GRAPH
void Edge::print_graph(file_t fd) {
    if (printed)
        return;
    printed = true;
    if (trees.size() == 0)
      dr_fprintf(fd, "a%p [shape=\"rectangle\" label=\"-\"]\n", this);
    else {
        map<unsigned long, TreeNode<unsigned long> *>::iterator sit = trees.begin();
        dr_fprintf(fd, "a%p [shape=\"rectangle\" label=\"", this);
        for ( ; sit != trees.end(); ++sit) {
            dr_fprintf(fd, "i%lu\\n", *sit);
        }
        dr_fprintf(fd, "\"]\n");
    }
    if (data != NULL) {
        dr_fprintf(fd, "a%p->a%p\n", this, data);
        data->print_graph(fd);
    }
}
#endif

#ifdef DEBUG_DATA
void Edge::print_debug(file_t fd, int depth) {
    dr_fprintf(fd, "E:");
    data->print_debug(fd, depth);
}

void printULongInstr(file_t fd, unsigned long instr) {
    dr_fprintf(fd, " i%lu", instr);
}

void Edge::print_deps(file_t fd) {
    map<unsigned long, TreeNode<unsigned long> *>::iterator mtit = trees.begin();
    while (mtit != trees.end()) {
        dr_fprintf(fd, "i%lu: ", mtit->first);
        if (mtit->second != NULL)
            mtit->second->print_debug(fd, printULongInstr);
        dr_fprintf(fd, "\n");
        ++mtit;
    }
}
#endif

void Edge::accept(Visitor *v) {
    v->visit(this);
}

void Edge::accept(ArguVisitor *av, void *arg) {
    av->visit(this, arg);
}

void *Edge::accept(RetArguVisitor *av, void *arg) {
    return av->visit(this, arg);
}

void *Edge::accept(RetVisitor *rv) {
    return rv->visit(this);
}

void Edge::insertRef(RefHolder<unsigned long> *rh) {
    unsigned long refId = rh->getRefId();

    trees[refId] = new TreeNode<unsigned long>(this);
    trees[refId]->insertData(refId);
}

TreeNode<unsigned long> *Edge::getTree(unsigned long refId) {
    return trees[refId];
}

void Edge::linkDependency(Edge *other, unsigned long instr, unsigned long dep) {
    if (instr == 0 || dep == 0) {
        return;
    }
    if (trees.find(instr) == trees.end()) {
        dr_fprintf(STDERR, "linkDep -- Unable to find instr %lu in this instrmapper: ", instr);
        map<unsigned long, TreeNode<unsigned long> *>::iterator mtit = trees.begin();
        for ( ; mtit != trees.end(); mtit++)
            dr_fprintf(STDERR, " %lu", mtit->first);
        dr_fprintf(STDERR, "\n");
        dr_abort();
    }
    if (other->trees.find(dep) == other->trees.end()) {
        dr_fprintf(STDERR, "linkDep -- Unable to find instr %lu in other instrmapper(instr %lu): ", dep, instr);
        map<unsigned long, TreeNode<unsigned long> *>::iterator mtit = other->trees.begin();
        for ( ; mtit != other->trees.end(); mtit++)
            dr_fprintf(STDERR, " %lu", mtit->first);
        dr_fprintf(STDERR, "\n");
        dr_abort();
    }
    trees[instr]->insertChild(other->trees[dep]);
}

void Edge::linkMovDependency(Edge *other, unsigned long instr, unsigned long dep) {
    if (instr == 0 || dep == 0) {
        return;
    }
    if (trees.find(instr) == trees.end()) {
        dr_fprintf(STDERR, "linkMovDep -- Unable to find instr %lu in this instrmapper: ", instr);
        map<unsigned long, TreeNode<unsigned long> *>::iterator mtit = trees.begin();
        for ( ; mtit != trees.end(); mtit++)
            dr_fprintf(STDERR, " %lu", mtit->first);
        dr_fprintf(STDERR, "\n");
        dr_abort();
    }
    if (other->trees.find(dep) == other->trees.end()) {
        dr_fprintf(STDERR, "linkMovDep -- Unable to find instr %lu in other instrmapper(instr %lu): ", dep, instr);
        map<unsigned long, TreeNode<unsigned long> *>::iterator mtit = other->trees.begin();
        for ( ; mtit != other->trees.end(); mtit++)
            dr_fprintf(STDERR, " %lu", mtit->first);
        dr_fprintf(STDERR, "\n");
        dr_abort();
    }
    trees[instr]->insertChild(other->trees[dep]);
    movDeps.insert(dep);
}

void Edge::linkOpndCalcDependency(Edge *other, unsigned long instr, unsigned long dep) {
    if (instr == 0 || dep == 0) {
        return;
    }
    if (trees.find(instr) == trees.end()) {
        dr_fprintf(STDERR, "linkOpndDep -- Unable to find opnddep instr %lu in this instrmapper: ", instr);
        map<unsigned long, TreeNode<unsigned long> *>::iterator mtit = trees.begin();
        for ( ; mtit != trees.end(); mtit++)
            dr_fprintf(STDERR, " %lu", mtit->first);
        dr_abort();
    }
    if (other->trees.find(dep) == other->trees.end()) {
        dr_fprintf(STDERR, "linkOpndDep -- Unable to find opnddep instr %lu in other instrmapper(instr %lu): ", dep, instr);
        map<unsigned long, TreeNode<unsigned long> *>::iterator mtit = other->trees.begin();
        for ( ; mtit != other->trees.end(); mtit++)
            dr_fprintf(STDERR, " %lu", mtit->first);
        dr_abort();
    }
    trees[instr]->insertCalcChild(other->trees[dep]);
}

bool Edge::evalDependsOn(unsigned long instr, unsigned long dependency) {
    if (trees.find(instr) == trees.end())
        return false;
    return (trees[instr])->subTreeContains(dependency);
}

bool Edge::containsRefOf(RefHolder<unsigned long> *rh) {
    return trees.find(rh->getRefId()) != trees.end();
}
