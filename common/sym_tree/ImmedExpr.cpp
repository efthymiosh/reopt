#include <climits>
#include <set>
#include <string.h>
#include <stdint.h>

#include "sym_tree.hh"
#include "../ExecutionContext.hh"
#include "../instr_funct.hh"

ImmedExpr::ImmedExpr(int64_t immediate, int size) {
    this->size = size;
    value.iint = immediate;
    switch (size) {
        case 1:
            type = INT8_TYPE;
            break;
        case 2:
            type = INT16_TYPE;
            break;
        case 4:
            type = INT32_TYPE;
            break;
        case 8:
            type = INT64_TYPE;
            break;
        case 0:
            type = SCALE_TYPE;
            break;
        default:
            type = UNKNOWN_TYPE;
    }
}

ImmedExpr::ImmedExpr(float immediate, int size) {
    value.ifloat = immediate;
    type = FLOAT_TYPE;
}

ImmedExpr::ImmedExpr(double immediate, int size) {
    this->size = size;
    value.idouble = immediate;
    type = DOUBLE_TYPE;
}

ImmedExpr::ImmedExpr(long double immediate, int size) {
    this->size = size;
    value.ilodouble = immediate;
    type = LODOUBLE_TYPE;
}

ImmedExpr::~ImmedExpr() {
}

void ImmedExpr::print(file_t fd, int depth) {
    switch (type) {
        default:
        case INT8_TYPE:
        case INT16_TYPE:
        case INT32_TYPE:
        case INT64_TYPE:
            dr_fprintf(fd, "$%lld", value.iint);
            break;
        case FLOAT_TYPE:
            dr_fprintf(fd, "$%f", value.ifloat);
            break;
        case DOUBLE_TYPE:
            dr_fprintf(fd, "$%f", value.idouble);
            break;
        case LODOUBLE_TYPE:
            dr_fprintf(fd, "$%llf", value.ilodouble);
            break;
    }
    if (!depth)
        dr_fprintf(fd, "\n");
}

void ImmedExpr::add_self(ExecutionContext *ectx, unsigned long instr, Edge* my_expr) {
    dr_fprintf(STDERR, "Error, attempting to write Immediate Expression to final symbols..\n");
    dr_abort();
}

uint64_t ImmedExpr::getHashCode(void) {
    if (!uid)
        return hash;

    hash = genHash(this->value.iint, this->uid);

    uid = 0;
    return hash;
}

bool ImmedExpr::reachable_through_oper(Expression *other, int *allowed_operations) {
    return (Expression*)this == other;
}

bool ImmedExpr::same_as(Expression *other) {
    if (other == NULL)
        return false;
    if (typeid(*this) != typeid(*other))
        return false;
    ImmedExpr *ie_other = (ImmedExpr *) other;
    if (this == ie_other)
        return true;
    return (value.iint == ie_other->value.iint) && (type == ie_other->type);
}

void ImmedExpr::absorbShadowData(Expression *other) {
}

void ImmedExpr::accept(Visitor *v) {
    v->visit(this);
}

void ImmedExpr::accept(ArguVisitor *av, void *arg) {
    av->visit(this, arg);
}

void *ImmedExpr::accept(RetArguVisitor *av, void *arg) {
    return av->visit(this, arg);
}

void *ImmedExpr::accept(RetVisitor *rv) {
    return rv->visit(this);
}

