#include <climits>
#include <set>
#include <string.h>
#include <stdint.h>

#include "sym_tree.hh"
#include "../ExecutionContext.hh"
#include "../instr_funct.hh"

RegExpr::RegExpr(reg_id_t reg) : regg(reg) {
    size = opnd_size_in_bytes(regg);
}

RegExpr::RegExpr(RegNameExpr *pExpr) {
    regg = pExpr->getRegg();
    size = pExpr->get_size();
}

RegExpr::~RegExpr() {
}

void RegExpr::print(file_t fd, int depth) {
    dr_fprintf(fd, "%s", get_reg_name(regg));
    if (!depth)
        dr_fprintf(fd, "\n");
}

void RegExpr::add_self(ExecutionContext *ectx, unsigned long instr, Edge* edge) {
    dr_fprintf(STDERR, "Error, attempting to write Initial Register Symbol to final symbols:\n");
    this->print(STDERR, 0);
    dr_abort();
}

uint64_t RegExpr::getHashCode() {
    uint64_t h = genHash((uint64_t) regg, (uint64_t) get_size());
    return hash = genHash(h, uid);
}

bool RegExpr::same_as(Expression *other) {
    if (other == NULL)
        return false;
    if (typeid(*this) != typeid(*other))
        return false;
    RegExpr *re_other = (RegExpr *)other;
    if (re_other == this)
        return true;
    return regg == re_other->regg && size == re_other->size;
}

void RegExpr::absorbShadowData(Expression *other) {
}

bool RegExpr::reachable_through_oper(Expression *other, int *allowed_operations) {
    return (Expression*)this == other;
}

void RegExpr::accept(Visitor *v) {
    v->visit(this);
}

void *RegExpr::accept(RetArguVisitor *av, void *arg) {
    return av->visit(this, arg);
}

void RegExpr::accept(ArguVisitor *v, void *arg) {
    v->visit(this, arg);
}

void *RegExpr::accept(RetVisitor *rv) {
    return rv->visit(this);
}
