#include "ExecutionContext.hh"
#include "instr_funct.hh"

alias_addrstats_t *init_alias_addrstats(void) {
    alias_addrstats_t *stats;
    stats = (alias_addrstats_t *) malloc(sizeof(alias_addrstats_t));
    stats->li = new list<Expression*>();
    stats->discoverable = 0;
    stats->maybe_discoverable = 0;
    stats->not_discoverable = 0;
    stats->waw = 0;
    stats->war = 0;
    stats->raw = 0;
    stats->rar = 0;
    return stats;
}

void alias_addrstats_add(alias_addrstats_t *stats, Expression *expr, bool is_dst) {
    Expression *oldfirst = NULL;
    list<Expression *>::iterator it = stats->li->begin();
    if (it != stats->li->end()) {
        oldfirst = *(it);
        if (stats->prev_was_dst) {
            if (is_dst)
                ++stats->waw;
            else
                ++stats->raw;
        }
        else {
            if (is_dst)
                ++stats->war;
            else
                ++stats->rar;
        }
    }
    stats->li->push_front(expr);
    stats->prev_was_dst = is_dst;
    return;
}

#define INSTRUMENT_OFF 0
#define INSTRUMENT_CHECK_ON 2
#define INSTRUMENT_ON 1
#define INSTRUMENT_CHECK_OFF 3

bool debug_instrument_ok_nop(instrlist_t *bb) {
    static int status = INSTRUMENT_OFF;
    instr_t *first_instr, *second_instr;
    first_instr = instrlist_first(bb);
    second_instr = instr_get_next(first_instr);

    switch (status) {
        case INSTRUMENT_OFF:
            if (instr_get_opcode(first_instr) == OP_nop_modrm && (opnd_get_addr(instr_get_src(first_instr, 0)) == (void *) 0x13371337)) {
                status = INSTRUMENT_ON;
                return true;
            }
            return false;
        case INSTRUMENT_ON:
            if (instr_get_opcode(first_instr) == OP_nop_modrm && (opnd_get_addr(instr_get_src(first_instr, 0)) == (void *) 0x13371337)) {
                status = INSTRUMENT_OFF;
                return false;
            }
            return true;
    }
}
