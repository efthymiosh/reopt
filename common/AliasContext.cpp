#include "AliasContext.hh"

void AliasContext::initObject() {
    ExecutionContext::initObject();
    aliasedExprSet = new set<set<Expression *> *>();
    aliasedDestinations = new set<Expression *>();
    aliasedDestsOrdered = new list<Expression *>();
}

void AliasContext::finiObject() {
    ExecutionContext::finiObject();
    delete aliasedExprSet;
}

/**
 * This function decides the relation between two expressions in regards to aliasing
 * May return ALIAS, NO_ALIAS, MAY_ALIAS, MAY_ALIAS__UNDER_THRESHOLD;
 */
AliasEnum AliasContext::decideAlias(Expression *expr1, Expression *expr2) {
    /* TODO: IMPLEMENT ORACLE */
    return MAY_ALIAS__UNDER_THRESHOLD;
    //return MAY_ALIAS;
}

set<Expression *> *AliasContext::getExprAliases(Expression *expr) {
    set<set<Expression *> *> *inSets = new set<set<Expression *> *>();
    set<Expression *> *aliasedDests = new set<Expression *>();
    set<set<Expression *> *>::iterator ssit = aliasedExprSet->begin();
    for ( ; ssit != aliasedExprSet->end(); ssit++) {
        set<Expression *> *grp = *ssit;
        set<Expression *>::iterator sit = grp->begin();
        for ( ; sit != grp->end(); sit++) {
            switch (decideAlias(*sit, expr)) {
                case ALIAS:
                case MAY_ALIAS:
                    inSets->insert(grp);
                    break;
                case MAY_ALIAS__UNDER_THRESHOLD:
                case NO_ALIAS:
                    continue;
            }
            break;
        }
    }
    ssit = inSets->begin();
    for ( ; ssit != inSets->end(); ssit++) {
        set<Expression *> *grp = *ssit;
        grp->insert(expr);
        set<Expression *>::iterator sit = grp->begin();
        for ( ; sit != grp->end(); sit++) {
            aliasedDests->insert(*sit);
        }
    }
    delete inSets;
    return aliasedDests;
}

Expression *AliasContext::currentSymExprForMemRef(Expression *expr, uint size) {
    /*
     * Since the operand correlating to the expression reads memory, the symbolic expression expr
     * is rebuilt as an ITE branching its initial symbolic value and the symbolic value of Symbolic Expressions
     * denoting existing writes to memory *which may be aliases of the memory address the expression evaluates to*.
     * The concrete execution ordering of the symbolic expressions evaluating write
     * addresses is preserved by the used list type. The ordering of the ITEs
     * and therefore the symbolic expressions that produce them affects the validity
     * of the expression.
     */
    Expression *initExpr = expr;
    set<Expression *> *aliasedDests = getExprAliases(expr);

    list<Expression *>::iterator lit;
    lit = aliasedDestsOrdered->begin();

    expr = ExecutionContext::currentSymExprForMemRef(expr, size);
    for (; lit != aliasedDestsOrdered->end(); lit++) {
        if (aliasedDests->find(*lit) != aliasedDests->end()) {
            Expression **assertOpnds = new Expression *[2];
            Expression *validTree = (*final_expr_symbols)[*lit];
            Expression *invalidTree = expr;
            assertOpnds[0] = initExpr;
            assertOpnds[1] = *lit;
            expr = fwfITEExpr->getFWObj(new ITEExpr(CONDITION_EQ, assertOpnds, 2, validTree, invalidTree));
            initExpr = ExecutionContext::currentSymExprForMemRef(expr, size);
        }
    }
    return expr;
}

void AliasContext::countMemoryWrite(opnd_t operand, Expression * expr) {
    ExecutionContext::countMemoryWrite(operand, expr);
    /*
     * If the instruction performs a write, the symbolic expression "self-propagates" over previous
     * (ordered by execution sequence) symbolic expressions derived by writes, building ITEs for them.
     */
    set<Expression *> *aliasedDests = getExprAliases(expr);

    if (aliasedDests->size() == 0) {
        set<Expression *> *sn = new set<Expression *, exprComparator>();
        sn->insert(expr);
        aliasedExprSet->insert(sn);
    }
    /* if-then-else alias handling */
    list<Expression *>::iterator lit;
    Expression *symExpr = (*final_expr_symbols)[expr];
    for (lit = aliasedDestsOrdered->begin(); lit != aliasedDestsOrdered->end(); lit++) {
        if (aliasedDests->find(*lit) != aliasedDests->end()) {
            Expression **assertOpnds = new Expression *[2];
            Expression *validTree = symExpr;
            Expression *invalidTree = (*final_expr_symbols)[*lit];
            assertOpnds[0] = expr;
            assertOpnds[1] = *lit;
            Expression *ite = fwfITEExpr->getFWObj(new ITEExpr(CONDITION_EQ, assertOpnds, 2, validTree, invalidTree));
            addSymAddrExpr(*lit, ite);
        }
    }
    if (aliasedDestinations->insert(expr).second) { //if successfully inserted.
        aliasedDestsOrdered->push_back(expr);
    }
}

void AliasContext::calculate_alias_stats() {
}

Expression *AliasContext::createMemRefFromOpnd(opnd_t concrete_opnd, uint size) {
    Expression *expr = ExecutionContext::createMemRefFromOpnd(concrete_opnd, size);

    return expr;
}
