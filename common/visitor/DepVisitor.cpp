#include <limits.h>


#include "Visitor.hh"
#include "DepVisitor.hh"
#include "../sym_tree/References.hh"
#include "../sym_tree/sym_tree.hh"

DepVisitor::DepVisitor(map<unsigned long, Annotations> *annots) : annots(annots) {}

DepVisitor::~DepVisitor() {}

void DepVisitor::visit(MemRefExpr *v) {
    if (v->base != NULL)
        v->base->accept(this);
    if (v->index != NULL)
        v->index->accept(this);
}

void DepVisitor::visit(OperExpr *v) {
    int i;
    for (i = 0; i < v->opnd_amt; i++)
        v->opnds[i]->accept(this);
    for ( ; i < EFLAGS_AMT; ++i) {
        if (v->opnds[i])
            v->opnds[i]->accept(this);
    }
}

void DepVisitor::visit(Edge *v) {
    if (visited.find(v) != visited.end())
        return;
    if (v->trees.size() > 1) {
        unsigned long *cse = new unsigned long[1];
        *cse = 0;
        map<unsigned long, TreeNode<unsigned long> *>::iterator mit;
        for (mit = v->trees.begin(); mit != v->trees.end(); mit++)
            (*annots)[mit->first].cse = cse;
    }
    visited.insert(v);

    v->data->accept(this);
}

void DepVisitor::visit(Expression *v) {
    v->accept(this);
}

void DepVisitor::visit(DerefExpr *v) {
    v->data->accept(this);
}

void DepVisitor::visit(ITEExpr *v) {
    return;
}

void DepVisitor::visit(RegExpr *v) {
    return;
}

void DepVisitor::visit(RegNameExpr *v) {
    return;
}

void DepVisitor::visit(AddrExpr *v) {
    return;
}

void DepVisitor::visit(ImmedExpr *v) {
    return;
}

void DepVisitor::visit(FlagsBitExpr *v) {
    return;
}

