#include <limits.h>


#include "Visitor.hh"
#include "OperCountVisitor.hh"
#include "../sym_tree/References.hh"
#include "../sym_tree/sym_tree.hh"

OperCountVisitor::OperCountVisitor() {
    operCount = 0;
    movCount = 0;
    freqMap = new map<int, unsigned int>();
}

OperCountVisitor::~OperCountVisitor() {}

void *OperCountVisitor::visit(MemRefExpr *v) {
    if (v->base != NULL)
        v->base->accept(this);
    if (v->index != NULL)
        v->index->accept(this);
}

void *OperCountVisitor::visit(OperExpr *v) {
    int i;
    if (visitedOper.find(v) != visitedOper.end())
        return (void *)0;
    visitedOper.insert(v);
    (*freqMap)[v->opcode] += 1;
    ++operCount;
    for (i = 0; i < v->opnd_amt; i++)
        v->opnds[i]->accept(this);
    for ( ; i < EFLAGS_AMT; ++i) {
        if (v->opnds[i])
            v->opnds[i]->accept(this);
    }
}

void *OperCountVisitor::visit(Edge *v) {
    if (visitedEdge.find(v) != visitedEdge.end())
        return (void *)0;
    visitedEdge.insert(v);
    movCount += v->movDeps.size();
    v->data->accept(this);
}

void *OperCountVisitor::visit(Expression *v) {
    v->accept(this);
}

void *OperCountVisitor::visit(DerefExpr *v) {
    v->data->accept(this);
}

void *OperCountVisitor::visit(ITEExpr *v) {
    return (void *)0;
}

void *OperCountVisitor::visit(RegExpr *v) {
    return (void *)0;
}

void *OperCountVisitor::visit(RegNameExpr *v) {
    return (void *)0;
}

void *OperCountVisitor::visit(AddrExpr *v) {
    return (void *)0;
}

void *OperCountVisitor::visit(ImmedExpr *v) {
    return (void *)0;
}

void *OperCountVisitor::visit(FlagsBitExpr *v) {
    return (void *)0;
}
