#ifndef DEPVISITOR_HH
#define DEPVISITOR_HH

#include "Visitor.hh"

#include <map>
#include <set>
#include <list>
#include <stdint.h>

#include "dr_api.h"

using ::std::map;
using ::std::set;
using ::std::list;

struct DepVisitor : public Visitor {
    map<Edge *, unsigned long> *colorMap;
    unsigned long nextColor;
    unsigned long currColor;

    set<Edge *> visited;

    DepVisitor(map<uint64_t, set<uint64_t> *> *depGraph);
    ~DepVisitor();

    virtual void visit(Expression *);
    virtual void visit(AddrExpr *);
    virtual void visit(DerefExpr *);
    virtual void visit(ImmedExpr *);
    virtual void visit(ITEExpr *);
    virtual void visit(MemRefExpr *);
    virtual void visit(OperExpr *);
    virtual void visit(RegExpr *);
    virtual void visit(RegNameExpr *);
    virtual void visit(Edge *);
    virtual void visit(FlagsBitExpr *);
};

#endif
