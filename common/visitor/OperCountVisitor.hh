#ifndef OPERCOUNTVISITOR_HH
#define OPERCOUNTVISITOR_HH

#include "Visitor.hh"

#include <map>
#include <set>
#include <list>
#include <stdint.h>

#include "dr_api.h"
#include "../instr_funct.hh"

using ::std::map;
using ::std::set;
using ::std::list;

struct OperCountVisitor : public RetVisitor {
    set<Edge *> visitedEdge;
    set<OperExpr *> visitedOper;
    map<int, unsigned int> *freqMap;
    map<Expression *, unsigned long> *heightMap;
    unsigned long operCount;
    unsigned long movCount;

    OperCountVisitor();
    ~OperCountVisitor();

    virtual void *visit(Expression *);
    virtual void *visit(AddrExpr *);
    virtual void *visit(DerefExpr *);
    virtual void *visit(ImmedExpr *);
    virtual void *visit(ITEExpr *);
    virtual void *visit(MemRefExpr *);
    virtual void *visit(OperExpr *);
    virtual void *visit(RegExpr *);
    virtual void *visit(RegNameExpr *);
    virtual void *visit(Edge *);
    virtual void *visit(FlagsBitExpr *);

};

#endif
