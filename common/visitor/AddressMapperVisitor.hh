#ifndef ADDRESSMAPPERVISITOR_HH
#define ADDRESSMAPPERVISITOR_HH

#include <map>
#include <set>
#include <list>
#include <stdint.h>

#include "Visitor.hh"
#include "../pconst/OffsetNode.hh"
#include "../FlyweightFactory.hh"

#include "dr_api.h"

using ::std::map;
using ::std::set;
using ::std::list;

struct AddressMapperVisitor : public RetArguVisitor {
    map<Expression *, OffsetNode *> offsetMap;
    map<OffsetNode *, StateNode *> inverseOffsetMap;
    FlyweightFactory<OffsetNode> onFac;

    AddressMapperVisitor();
    ~AddressMapperVisitor();

    virtual void *visit(Expression *, void *arg);
    virtual void *visit(AddrExpr *, void *arg);
    virtual void *visit(DerefExpr *, void *arg);
    virtual void *visit(ImmedExpr *, void *arg);
    virtual void *visit(ITEExpr *, void *arg);
    virtual void *visit(MemRefExpr *, void *arg);
    virtual void *visit(OperExpr *, void *arg);
    virtual void *visit(RegExpr *, void *arg);
    virtual void *visit(RegNameExpr *, void *arg);
    virtual void *visit(Edge *, void *arg);
    virtual void *visit(FlagsBitExpr *, void *arg);

    StateNode *getSingleForm(StateNode *mre);
};

#endif
