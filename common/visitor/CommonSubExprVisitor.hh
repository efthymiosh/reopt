#ifndef COMMONSUBEXPRVISITOR_HH
#define COMMONSUBEXPRVISITOR_HH

#include "Visitor.hh"

#include <map>
#include <set>
#include <list>
#include <stdint.h>

#include "dr_api.h"
#include "../TreeNode.hh"
#include "../instr_funct.hh"

using ::std::map;
using ::std::set;
using ::std::list;

struct CommonSubExprVisitor : public Visitor {

    set<Edge *> visited;
    map<unsigned long, Annotations> *annots;

    CommonSubExprVisitor(map<unsigned long, Annotations> *annots);
    ~CommonSubExprVisitor();
    virtual void visit(Expression *);
    virtual void visit(AddrExpr *);
    virtual void visit(DerefExpr *);
    virtual void visit(ImmedExpr *);
    virtual void visit(ITEExpr *);
    virtual void visit(MemRefExpr *);
    virtual void visit(OperExpr *);
    virtual void visit(RegExpr *);
    virtual void visit(RegNameExpr *);
    virtual void visit(Edge *);
    virtual void visit(FlagsBitExpr *varg);

    void init(Edge *);
};

#endif //COMMONSUBEXPRVISITOR_HH
