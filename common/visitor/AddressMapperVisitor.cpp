#include "Visitor.hh"
#include "AddressMapperVisitor.hh"
#include "../sym_tree/References.hh"
#include "../sym_tree/sym_tree.hh"

AddressMapperVisitor::AddressMapperVisitor() {
}

AddressMapperVisitor::~AddressMapperVisitor() {
}

void *AddressMapperVisitor::visit(MemRefExpr *v, void *arg) {
    OffsetNode *on, *onmine = offsetMap[v];
    if (onmine == NULL) {
        /* Before doing anything, check with the scale. If it's something that requires multiplication,
         * don't bother with the memory reference: handle it as an unknown
         */
        onmine = new OffsetNode();
        if (v->index != NULL) {
            if (v->scale != NULL) {
                if (((ImmedExpr *)v->scale)->value.iint != 1) {
                    onmine->additions.insert(v);
                    offsetMap[v] = onmine = onFac.getFWObj(onmine);
                    if (inverseOffsetMap[onmine] == NULL)
                        inverseOffsetMap[onmine] = v;
                    return (void *) onmine;
                }
            }
            on = (OffsetNode *) v->index->accept(this, arg);
            onmine->add(on);
        }
        if (v->base != NULL) {
            on = (OffsetNode *) v->base->accept(this, arg);
            onmine->add(on);
        }
        if (v->disp != NULL)
            onmine->offset += ((ImmedExpr *)v->disp)->value.iint;
        onmine = onFac.getFWObj(onmine);
        offsetMap[v] = onmine;
    }
    if (inverseOffsetMap[onmine] == NULL)
        inverseOffsetMap[onmine] = v;
    return (void *) onmine;
}

void *AddressMapperVisitor::visit(OperExpr *v, void *arg) {
    int i;
    OffsetNode *on; 
    if (offsetMap.find(v) != offsetMap.end())
        return offsetMap[v];
    OffsetNode *onmine = new OffsetNode();
    switch (v->opcode) {
        case OP_sub:
            on = (OffsetNode *)v->opnds[0]->accept(this, arg);
            onmine->subtract(on);
            on = (OffsetNode *)v->opnds[1]->accept(this, arg);
            onmine->add(on);
            break;
        case OP_push:
        case OP_pop:
        case OP_call:
        case OP_ret:
        case OP_add:
            on = (OffsetNode *)v->opnds[0]->accept(this, arg);
            onmine->add(on);
            on = (OffsetNode *)v->opnds[1]->accept(this, arg);
            onmine->add(on);
            break;
        default:
            onmine->additions.insert((Expression *)arg);
            /* ^-- bad cast, terrible programming, arg is Edge*. doesn't matter, as long
             * as it uniquely identifies this OperExpr as a result of the destination operand
             * uniquely described in the Edge* "arg" leading to it.
             */
    }
    offsetMap[v] = onmine = onFac.getFWObj(onmine);
    return onmine;
}

void *AddressMapperVisitor::visit(Edge *v, void *arg) {
    return v->data->accept(this, (void *)v);
}

void *AddressMapperVisitor::visit(Expression *v, void *arg) {
    return v->accept(this, arg);
}

void *AddressMapperVisitor::visit(DerefExpr *v, void *arg) {
    if (offsetMap[v] == NULL) {
        OffsetNode *on = new OffsetNode();
        on->additions.insert(v);
        offsetMap[v] = onFac.getFWObj(on);
    }
    return offsetMap[v];
}

void *AddressMapperVisitor::visit(ITEExpr *v, void *arg) {
    if (offsetMap[v] == NULL) {
        OffsetNode *on = new OffsetNode();
        on->additions.insert(v);
        offsetMap[v] = onFac.getFWObj(on);
    }
    return offsetMap[v];
}

void *AddressMapperVisitor::visit(RegExpr *v, void *arg) {
    if (offsetMap[v] == NULL) {
        OffsetNode *on = new OffsetNode();
        on->additions.insert(v);
        offsetMap[v] = onFac.getFWObj(on);
    }
    return offsetMap[v];
}

void *AddressMapperVisitor::visit(RegNameExpr *v, void *arg) {
    dr_fprintf(STDERR, "AddressMapperVisitor visiting RegNameExpr? Aborting.\n");
    dr_abort();
    return NULL;
}

void *AddressMapperVisitor::visit(AddrExpr *v, void *arg) {
    if (offsetMap[v] == NULL) {
        OffsetNode *on = new OffsetNode();
        on->additions.insert(v);
        offsetMap[v] = onFac.getFWObj(on);
    }
    return offsetMap[v];
}

void *AddressMapperVisitor::visit(ImmedExpr *v, void *arg) {
    if (offsetMap[v] == NULL) {
        OffsetNode *on = new OffsetNode();
        on->offset += v->value.iint;
        offsetMap[v] = onFac.getFWObj(on);
    }
    return offsetMap[v];
}

void *AddressMapperVisitor::visit(FlagsBitExpr *v, void *arg) {
    if (offsetMap[v] == NULL) {
        OffsetNode *on = new OffsetNode();
        on->additions.insert(v);
        offsetMap[v] = onFac.getFWObj(on);
    }
    return offsetMap[v];
}

StateNode *AddressMapperVisitor::getSingleForm(StateNode *mre) {
    OffsetNode *onode = offsetMap[(Expression *) mre];
    if (onode == NULL)
        onode = (OffsetNode *) visit(mre, NULL);
    StateNode *ret = inverseOffsetMap[onode];
    if (ret == NULL)
        ret = inverseOffsetMap[onode] = mre;
    return ret;
}
