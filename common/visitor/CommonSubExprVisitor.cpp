#include "Visitor.hh"
#include "CommonSubExprVisitor.hh"
#include "../sym_tree/References.hh"
#include "../sym_tree/sym_tree.hh"


CommonSubExprVisitor::CommonSubExprVisitor(map<unsigned long, Annotations> *annots) : annots(annots) {
}


CommonSubExprVisitor::~CommonSubExprVisitor() {}

void CommonSubExprVisitor::init(Edge *v) {
    v->accept(this);
}

void CommonSubExprVisitor::visit(MemRefExpr *v) {
    if (v->base != NULL)
        v->base->accept(this);
    if (v->index != NULL)
        v->index->accept(this);
}

void CommonSubExprVisitor::visit(OperExpr *v) {
    int i;
    for (i = 0; i < v->opnd_amt; i++) {
        v->opnds[i]->accept(this);
    }
    for ( ; i < EFLAGS_AMT; ++i) {
        if (v->opnds[i])
            v->opnds[i]->accept(this);
    }
}

static unsigned long aliveDAGs(Edge *v, unsigned long *firstAlive) {
    int i = 0;
    *firstAlive = 0;
    map<unsigned long, TreeNode<unsigned long> *>::iterator mit;
    mit = v->trees.begin();
    while (mit != v->trees.end()) {
        if (mit->second->annot && v->movDeps.find(mit->first) == v->movDeps.end()) {
            ++i;
            if (!*firstAlive)
                *firstAlive = mit->first;
        }
        ++mit;
    }
    return i;
}

void CommonSubExprVisitor::visit(Edge *v) {
    if (visited.find(v) != visited.end())
        return;
    visited.insert(v);
    unsigned long first;
    unsigned long refs;
    if ((refs = aliveDAGs(v, &first)) > 1) {
        (*annots)[first].saved = 1;
        return;
    }
    v->data->accept(this);
    return;
}

void CommonSubExprVisitor::visit(Expression *v) {
    v->accept(this);
}

void CommonSubExprVisitor::visit(DerefExpr *v) {
    v->data->accept(this);
}

void CommonSubExprVisitor::visit(FlagsBitExpr *v) {
    return;
}

void CommonSubExprVisitor::visit(ITEExpr *v) {
    return;
}

void CommonSubExprVisitor::visit(RegExpr *v) {
    return;
}

void CommonSubExprVisitor::visit(RegNameExpr *v) {
    return;
}

void CommonSubExprVisitor::visit(AddrExpr *v) {
    return;
}

void CommonSubExprVisitor::visit(ImmedExpr *v) {
    return;
}
