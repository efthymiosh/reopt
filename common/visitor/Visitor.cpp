#include "Visitor.hh"
#include "WhitelistVisitor.hh"
#include "../sym_tree/References.hh"
#include "../sym_tree/sym_tree.hh"

WhitelistVisitor::WhitelistVisitor() {
    whitelist = new set<unsigned long>();
}

WhitelistVisitor::~WhitelistVisitor() {
}

void WhitelistVisitor::visit(MemRefExpr *v, void *arg) {
    if (v->base != NULL)
        v->base->accept(this, arg);
    if (v->index != NULL)
        v->index->accept(this, arg);
}

void WhitelistVisitor::visit(OperExpr *v, void *arg) {
    for (int i = 0; i < v->opnd_amt; i++)
        v->opnds[i]->accept(this, arg);
}

void WhitelistVisitor::visit(Edge *v, void *arg) {
    set<uint64_t> *deps = (set<uint64_t> *) arg;
    RefInstrMapper<unsigned long> *rimap = (RefInstrMapper<unsigned long> *)v;
    set<uint64_t>::iterator sit = deps->begin();
    for ( ; sit != deps->end(); sit++) {
        whitelist->insert(*sit);
    }
    v->data->accept(this, arg);
}

void WhitelistVisitor::visit(Expression *v, void *arg) {
    v->accept(this, arg);
}

void WhitelistVisitor::visit(DerefExpr *v, void *arg) {
    v->data->accept(this, arg);
}

void WhitelistVisitor::visit(ITEExpr *v, void *arg) {
    return;
}

void WhitelistVisitor::visit(RegExpr *v, void *arg) {
    return;
}

void WhitelistVisitor::visit(RegNameExpr *v, void *arg) {
    return;
}

void WhitelistVisitor::visit(AddrExpr *v, void *arg) {
    return;
}

void WhitelistVisitor::visit(ImmedExpr *v, void *arg) {
    return;
}
