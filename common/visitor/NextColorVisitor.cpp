#include "Visitor.hh"
#include "DepVisitor.hh"
#include "../sym_tree/References.hh"
#include "../sym_tree/sym_tree.hh"

DepVisitor::DepVisitor() {
    colorMap = new map<Edge *, unsigned long>();
    nextColor = 1;
}

DepVisitor::~DepVisitor() {
}

void *DepVisitor::visit(MemRefExpr *v) {
    if (v->base != NULL)
        v->base->accept(this, arg);
    if (v->index != NULL)
        v->index->accept(this, arg);
}

void *DepVisitor::visit(OperExpr *v) {
    for (int i = 0; i < v->opnd_amt; i++)
        v->opnds[i]->accept(this, arg);
}

void *DepVisitor::visit(Edge *v) {

    v->data->accept(this, arg);
}

void *DepVisitor::visit(Expression *v) {
    v->accept(this, arg);
}

void *DepVisitor::visit(DerefExpr *v) {
    v->data->accept(this, arg);
}

void *DepVisitor::visit(ITEExpr *v) {
    return NULL;
}

void *DepVisitor::visit(RegExpr *v) {
    return NULL;
}

void *DepVisitor::visit(RegNameExpr *v) {
    return NULL;
}

void *DepVisitor::visit(AddrExpr *v) {
    return NULL;
}

void *DepVisitor::visit(ImmedExpr *v) {
    return NULL;
}

void DepVisitor::init(Edge *) {
    currColor = nextColor++;
    visited.clear();
}
