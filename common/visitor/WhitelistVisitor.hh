#ifndef WHITELISTVISITOR_HH
#define WHITELISTVISITOR_HH

#include "Visitor.hh"

#include <map>
#include <set>
#include <list>
#include <stdint.h>

#include "dr_api.h"

using ::std::map;
using ::std::set;
using ::std::list;

struct WhitelistVisitor : public ArguVisitor {
    set<unsigned long> *whitelist;

    WhitelistVisitor();
    ~WhitelistVisitor();
    virtual void visit(Expression *, void *);
    virtual void visit(AddrExpr *, void *);
    virtual void visit(DerefExpr *, void *);
    virtual void visit(ImmedExpr *, void *);
    virtual void visit(ITEExpr *, void *);
    virtual void visit(MemRefExpr *, void *);
    virtual void visit(OperExpr *, void *);
    virtual void visit(RegExpr *, void *);
    virtual void visit(RegNameExpr *, void *);
    virtual void visit(Edge *, void *);
};

#endif
