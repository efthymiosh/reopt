#ifndef VISITOR_HH
#define VISITOR_HH

#include <map>
#include <set>
#include <list>
#include <stdint.h>

#include "dr_api.h"

using ::std::map;
using ::std::set;
using ::std::list;

class Expression;
class OperExpr;
class RegExpr;
class RegNameExpr;
class FlagsBitExpr;
class ImmedExpr;
class MemRefExpr;
class AddrExpr;
class ITEExpr;
class DerefExpr;
struct Edge;

struct Visitor {
    virtual void visit(Expression *) = 0;
    virtual void visit(AddrExpr *) = 0;
    virtual void visit(DerefExpr *) = 0;
    virtual void visit(ImmedExpr *) = 0;
    virtual void visit(ITEExpr *) = 0;
    virtual void visit(MemRefExpr *) = 0;
    virtual void visit(OperExpr *) = 0;
    virtual void visit(RegExpr *) = 0;
    virtual void visit(RegNameExpr *) = 0;
    virtual void visit(FlagsBitExpr *) = 0;
    virtual void visit(Edge *) = 0;
};

struct ArguVisitor {
    virtual void visit(Expression *, void *) = 0;
    virtual void visit(AddrExpr *, void *) = 0;
    virtual void visit(DerefExpr *, void *) = 0;
    virtual void visit(ImmedExpr *, void *) = 0;
    virtual void visit(ITEExpr *, void *) = 0;
    virtual void visit(MemRefExpr *, void *) = 0;
    virtual void visit(OperExpr *, void *) = 0;
    virtual void visit(RegExpr *, void *) = 0;
    virtual void visit(RegNameExpr *, void *) = 0;
    virtual void visit(FlagsBitExpr *, void *) = 0;
    virtual void visit(Edge *, void *) = 0;
};

struct RetVisitor {
    virtual void *visit(Expression *) = 0;
    virtual void *visit(AddrExpr *) = 0;
    virtual void *visit(DerefExpr *) = 0;
    virtual void *visit(ImmedExpr *) = 0;
    virtual void *visit(ITEExpr *) = 0;
    virtual void *visit(MemRefExpr *) = 0;
    virtual void *visit(OperExpr *) = 0;
    virtual void *visit(RegExpr *) = 0;
    virtual void *visit(RegNameExpr *) = 0;
    virtual void *visit(FlagsBitExpr *) = 0;
    virtual void *visit(Edge *) = 0;
};

struct RetArguVisitor {
    virtual void *visit(Expression *, void *arg) = 0;
    virtual void *visit(AddrExpr *, void *arg) = 0;
    virtual void *visit(DerefExpr *, void *arg) = 0;
    virtual void *visit(ImmedExpr *, void *arg) = 0;
    virtual void *visit(ITEExpr *, void *arg) = 0;
    virtual void *visit(MemRefExpr *, void *arg) = 0;
    virtual void *visit(OperExpr *, void *arg) = 0;
    virtual void *visit(RegExpr *, void *arg) = 0;
    virtual void *visit(RegNameExpr *, void *arg) = 0;
    virtual void *visit(FlagsBitExpr *, void *arg) = 0;
    virtual void *visit(Edge *, void *arg) = 0;
};

#endif
