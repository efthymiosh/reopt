#include <inttypes.h>
#include <algorithm>

#include "ExecutionContext.hh"
#include "./visitor/CommonSubExprVisitor.hh"
#include "./visitor/DepVisitor.hh"
#include "./visitor/OperCountVisitor.hh"
#include "dr_api.h"

#define MIN_INSTR_AMOUNT 3

using ::std::set;
using ::std::map;
using ::std::vector;
using ::std::list;

void print_register_state(int fid) {
    dr_mcontext_t mctx;
    mctx.size = sizeof(dr_mcontext_t);
    mctx.flags = DR_MC_ALL;
    if (dr_get_mcontext(dr_get_current_drcontext(), &mctx) == false) {
        dr_fprintf(STDERR, "Unable to retrieve context\n");
        return;
    }
    char flags_str[33];
    uint64_t rflags = (uint64_t)mctx.xflags;
    dr_fprintf(
            fid,
            "%%RAX: %lp\n%%RBX: %lp\n%%RCX: %lp\n%%RDX: %lp\n"
                    "%%RDI: %lp\n%%RSI: %lp\n%%RSP: %lp\n%%RBP: %lp\n"
                    "%%R8 : %lp\n%%R9 : %lp\n%%R10: %lp\n%%R11: %lp\n"
                    "%%R12: %lp\n%%R13: %lp\n%%R14: %lp\n%%R15: %lp\n",
            (uint64_t)mctx.xax, (uint64_t)mctx.xbx, (uint64_t)mctx.xcx, (uint64_t)mctx.xdx,
            (uint64_t)mctx.xdi, (uint64_t)mctx.xsi, (uint64_t)mctx.xsp, (uint64_t)mctx.xbp,
            (uint64_t)mctx.r8,  (uint64_t)mctx.r9,  (uint64_t)mctx.r10, (uint64_t)mctx.r11,
            (uint64_t)mctx.r12, (uint64_t)mctx.r13, (uint64_t)mctx.r14, (uint64_t)mctx.r15
    );
    flags_str[32] = '\0';
    for(int i = 0; i < 10; i++)
        flags_str[i] = 'x';
    for(int i = 31; i >= 10; --i, rflags >>= 1)
        flags_str[i] = '0' + (rflags & 1);
    flags_str[31 - 1] = flags_str[31 - 3] = flags_str[31 - 5] = flags_str[31 - 15] = 'x';
    dr_fprintf(fid, "EFLAGS: rrrrrrrrrrIVVAVRrN21ODITSZrArPrC\nEFLAGS: %s\n", flags_str);
    return;
}

void ExecutionContext::countMemoryRead(opnd_t src, Expression *src_expr) {
    dr_mcontext_t mctx;
    mctx.size = sizeof(dr_mcontext_t);
    mctx.flags = DR_MC_ALL;
    dr_get_mcontext(dr_get_current_drcontext(), &mctx);
    byte *addr = (byte *) opnd_compute_address(src, &mctx);
    ++mem_reads;
    if ((*addr_aliases)[addr] == NULL)
        (*addr_aliases)[addr] = init_alias_addrstats();
    alias_addrstats_add((*addr_aliases)[addr], src_expr, false);
    return;
}

void ExecutionContext::countMemoryWrite(opnd_t dst, Expression *dst_expr) {
    dr_mcontext_t mctx;
    mctx.size = sizeof(dr_mcontext_t);
    mctx.flags = DR_MC_ALL;
    dr_get_mcontext(dr_get_current_drcontext(), &mctx);
    byte *addr = (byte *) opnd_compute_address(dst, &mctx);
    ++mem_writes;
    if ((*addr_aliases)[addr] == NULL)
        (*addr_aliases)[addr] = init_alias_addrstats();
    alias_addrstats_add((*addr_aliases)[addr], dst_expr, true);
    return;
}

unsigned long ExecutionContext::addTraceInstr(instr_t *instr) {
    instr_t *cloned = instr_clone(dr_get_current_drcontext(), instr);
    trace->push_back(instr);
    return trace->size();
}


void ExecutionContext::initObject(void) {
    int len;
    char logname[256];
    insCount = 0;
    mem_reads = 0;
    mem_writes = 0;
    aliases = 0;
    enabled = true;
    build_instr_fn_map();
    build_addr_aliases_map();
    //build_symbol_map_reg(mctx);
    build_symbol_map_flags();
    build_symbol_map_expr();

    fwfAddrExpr = new FlyweightFactory<AddrExpr>();
    fwfDerefExpr = new FlyweightFactory<DerefExpr>();
    fwfImmedExpr = new FlyweightFactory<ImmedExpr>();
    fwfITEExpr = new FlyweightFactory<ITEExpr>();
    fwfMemRefExpr = new FlyweightFactory<MemRefExpr>();
    fwfOperExpr = new FlyweightFactory<OperExpr>();
    fwfRegExpr = new FlyweightFactory<RegExpr>();
    fwfFlagsBitExpr = new FlyweightFactory<FlagsBitExpr>();
    fwfRegNameExpr = new FlyweightFactory<RegNameExpr>();
    fwfEdge = new FlyweightFactory<Edge>();

    trace = new list<instr_t *>();
    amv = new AddressMapperVisitor();

    /* Open Symbolic Expressions output file */
    len = dr_snprintf(logname, sizeof(logname)/sizeof(logname[0]), OUTPATH("symbolic.out"));
    DR_ASSERT(len > 0);
    sym_f = dr_open_file(logname, DR_FILE_WRITE_OVERWRITE);
    DR_ASSERT(sym_f != INVALID_FILE);

    /* Open Disassembled instructions output file */
    len = dr_snprintf(logname, sizeof(logname)/sizeof(logname[0]), OUTPATH("asm.out"));
    DR_ASSERT(len > 0);
    asm_f = dr_open_file(logname, DR_FILE_WRITE_OVERWRITE);
    DR_ASSERT(asm_f != INVALID_FILE);

    /* Open aliasing statistics output file */
    len = dr_snprintf(logname, sizeof(logname)/sizeof(logname[0]), OUTPATH("aliases.out"));
    DR_ASSERT(len > 0);
    alias_f = dr_open_file(logname, DR_FILE_WRITE_APPEND);
    DR_ASSERT(asm_f != INVALID_FILE);

#ifdef DEBUG_GRAPH
    /* Open Graph output file  */
    len = dr_snprintf(logname, sizeof(logname)/sizeof(logname[0]), OUTPATH("graph.dot"));
    DR_ASSERT(len > 0);
    graph_sym_f = dr_open_file(logname, DR_FILE_WRITE_OVERWRITE);
    DR_ASSERT(asm_f != INVALID_FILE);

    /* Open Graph(Symbolic Expressions only) output file  */
    len = dr_snprintf(logname, sizeof(logname)/sizeof(logname[0]), OUTPATH("graph_expr.dot"));
    DR_ASSERT(len > 0);
    graph_expronly_f = dr_open_file(logname, DR_FILE_WRITE_OVERWRITE);
    DR_ASSERT(asm_f != INVALID_FILE);

    /* Open Graph(RFLAGS bits only) output file  */
    len = dr_snprintf(logname, sizeof(logname)/sizeof(logname[0]), OUTPATH("graph_flags.dot"));
    DR_ASSERT(len > 0);
    graph_flagsonly_f = dr_open_file(logname, DR_FILE_WRITE_OVERWRITE);
    DR_ASSERT(asm_f != INVALID_FILE);
#endif
}

void ExecutionContext::finiObject(void) {
    dr_close_file(asm_f);
    dr_close_file(sym_f);
    dr_close_file(alias_f);
#ifdef DEBUG_GRAPH
    dr_fprintf(graph_sym_f, "\n}\n");
    dr_fprintf(graph_expronly_f, "\n}\n");
    dr_fprintf(graph_flagsonly_f, "\n}\n");
    dr_close_file(graph_sym_f);
    dr_close_file(graph_expronly_f);
    dr_close_file(graph_flagsonly_f);
#endif

    delete instr_fn_map;
}

void ExecutionContext::printInstruction(void* drctx, instr_t* instr) {
    disassemble_with_info(drctx, (byte*)instr_get_app_pc(instr), asm_f, false, false);
}

void ExecutionContext::printTrace(void *drctx) {
    list<instr_t *>::iterator lit;
    for (lit = trace->begin(); lit != trace->end(); lit++) {
        disassemble_with_info(drctx, (byte*)instr_get_app_pc(*lit), STDERR, false, false);
    }
}

static set<Edge *> *visited_edges_set;

bool setAlive(TreeNode<unsigned long> *tn, void *param) {
    map<unsigned long, Annotations> *annots = (map<unsigned long, Annotations> *) param;
    tn->annot = &((*annots)[tn->data]);
    tn->annot->corrNode = tn;
    if (++tn->annot->references > 1)
        return false; //break traversal
#ifdef DEBUG_SYM
    dr_fprintf(STDERR, "Set %lu alive\n", tn->data);
#endif
    if (tn->annot->cse && (*tn->annot->cse > tn->data))
        *tn->annot->cse = tn->data;
    return true;
}

bool countDeps(TreeNode<unsigned long> *tn, void *param) {
    unsigned long *amt = (unsigned long *) param;
    if (tn->annot->references > 1)
        return false;

    ++(*amt);
    return true;
}

bool decreaseDeps(TreeNode<unsigned long> *tn, void *param) {
    if (--tn->annot->references >= 1)
        return false;
    return true;
}

bool increaseDeps(TreeNode<unsigned long> *tn, void *param) {
    map<unsigned long, Annotations> *annots = (map<unsigned long, Annotations> *) param;
    tn->annot = &((*annots)[tn->data]);
    tn->annot->corrNode = tn;
    if (++tn->annot->references > 1)
        return false;
    return true;
}

static bool pairComparator(pair<int, int> *p1, pair<int, int> *p2) {
    return p1->first > p2->first;
}

void ExecutionContext::calcFinalTrace(void *drctx) {
    map<Expression*, Edge*>::iterator expr_iter;
    map<FlagsBitExpr*, Edge*>::iterator flags_iter;
    list<instr_t *>::iterator lit;
    unsigned long init_total = 0;
    for (lit = trace->begin(); lit != trace->end(); lit++)
        ++init_total;
    //Counting the OperExprs
    OperCountVisitor *ocv = new OperCountVisitor();
    expr_iter = final_expr_symbols->begin();
    while (expr_iter != final_expr_symbols->end()) {
        if (expr_iter->second != NULL)
            ocv->visit(expr_iter->second);
        ++expr_iter;
    }
    flags_iter = final_flag_symbols->begin();
    while (flags_iter != final_flag_symbols->end()) {
        if (flags_iter->second != NULL)
            ocv->visit(flags_iter->second);
        ++flags_iter;
    }
    dr_fprintf(sym_f, "%-10lu %-10lu %-10lu ", init_total, ocv->operCount, ocv->movCount);
#ifdef DEBUG_BRANCH
    set<Edge *>::iterator sit = final_jump_symbols->begin();
    while (sit != final_jump_symbols->end()) {
        if (*sit != NULL) {
            unsigned long height = (unsigned long) ocv->visit(*sit);
            //dr_fprintf(STDERR, "%lu ", height);
        }
        ++sit;
    }
    dr_fprintf(sym_f, "%-10lu %-10lu\n", ocv->operCount, ocv->movCount);
    dr_fprintf(STDERR, "====++FREQUENCY++====\n");
    
    vector<pair<int, int>*> vec;
    map<int, unsigned int>::iterator miit = ocv->freqMap->begin();
    for(; miit != ocv->freqMap->end(); miit++)
        vec.push_back(new pair<int, int>(miit->second, miit->first));
    std::sort(vec.begin(), vec.end(), pairComparator);

    vector<pair<int, int>*>::iterator vit = vec.begin();
    for (; vit != vec.end(); vit++)
        dr_printf("%8s: %u\n", get_opcode_string((*vit)->second), (*vit)->first);

    expr_iter = final_expr_symbols->begin();
    dr_fprintf(STDERR, "====++--*FIN*--++====\n");
    dr_fprintf(STDERR, "Symbols     Jump Targets\n%d        %d\n", final_expr_symbols->size(), final_jump_symbols->size());
#endif
}

//void ExecutionContext::calcFinalTrace(void *drctx) {
//    map<Expression*, Edge*>::iterator expr_iter;
//    map<FlagsBitExpr*, Edge*>::iterator flags_iter;
//    map<unsigned long, Annotations> annots;
//    visited_edges_set = new set<Edge *>();
//    DepVisitor *dv = new DepVisitor(&annots);
//
//    expr_iter = final_expr_symbols->begin();
//    while (expr_iter != final_expr_symbols->end()) {
//        RefHolder<unsigned long> *rh = dynamic_cast<RefHolder<unsigned long> *>(expr_iter->first);
//        unsigned long rid = rh->getRefId();
//        if (expr_iter->second != NULL)
//            dv->visit(expr_iter->second);
//        expr_iter++;
//    }
//    flags_iter = final_flag_symbols->begin();
//    while (flags_iter != final_flag_symbols->end()) {
//        FlagsBitExpr *fbe = flags_iter->first;
//        unsigned long rid = fbe->getRefId();
//        if (flags_iter->second != NULL)
//            dv->visit(flags_iter->second);
//        ++flags_iter;
//    }
//
//    expr_iter = final_expr_symbols->begin();
//    while (expr_iter != final_expr_symbols->end()) {
//        RefHolder<unsigned long> *rh = dynamic_cast<RefHolder<unsigned long> *>(expr_iter->first);
//        unsigned long rid = rh->getRefId();
//        if (expr_iter->second) {
//            expr_iter->second->getTree(rid)->applyFunc(setAlive, (void *) &annots);
//        }
//        expr_iter++;
//    }
//    flags_iter = final_flag_symbols->begin();
//    while (flags_iter != final_flag_symbols->end()) {
//        FlagsBitExpr *fbe = flags_iter->first;
//        unsigned long rid = fbe->getRefId();
//        if (flags_iter->second != NULL) {
//            annots[rid].flagSetter = true;
//            flags_iter->second->getTree(rid)->applyFunc(setAlive, (void *) &annots);
//        }
//        ++flags_iter;
//    }
//
//    //CommonSubExprVisitor *csv = new CommonSubExprVisitor(&annots);
//
//    //expr_iter = final_expr_symbols->begin();
//    //while (expr_iter != final_expr_symbols->end()) {
//    //    if (expr_iter->second != NULL)
//    //        csv->init(expr_iter->second);
//    //    expr_iter++;
//    //}
//    //flags_iter = final_flag_symbols->begin();
//    //while (flags_iter != final_flag_symbols->end()) {
//    //    if (flags_iter->second != NULL)
//    //        csv->init(flags_iter->second);
//    //    ++flags_iter;
//    //}
//
//
//    map<unsigned long, Annotations>::reverse_iterator aiter = annots.rbegin();
//    for ( ; aiter != annots.rend(); ++aiter) {
//        Annotations &annot = aiter->second;
//        if (!(annot.references) || annot.flagSetter)
//            continue;
//        if (annot.corrNode)
//            annot.corrNode->applyCalcFunc(increaseDeps, (void *)&annots);
//        if ((annot.cse) && (*annot.cse != aiter->first)) {
//            if (annots[*annot.cse].saved) {
//                unsigned long cnt = 1; //self
//                annot.corrNode->applyFuncChildren(countDeps, (void *) &cnt);
//                if (cnt >= MIN_INSTR_AMOUNT) {
//                    annot.corrNode->applyFuncChildren(decreaseDeps, (void *) &cnt);
//                    annot.load = true;
//                    annots[*annot.cse].saved++;
//                }
//            }
//        }
//    }
//
//
//#if defined(DEBUG_DATA) || defined(DEBUG_SYM)
//    map<unsigned long, Annotations>::iterator mit = annots.begin();
//    while(mit != annots.end()) {
//        dr_fprintf(STDERR, "i%lu : fl%d s%lu l%d c%d || CSE(%p, %lu) refs%lu\n",
//                mit->first,
//                mit->second.flagSetter ? 1 : 0,
//                mit->second.saved,
//                mit->second.load ? 1 : 0,
//                mit->second.calcs ? 1 : 0,
//                mit->second.cse,
//                mit->second.cse == NULL ? 0 : *mit->second.cse,
//                mit->second.references
//                );
//
//        ++mit;
//    }
//
//#endif
//
//    list<instr_t *>::iterator lit;
//    unsigned long i = 1;
//    unsigned long rmed_dead_code = 0, rmed_common_subexpr = 0, added = 0, init_total = 0;
//    for (lit = trace->begin(); lit != trace->end(); lit++, i++) {
//        ++init_total;
//        unsigned long temp;
//        Annotations &a = annots[i];
//        if (a.saved > 1) { // saveable with at least one common subexpression valid for loading.
//#ifdef DEBUG_PRINT_INSTR
//            dr_fprintf(STDERR, "i%-9lu   ", i);
//            disassemble_with_info(drctx, (byte*)instr_get_app_pc(*lit), STDERR, false, false);
//#endif
//            unsigned int dst_amount = instr_num_dsts(*lit);
//            for (int cnt = 0; cnt < dst_amount; cnt++) {
//                ++added;
//#ifdef DEBUG_PRINT_INSTR
//                dr_fprintf(STDERR, "i%-9lu++ mov    ", i);
//                opnd_disassemble(drctx, instr_get_dst(*lit, cnt), STDERR);
//                dr_fprintf(STDERR, ", $((temp%lu + %u))\n", i, cnt * 8);
//#endif
//            }
//        }
//        else if (a.load) { // common subexpression
//            unsigned int dst_amount = instr_num_dsts(*lit);
//            ++rmed_common_subexpr;
//            for (int cnt = 0; cnt < dst_amount; cnt++) {
//                ++added;
//#ifdef DEBUG_PRINT_INSTR
//                dr_fprintf(STDERR, "i%-9lu+- mov    $((temp%lu + %u)), ", i, *a.cse, cnt * 8);
//                opnd_disassemble(drctx, instr_get_dst(*lit, cnt), STDERR);
//                dr_fprintf(STDERR, "\n");
//#endif
//            }
//        }
//        else if (a.flagSetter || a.references) { // has say in final flags state, or is otherwise alive.
//#ifdef DEBUG_PRINT_INSTR
//            dr_fprintf(STDERR, "i%-9lu   ", i);
//            disassemble_with_info(drctx, (byte*)instr_get_app_pc(*lit), STDERR, false, false);
//#endif
//        }
//        else { //removed
//            if (a.cse && annots[*a.cse].references > 0)
//                ++rmed_common_subexpr;
//            else
//                ++rmed_dead_code;
//#ifdef DEBUG_PRINT_INSTR
//            dr_fprintf(STDERR, "i%-9lu-- \x1B[31m", i);
//            disassemble_with_info(drctx, (byte*)instr_get_app_pc(*lit), STDERR, false, false);
//            dr_fprintf(STDERR, "\x1B[0m");
//#endif
//        }
//    }
//
//    //Counting the OperExprs
//    OperCountVisitor *ocv = new OperCountVisitor();
//    expr_iter = final_expr_symbols->begin();
//    while (expr_iter != final_expr_symbols->end()) {
//        if (expr_iter->second != NULL)
//            ocv->visit(expr_iter->second);
//        ++expr_iter;
//    }
//    flags_iter = final_flag_symbols->begin();
//    while (flags_iter != final_flag_symbols->end()) {
//        if (flags_iter->second != NULL)
//            ocv->visit(flags_iter->second);
//        ++flags_iter;
//    }
//
//    //dr_fprintf(sym_f, "INIT       FINAL      ADDED      DEADCODE   COMMON     OperExprCnt MovCnt\n");
//    //dr_fprintf(sym_f, "%-10lu %-10lu %-10lu %-10lu %-12lu %-10lu %-10lu\n", init_total, init_total - rmed_common_subexpr - rmed_dead_code + added, added, rmed_dead_code, rmed_common_subexpr, ocv->operCount, ocv->movCount);
//    //dr_fprintf(sym_f, "INIT   OperExprCnt MovCnt\n");
//    dr_fprintf(sym_f, "%-10lu %-10lu %-10lu\n", init_total, ocv->operCount, ocv->movCount);
//}

void ExecutionContext::calculate_alias_stats() {
    int i = 0;
    map<byte *, alias_addrstats_t *>::iterator mit = (*addr_aliases).begin();
    int tot_disc = 0, tot_maydisc = 0, tot_notdisc = 0;
    int waw = 0, raw = 0, war = 0, rar = 0;
    for (; mit != addr_aliases->end(); mit++) {
        alias_addrstats_t *stats = mit->second;
        tot_disc += stats->discoverable;
        tot_maydisc += stats->maybe_discoverable;
        tot_notdisc += stats->not_discoverable;
        waw += stats->waw;
        rar += stats->rar;
        war += stats->war;
        raw += stats->raw;
    }
    dr_fprintf(alias_f, "\t%d\t\t%d\t\t%d\t\t%d\t%d\t%d\t%d\n\n",
               tot_disc, tot_maydisc, tot_notdisc, rar, waw, raw, war);
    return;
}

Edge *ExecutionContext::currentSymExprForMemRef(Expression *expr, uint size) {
    if ((*final_expr_symbols)[expr] != NULL) {
         return (*final_expr_symbols)[expr];
    }
    else  {
        Edge *derefEdge = fwfEdge->getFWObj(new Edge(EDGE_DEST_ANY, expr));
        DerefExpr *de = fwfDerefExpr->getFWObj(new DerefExpr(derefEdge, expr->size));
        return fwfEdge->getFWObj(new Edge(EDGE_DEST_ANY, de));
    }
}

Edge *ExecutionContext::currentSymExprForReg(opnd_t registerOpnd) {
    RegNameExpr *regName = getRegNameObj(registerOpnd);
    Edge *ed = (*final_expr_symbols)[regName];
    if (ed == NULL) {
        Expression *expr = fwfRegExpr->getFWObj(new RegExpr(regName));
        ed = fwfEdge->getFWObj(new Edge(EDGE_DEST_ANY, expr));
    }
    return ed;
}

Edge *ExecutionContext::currentSymExprForFlag(FlagsBitExpr *fbe) {
    Edge *ed = (*final_flag_symbols)[fbe];
    if (ed == NULL) {
        ed = fwfEdge->getFWObj(new Edge(EDGE_DEST_ANY, fbe));
    }
    return ed;
}

Edge *ExecutionContext::currentSymExprForReg(reg_id_t regg) {
    RegNameExpr *regName = getRegNameObj(regg);
    Edge *ed = (*final_expr_symbols)[regName];
    if (ed == NULL) {
        Expression *expr = fwfRegExpr->getFWObj(new RegExpr(regName));
        ed = fwfEdge->getFWObj(new Edge(EDGE_DEST_INITIAL, expr));
    }
    return ed;
}

RegNameExpr * ExecutionContext::getRegNameObj(opnd_t concrete_opnd) {
    reg_id_t reg = opnd_get_reg(concrete_opnd);
    reg = reg_to_pointer_sized(reg);
    return fwfRegNameExpr->getFWObj(new RegNameExpr(reg));
}

RegNameExpr * ExecutionContext::getRegNameObj(reg_id_t reg) {
    reg = reg_to_pointer_sized(reg);
    return fwfRegNameExpr->getFWObj(new RegNameExpr(reg));
}

MemRefExpr *ExecutionContext::createBaseDisp(opnd_t concrete_opnd, uint size) {
    reg_id_t far_segment = DR_REG_NULL, base, index;
    int disp, scale;
    if (opnd_is_far_base_disp(concrete_opnd))
        far_segment = opnd_get_segment(concrete_opnd);
    base = opnd_get_base(concrete_opnd);
    index = opnd_get_index(concrete_opnd);
    disp = opnd_get_disp(concrete_opnd);
    scale = opnd_get_scale(concrete_opnd);

    Edge *ed;
    Expression *expr_scale = NULL, *expr_disp = NULL;
    Edge *ed_base = NULL, *ed_index = NULL;
    MemRefExpr *ret_expr;
    if (base != DR_REG_NULL) {
        ed_base = currentSymExprForReg(base);
    }
    if (index != DR_REG_NULL)
        ed_index = currentSymExprForReg(index);
    if (disp)
        expr_disp = fwfImmedExpr->getFWObj(new ImmedExpr((int64_t) disp, DEFAULT_DISP_SIZE));
    if (scale) {
        expr_scale = fwfImmedExpr->getFWObj(new ImmedExpr((int64_t) scale, DEFAULT_SCALE_SIZE));
    }
    if (far_segment != DR_REG_NULL)
        ret_expr = fwfMemRefExpr->getFWObj(new MemRefExpr(ed_base, ed_index, expr_disp, expr_scale, size));
    else
        ret_expr = fwfMemRefExpr->getFWObj(new MemRefExpr(far_segment, ed_base, ed_index, expr_disp, expr_scale, size));
    return ret_expr;
}


AddrExpr *ExecutionContext::createAbsAddrOperand(opnd_t concrete_opnd, uint size) {
    byte *memaddr = (byte*) opnd_get_addr(concrete_opnd);
    if (opnd_is_far_memory_reference(concrete_opnd))
        return fwfAddrExpr->getFWObj(new AddrExpr(opnd_get_segment(concrete_opnd), memaddr, size));
    return fwfAddrExpr->getFWObj(new AddrExpr(memaddr, size));
}

#ifdef DEBUG_SYM
void ExecutionContext::printExpressions(void) {
    std::set<Expression *> *initial = new std::set<Expression *>();
    if (final_expr_symbols == NULL) {
        dr_fprintf(STDERR, "Final expression symbols is NULL\n");
        return;
    }
    map<Expression*, Edge*>::iterator expr_iter = final_expr_symbols->begin();
    while (expr_iter != final_expr_symbols->end()) {

        if (expr_iter->second != NULL) {
            dr_fprintf(STDERR, "\x1B[33m");
            expr_iter->first->print(STDERR, 1);
            dr_fprintf(STDERR, "\x1B[0m := ");
            expr_iter->second->print(STDERR);
        }
        else {
            initial->insert(expr_iter->first);
        }
        ++expr_iter;
    }

    std::set<Expression *>::iterator sit = initial->begin();
    dr_fprintf(STDERR, "\x1B[31m");
    for (; sit != initial->end(); sit++) {
        (*sit)->print(STDERR);
    }
    dr_fprintf(STDERR, "\x1B[0m");

    return;
}
#endif

#ifdef DEBUG_GRAPH
void ExecutionContext::printGraph(void) {
    std::set<Expression *> *initial = new std::set<Expression *>();
    if (final_expr_symbols == NULL) {
        dr_fprintf(STDERR, "Final expression symbols is NULL\n");
        return;
    }
    map<Expression*, Edge*>::iterator expr_iter = final_expr_symbols->begin();
    dr_fprintf(graph_sym_f, "digraph symbolic_rel_graph {\n");
    dr_fprintf(graph_expronly_f, "digraph symbolic_rel_graph {\n");
    dr_fprintf(graph_flagsonly_f, "digraph symbolic_rel_graph {\n");
    while (expr_iter != final_expr_symbols->end()) {
        if (expr_iter->second != NULL) {
            expr_iter->first->print_graph_as_final(graph_sym_f);
            expr_iter->second->print_graph(graph_sym_f);
            dr_fprintf(graph_sym_f, "a%pfin->a%p [dir=none]\n", expr_iter->first, expr_iter->second);
            dr_fprintf(graph_expronly_f, "a%pfin->a%p [dir=none]\n", expr_iter->first, expr_iter->second);
        }
        ++expr_iter;
    }

    map<FlagsBitExpr*, Edge*>::iterator flags_iter = final_flag_symbols->begin();
    while (flags_iter != final_flag_symbols->end()) {
        FlagsBitExpr *fbe = flags_iter->first;
        if (flags_iter->second != NULL) {
            fbe->print_graph_as_final(graph_sym_f);
            flags_iter->second->print_graph(graph_sym_f);
            dr_fprintf(graph_sym_f, "a%pfin->a%p [dir=none]\n", fbe, flags_iter->second);
        }
        ++flags_iter;
    }

    return;
}
#endif

void ExecutionContext::build_instr_fn_map(void) {
    instr_fn_map = new map<int, void (*)(InstrData &instrData, instr_t *instr)>();
    (*instr_fn_map)[OP_mov_ld] = &fn_mov_family;
    (*instr_fn_map)[OP_mov_st] = &fn_mov_family;
    (*instr_fn_map)[OP_mov_imm] = &fn_mov_family;
    (*instr_fn_map)[OP_push] = &fn_push;
    (*instr_fn_map)[OP_pop] = &fn_pop;
}

void ExecutionContext::build_symbol_map_reg(dr_mcontext_t mctx) {
    initial_reg_symbols = new map<reg_id_t, int64_t>();
    (*initial_reg_symbols)[DR_REG_RAX] = (uint64_t)mctx.xax;
    (*initial_reg_symbols)[DR_REG_RBX] = (uint64_t)mctx.xbx;
    (*initial_reg_symbols)[DR_REG_RCX] = (uint64_t)mctx.xcx;
    (*initial_reg_symbols)[DR_REG_RDX] = (uint64_t)mctx.xdx;
    (*initial_reg_symbols)[DR_REG_RDI] = (uint64_t)mctx.xdi;
    (*initial_reg_symbols)[DR_REG_RSI] = (uint64_t)mctx.xsi;
    (*initial_reg_symbols)[DR_REG_RSP] = (uint64_t)mctx.xsp;
    (*initial_reg_symbols)[DR_REG_RBP] = (uint64_t)mctx.xbp;
    (*initial_reg_symbols)[DR_REG_R8]  = (uint64_t)mctx.r8;
    (*initial_reg_symbols)[DR_REG_R9]  = (uint64_t)mctx.r9;
    (*initial_reg_symbols)[DR_REG_R10] = (uint64_t)mctx.r10;
    (*initial_reg_symbols)[DR_REG_R11] = (uint64_t)mctx.r11;
    (*initial_reg_symbols)[DR_REG_R12] = (uint64_t)mctx.r12;
    (*initial_reg_symbols)[DR_REG_R13] = (uint64_t)mctx.r13;
    (*initial_reg_symbols)[DR_REG_R14] = (uint64_t)mctx.r14;
    (*initial_reg_symbols)[DR_REG_R15] = (uint64_t)mctx.r15;
}

void ExecutionContext::build_symbol_map_expr(void) {
    final_expr_symbols = new map<Expression*, Edge*>();
#ifdef DEBUG_BRANCH
    final_jump_symbols = new set<Edge*>();
#endif
}

void ExecutionContext::build_addr_aliases_map(void) {
    addr_aliases = new map<byte*, alias_addrstats_t *>();
    dstaddr_aliases = new map<byte*, alias_addrstats_t *>();
}

void ExecutionContext::build_symbol_map_flags() {
    final_flag_symbols = new map<FlagsBitExpr *, Edge*>();

}

void ExecutionContext::add_init_symbol_addr(byte *addr, size_t size) {
    if (!size)
        size = 8;
    if (!dr_safe_read(addr, size, &((*initial_addr_symbols)[addr]), NULL)) {
        dr_fprintf(STDERR, "failed to read %d bytes from memory %p\n", size, addr);
        dr_abort();
    }
}

void ExecutionContext::add_init_symbol_expr(Expression *expr, byte *addr, size_t size) {
    if (!size)
        size = 8;
    if (!dr_safe_read(addr, size, &((*initial_addr_symbols)[addr]), NULL)) {
        dr_fprintf(STDERR, "failed to read %d bytes from memory %p\n", size, addr);
        dr_abort();
    }
    if (!dr_safe_read(addr, size, &((*initial_expr_symbols)[expr]), NULL)) {
        dr_fprintf(STDERR, "failed to read %d bytes from memory %p\n", size, addr);
        dr_abort();
    }
}

void ExecutionContext::addFlagExpr(int flag, unsigned long instr, Edge *ed) {
    FlagsBitExpr *fbe = fwfFlagsBitExpr->getFWObj(new FlagsBitExpr(flag));
    fbe->setRefId(instr);
    ed->insertRef(fbe);
    (*final_flag_symbols)[fbe] = ed;
}

void ExecutionContext::addSymAddrExpr(MemRefExpr *exprMemRef, unsigned long instr, Edge *ed) {
    exprMemRef->setRefId(instr);
    ed->insertRef(exprMemRef);
    (*final_expr_symbols)[exprMemRef] = ed;
}

void ExecutionContext::addSymRegExpr(RegNameExpr *exprReg, unsigned long instr, Edge *ed) {
    exprReg->setRefId(instr);
    ed->insertRef(exprReg);
    (*final_expr_symbols)[exprReg] = ed;
}

void ExecutionContext::addAbsAddrExpr(AddrExpr *exprAddr, unsigned long instr, Edge *ed) {
    exprAddr->setRefId(instr);
    ed->insertRef(exprAddr);
    (*final_expr_symbols)[exprAddr] = ed;
}
