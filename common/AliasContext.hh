#ifndef DYNAMORIO_SAMPLES_ALIASCONTEXT_H
#define DYNAMORIO_SAMPLES_ALIASCONTEXT_H


#include "ExecutionContext.hh"

enum AliasEnum {
    ALIAS = 0,
    NO_ALIAS,
    MAY_ALIAS,
    MAY_ALIAS__UNDER_THRESHOLD,
};

using ::std::set;
using ::std::list;
class AliasContext : public ExecutionContext {
private:
    set<set<Expression *> *> *aliasedExprSet;
    set<Expression *> *aliasedDestinations;
    list<Expression *> *aliasedDestsOrdered;
public:
    virtual void initObject();
    virtual void finiObject();
    virtual void countMemoryWrite(opnd_t, Expression *);
    virtual void calculate_alias_stats();
    virtual Expression *createMemRefFromOpnd(opnd_t concrete_opnd, uint size);
    set<Expression *> *getExprAliases(Expression *expr);
    AliasEnum decideAlias(Expression *expr1, Expression *expr2);

    Expression * currentSymExprForMemRef(Expression *expr, uint size);
};


#endif //DYNAMORIO_SAMPLES_ALIASCONTEXT_H
