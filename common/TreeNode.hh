#ifndef TREENODE_HH
#define TREENODE_HH


#include <set>
#include <map>
#include <vector>

#include "sym_tree/References.hh"

#define TREENODE_INIT_MAXC 2

using ::std::set;
using ::std::vector;
using ::std::map;

struct Annotations;
struct Edge;

template<typename T>
struct TreeNode {
    Edge *corrEd;
    T data;
    Annotations *annot;

    vector<TreeNode<T> *> *cArray;
    vector<TreeNode<T> *> *calcArray;
    unsigned long amtC;
    unsigned long maxC;
    unsigned long amtCalc;
    unsigned long maxCalc;

    TreeNode(Edge *ed) : corrEd(ed), amtC(0), maxC(TREENODE_INIT_MAXC){
        cArray = new vector<TreeNode<T> *>(TREENODE_INIT_MAXC);
        calcArray = new vector<TreeNode<T> *>(TREENODE_INIT_MAXC);

        amtCalc = 0;
        maxCalc = TREENODE_INIT_MAXC;
    }
    ~TreeNode() {}

    void insertData(T tee) {
        data = tee;
    }

    void insertChild(TreeNode<T> *child) {
        if (maxC == amtC)
            (*cArray).resize(maxC = maxC << 1);
        (*cArray)[amtC++] = child;
    }

    void insertCalcChild(TreeNode<T> *child) {
        if (maxCalc == amtCalc)
            (*calcArray).resize(maxCalc = maxCalc << 1);
        (*calcArray)[amtCalc++] = child;
    }

    //can be optimized iterative search + based on
    //order of occurrence (lower numbers will always be preceded by higher)
    bool subTreeContains(T tee) {
        if (data == tee)
            return true;
        for(int i = 0; i < amtC; ++i)
            if ((*cArray)[i]->subTreeContains(tee))
                return true;
        return false;
    }


    void applyFunc(bool (*func)(TreeNode<T> *, void*), void *param) {
        if (!func(this, param))
            return;
        for(int i = 0; i < amtC; ++i)
            if ((*cArray)[i])
                (*cArray)[i]->applyFunc(func, param);
    }

    void applyFuncChildren(bool (*func)(TreeNode<T> *, void*), void *param) {
        for(int i = 0; i < amtC; ++i)
            if ((*cArray)[i])
                (*cArray)[i]->applyFunc(func, param);
    }

    void applyCalcFunc(bool (*func)(TreeNode<T> *, void*), void *param) {
        for(int i = 0; i < amtCalc; ++i) {
            if ((*calcArray)[i]) {
                (*calcArray)[i]->applyFunc(func, param);
                (*calcArray)[i]->applyCalcFunc(func, param);
            }
        }
    }

#if defined(DEBUG_DATA) || defined(DEBUG_SYM)
    void print_debug(file_t fd, void (*dataPrinter)(file_t a, T b)) {
        dataPrinter(fd, data);
        for(int i = 0; i < amtC; ++i)
            if ((*cArray)[i])
                (*cArray)[i]->print_debug(fd, dataPrinter);
    }
#endif
};

#endif
