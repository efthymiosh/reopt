#ifndef PCONSTOFFSETMAP
#define PCONSTOFFSETMAP

#include <map>
#include <inttypes.h>

#include "../sym_tree/sym_tree.hh"
#include "OffsetNode.hh"
#include "../FlyweightFactory.hh"


class OffsetMap {
private:
    map<Expression *, OffsetNode *> offsetMap;
    map<MemRefExpr *, MemRefExpr *> simplestFormMap;
public:
    getSimplestForm(Expression *);
};

#endif
