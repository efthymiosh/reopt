#ifndef PCONSTOFFSETNODE
#define PCONSTOFFSETNODE

#include <inttypes.h>

#include "../sym_tree/sym_tree.hh"
#include "../FlyweightFactory.hh"
#include "../FlyweightObject.hh"

struct OffsetNode : public FlyweightObject<OffsetNode> {
    MemRefExpr *simplestForm;

    set <Expression *> additions;
    set <Expression *> subtractions;
    int64_t offset; 

    OffsetNode();
    void print(int fd);
    void add(OffsetNode *);
    void subtract(OffsetNode *);

    virtual uint64_t getHashCode(void);
    virtual bool same_as(OffsetNode *);
    virtual void absorbShadowData(OffsetNode *);

};

#endif
