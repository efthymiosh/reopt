#include "OffsetNode.hh"

bool OffsetNode::same_as(OffsetNode *otherON) {
    return (additions == otherON->additions)
        && (subtractions == otherON->subtractions)
        && (offset == otherON->offset);
}

void OffsetNode::absorbShadowData(OffsetNode *other) {
}

OffsetNode::OffsetNode() {
    offset = 0;
}
void OffsetNode::print(int fd) {
    set<Expression *>::iterator sit = additions.begin();
    dr_fprintf(fd, "OffsetNode %p:(+:", this);
    for ( ; sit != additions.end(); ++sit)
        (*sit)->print(fd, 1);
    dr_fprintf(fd, ")(-:");
    for (sit = subtractions.begin(); sit != subtractions.end(); ++sit)
        (*sit)->print(fd, 1);
    dr_fprintf(fd, ")(+-: %" PRId64 ")\n", offset);
}
void OffsetNode::add(OffsetNode *other) {
    set<Expression *>::iterator sit = other->additions.begin();
    for ( ; sit != other->additions.end(); sit++) {
        if (subtractions.erase(*sit) == 0)
            additions.insert(*sit);
    }
    sit = other->subtractions.begin();
    for ( ; sit != other->subtractions.end(); sit++) {
        if (additions.erase(*sit) == 0)
            subtractions.insert(*sit);
    }
    offset += other->offset;
}

void OffsetNode::subtract(OffsetNode *other) {
    set<Expression *>::iterator sit = other->additions.begin();
    for ( ; sit != other->additions.end(); sit++) {
        if (additions.erase(*sit) == 0)
            subtractions.insert(*sit);
    }
    sit = other->subtractions.begin();
    for ( ; sit != other->subtractions.end(); sit++) {
        if (subtractions.erase(*sit) == 0)
            additions.insert(*sit);
    }
    offset -= other->offset;
}

uint64_t OffsetNode::getHashCode(void) {
    if (!uid)
        return hash;

    hash = uid;
    if (additions.begin() != additions.end())
        hash = genHash((*additions.begin())->getHashCode(), uid);
    if (subtractions.begin() != subtractions.end())
        hash = genHash((*subtractions.begin())->getHashCode(), hash);
    hash = genHash((uint64_t)offset, hash);

    uid = 0;
    return hash;
}
