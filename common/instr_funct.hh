#ifndef INSTR_FUNCTHH
#define INSTR_FUNCTHH

#include <map>
#include <list>
#include <set>
#include <string>
#include <typeinfo>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

#include "dr_api.h"

#include "sym_tree/sym_tree.hh"
#include "compile_mode.hh"

#define EFLAGS_AMT 11
#define POS_CF 0
#define POS_PF 1
#define POS_AF 2
#define POS_ZF 3
#define POS_SF 4
#define POS_TF 5
#define POS_IF 6
#define POS_DF 7
#define POS_OF 8
#define POS_NT 9
#define POS_RF 10

#define GET_FLAGS_BIT(x, flags) (flags >> ((int)x)) & 1

#define MAX_X86_SRC_DST_OPNDS 4

using ::std::map;
using ::std::list;

extern int starting_block;
extern int ending_block;

struct InstrData {
    Edge *my_edge;
    ExecutionContext *ectx;
    Edge **opnds;
    Edge **dstopnds;
    map<unsigned long, Edge*> memref_rhl_map; //links possible base and index expression to corresponding opnds.
    unsigned long *src_rhl;
    unsigned int flags_usage;
    int instr_num;
    int opcode;
    int src_amt;
    int dst_amt;
};

struct Annotations {
    bool flagSetter;
    unsigned long saved;
    bool load;
    bool calcs;
    unsigned long *cse;
    unsigned long references;
    TreeNode<unsigned long> *corrNode;
    Annotations() : flagSetter(false), load(false), saved(0), calcs(false), references(0), cse(NULL), corrNode(NULL) {}
    ~Annotations() {}
};

/**
 * Returns the name of the appropriate register enum as string
 */
const char *get_reg_name(reg_id_t regg);

/**
 * Returns the name of the appropriate flags_bit enum as string
 */
const char *get_flags_name(int flag);

/**
 * Returns the dynamorio opcode as string
 */
const char *get_opcode_string(int);


/**
 * Functions to be called for the corresponding (DynamoRIO) opcodes.
 */
int fn_common(ExecutionContext *ectx, instr_t *instr);
void fn_mov_family(InstrData &instrData, instr_t *instr);
void fn_push(InstrData &instrData, instr_t *instr);
void fn_pop(InstrData &instrData, instr_t *instr);
void fn_call(InstrData &instrData, instr_t *instr);
void fn_ret(InstrData &instrData, instr_t *instr);
void fn_default(InstrData &instrData, instr_t *instr);

/* HELPER FUNCTIONS */

/**
 * Prints the register state of dr_mcontext_t to a file descriptor
 */
void print_register_state(int);


#endif
