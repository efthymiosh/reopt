#include <list>

#include "sym_tree/References.hh"
#include "instr_funct.hh"
#include "ExecutionContext.hh"

#include "dr_ir_instr.h"

using ::std::map;

enum MemAccess {
    NONE = 0,
    MEM_WRITE = 1,
    MEM_READ = 2
};

static void flagsCheckRead(int eflags_read_fl, int edge_dest_fl, int pos_fl, InstrData &instrData) {
    instrData.opnds[instrData.src_amt + pos_fl] = NULL;
    if (instrData.flags_usage & eflags_read_fl) {
        FlagsBitExpr *fbe = instrData.ectx->fwfFlagsBitExpr->getFWObj(new FlagsBitExpr(edge_dest_fl));
        instrData.src_rhl[instrData.src_amt + pos_fl] = fbe->getRefId();
        instrData.opnds[instrData.src_amt + pos_fl] = instrData.ectx->currentSymExprForFlag(fbe);
    }
}

static void flagsCheckWrite(int eflags_write_fl, int edge_dest_fl, Expression *my_expr, Edge **opnds, InstrData &instrData) {
    if (instrData.flags_usage & eflags_write_fl) {
        Edge *ed = instrData.ectx->fwfEdge->getFWObj(new Edge(edge_dest_fl, my_expr));
        instrData.ectx->addFlagExpr(edge_dest_fl, instrData.instr_num, ed);
        int i;
        for (i = 0; i < instrData.src_amt; ++i)
            ed->linkDependency(opnds[i], instrData.instr_num, instrData.src_rhl[i]);
        for ( ; i < instrData.src_amt + EFLAGS_AMT; ++i) {
            if (opnds[i]) {
                ed->linkDependency(opnds[i], instrData.instr_num, instrData.src_rhl[i]);
                map<unsigned long, Edge *>::iterator eit;
                for (eit = instrData.memref_rhl_map.begin(); eit != instrData.memref_rhl_map.end(); ++eit)
                    ed->linkOpndCalcDependency(eit->second, instrData.instr_num, eit->first);
            }
        }
    }
}


/**
 * Refer to Intel Software Developer Manual, Vol 1. Section 3.4.1 for destination operand sizes.
 */
int fn_common(ExecutionContext *ectx, instr_t *instr) {
    MemAccess hasMemRef = NONE;
    int memAccessCtr = -1;
    int i;
    InstrData instrData;
    instrData.src_amt = instr_num_srcs(instr);
    instrData.dst_amt = instr_num_dsts(instr);
    instrData.opcode = instr_get_opcode(instr);
    instrData.instr_num = ectx->addTraceInstr(instr);
    instrData.ectx = ectx;

#ifdef DEBUG_PRINT_INSTR
    dr_fprintf(STDERR, "i%-9lu: ", instrData.instr_num);
    disassemble_with_info(dr_get_current_drcontext(), (byte*)instr_get_app_pc(instr), STDERR, false, false);
#endif
#ifdef DEBUG_PRINT_STATE
    print_register_state(STDERR);
#endif

    /* if the instruction changes context, break instrumentation */
    if ((instrData.opcode == OP_int) || (instrData.opcode == OP_syscall) || (instrData.opcode == OP_sysenter))
        return 0;

    instrData.opnds = new Edge*[instrData.src_amt + EFLAGS_AMT];
    instrData.dstopnds = new Edge*[instrData.dst_amt + EFLAGS_AMT];
    instrData.src_rhl = new unsigned long[instrData.src_amt + EFLAGS_AMT];
    /* source operand handling */
    for (uint i = 0; i < instrData.src_amt; i++) {
        instrData.src_rhl[i] = 0;
        opnd_t src = instr_get_src(instr, i);
        if (opnd_is_reg(src)) { /* is a register */
            instrData.opnds[i] = ectx->currentSymExprForReg(src);
            RefHolder<unsigned long> *rh;
            if ((rh = dynamic_cast<RefHolder<unsigned long> *>(ectx->getRegNameObj(src))) != NULL) {
                instrData.src_rhl[i] = rh->getRefId();
            }
        }
        else if (opnd_is_memory_reference(src)) { /* degenerates to an address */
            uint size = opnd_size_in_bytes(opnd_get_size(src));
            StateNode *expr = NULL;
            if (opnd_is_base_disp(src)) {
                MemRefExpr *mre = ectx->createBaseDisp(src, size);
                expr = (StateNode *) mre;
                RegNameExpr *baseRN, *indexRN;
                if (mre->base != NULL) {
                    baseRN = ectx->getRegNameObj(opnd_get_base(src));
                    if (baseRN != NULL)
                        instrData.memref_rhl_map[baseRN->getRefId()] = ectx->currentSymExprForReg(opnd_get_base(src));
                }
                if (mre->index != NULL) {
                    indexRN = ectx->getRegNameObj(opnd_get_index(src));
                    if (indexRN != NULL)
                        instrData.memref_rhl_map[indexRN->getRefId()] = ectx->currentSymExprForReg(opnd_get_index(src));
                }
            }
            else if (opnd_is_abs_addr(src))
                expr = ectx->createAbsAddrOperand(src, size);
            else if (opnd_is_rel_addr(src))
                expr = ectx->createAbsAddrOperand(src, size); /* not an oversight, dr gives the calculated addr anyway */
            else {
                dr_fprintf(STDERR, "Did not create mem ref from opnd\n");
                dr_abort();
            }

            StateNode *singleForm = ectx->amv->getSingleForm(expr); /*** alias handling ***/

            /* By checking if the instruction reads memory in this manner it is assumed that
             * it indeed reads memory iff all its memory source operands read memory.  */
            if (instr_reads_memory(instr)) {
                instrData.src_rhl[i] = expr->getRefId();
                /*** alias handling ***/
#ifdef DEBUG_SYM
                if (singleForm == NULL) {
                    dr_fprintf(STDERR, "Resolved single memory form NULL, aborting\n");
                    dr_abort();
                }
#endif
                instrData.opnds[i] = ectx->currentSymExprForMemRef(singleForm, instr_memory_reference_size(instr));
                /*** ***** ******** ***/

                hasMemRef = MEM_READ;
                memAccessCtr = i;
            }
            else {
                instrData.opnds[i] = ectx->fwfEdge->getFWObj(new Edge(EDGE_DEST_ANY, singleForm));
                if (expr != singleForm)
                    instrData.opnds[i]->absorbShadowData(ectx->fwfEdge->getFWObj(new Edge(EDGE_DEST_ANY, expr)));
            }
        }
        else if (opnd_is_immed(src)) {
            if (opnd_is_immed_int(src)) {
                Expression *expr = ectx->fwfImmedExpr->getFWObj(new ImmedExpr((int64_t) opnd_get_immed_int(src), opnd_size_in_bytes(opnd_get_size(src))));
                instrData.opnds[i] = ectx->fwfEdge->getFWObj(new Edge(EDGE_DEST_ANY, expr));
            }
            else { //if (opnd_is_immed_float())
                Expression *expr = ectx->fwfImmedExpr->getFWObj(new ImmedExpr(opnd_get_immed_float(src), opnd_size_in_bytes(opnd_get_size(src))));
                instrData.opnds[i] = ectx->fwfEdge->getFWObj(new Edge(EDGE_DEST_ANY, expr));
            }
        }
        else if (opnd_is_pc(src)) {
            Expression *expr = ectx->fwfAddrExpr->getFWObj(new AddrExpr(opnd_get_pc(src), opnd_size_in_bytes(opnd_get_size(src))));
            instrData.opnds[i] = ectx->fwfEdge->getFWObj(new Edge(EDGE_DEST_ANY, expr));
        }
        else {
            disassemble_with_info(dr_get_current_drcontext(), (byte*)instr_get_app_pc(instr), STDERR, false, false);
            dr_abort();
        }

    }


    /* destination operand handling */
    for (i = 0; i < instrData.dst_amt; i++) {
        opnd_t dst = instr_get_dst(instr, i);
        if (opnd_is_reg(dst)) { /* is a register */
            instrData.dstopnds[i] = ectx->fwfEdge->getFWObj(new Edge(i, ectx->getRegNameObj(dst)));
        }
        else if (opnd_is_memory_reference(dst)) { /* degenerates to an address */
            StateNode *expr = NULL;
            uint size = opnd_size_in_bytes(opnd_get_size(dst));
            if (opnd_is_base_disp(dst)) {
                MemRefExpr *mre = ectx->createBaseDisp(dst, size);
                expr = (StateNode *) mre;
                RegNameExpr *baseRN, *indexRN;
                //if it's a memory reference, then it probably reads registers. These cause read dependences.
                if (mre->base != NULL) {
                    baseRN = ectx->getRegNameObj(opnd_get_base(dst));
                    if (baseRN != NULL)
                        instrData.memref_rhl_map[baseRN->getRefId()] = ectx->currentSymExprForReg(opnd_get_base(dst));
                }
                if (mre->index != NULL) {
                    indexRN = ectx->getRegNameObj(opnd_get_index(dst));
                    if (indexRN != NULL)
                        instrData.memref_rhl_map[indexRN->getRefId()] = ectx->currentSymExprForReg(opnd_get_index(dst));
                }
            }
            else if (opnd_is_abs_addr(dst))
                expr = ectx->createAbsAddrOperand(dst, size);
            else if (opnd_is_rel_addr(dst))
                expr = ectx->createAbsAddrOperand(dst, size); /* not an oversight, dr gives the calculated addr anyway */
            else {
                dr_fprintf(STDERR, "Did not create mem ref from opnd\n");
                dr_abort();
            }
            /*** alias handling ***/
            StateNode *singleForm = ectx->amv->getSingleForm(expr);
            /*** ***** ******** ***/
            instrData.dstopnds[i] = ectx->fwfEdge->getFWObj(new Edge(i, singleForm));
            hasMemRef = MEM_WRITE;
            memAccessCtr = i;
            
        }
        else {
            dr_fprintf(STDERR, "Unhandled dst operand type(dst opnd #%d): ", i);
            disassemble_with_info(dr_get_current_drcontext(), (byte*)instr_get_app_pc(instr), STDERR, false, false);
            dr_abort();
        }
    }
    switch (instrData.opcode) {
        case OP_mov_ld:
        case OP_mov_st:
        case OP_mov_imm:
            fn_mov_family(instrData, instr); break;
        case OP_push:
            fn_push(instrData, instr); break;
        case OP_pop:
            fn_pop(instrData, instr); break;
        case OP_call:
            fn_call(instrData, instr); break;
        case OP_ret:
            fn_ret(instrData, instr); break;
#ifdef DEBUG_BRANCH
        case OP_jo_short:
        case OP_jno_short:
        case OP_jb_short:
        case OP_jnb_short:
        case OP_jz_short:
        case OP_jnz_short:
        case OP_jbe_short:
        case OP_jnbe_short:
        case OP_js_short:
        case OP_jns_short:
        case OP_jp_short:
        case OP_jnp_short:
        case OP_jl_short:
        case OP_jnl_short:
        case OP_jle_short:
        case OP_jnle_short:
        case OP_jmp:
        case OP_jmp_short:
        case OP_jmp_ind:
        case OP_jmp_far:
        case OP_jmp_far_ind:
        case OP_jecxz:
        case OP_jo:
        case OP_jno:
        case OP_jb:
        case OP_jnb:
        case OP_jz:
        case OP_jnz:
        case OP_jbe:
        case OP_jnbe:
        case OP_js:
        case OP_jns:
        case OP_jp:
        case OP_jnp:
        case OP_jl:
        case OP_jnl:
        case OP_jle:
        case OP_jnle:
            flagsCheckRead(EFLAGS_READ_CF, EDGE_DEST_CF, POS_CF, instrData);
            flagsCheckRead(EFLAGS_READ_PF, EDGE_DEST_PF, POS_PF, instrData);
            flagsCheckRead(EFLAGS_READ_AF, EDGE_DEST_AF, POS_AF, instrData);
            flagsCheckRead(EFLAGS_READ_ZF, EDGE_DEST_ZF, POS_ZF, instrData);
            flagsCheckRead(EFLAGS_READ_SF, EDGE_DEST_SF, POS_SF, instrData);
            flagsCheckRead(EFLAGS_READ_DF, EDGE_DEST_DF, POS_DF, instrData);
            flagsCheckRead(EFLAGS_READ_OF, EDGE_DEST_OF, POS_OF, instrData);
            flagsCheckRead(EFLAGS_READ_TF, EDGE_DEST_TF, POS_TF, instrData);
            flagsCheckRead(EFLAGS_READ_IF, EDGE_DEST_IF, POS_IF, instrData);
            flagsCheckRead(EFLAGS_READ_NT, EDGE_DEST_NT, POS_NT, instrData);
            flagsCheckRead(EFLAGS_READ_RF, EDGE_DEST_RF, POS_RF, instrData);
            for (i = 0; i < instrData.src_amt; ++i)
                ectx->final_jump_symbols->insert(instrData.opnds[i]);
            for ( ; i < instrData.src_amt + EFLAGS_AMT; ++i)
                if (instrData.opnds[i] != NULL)
                    ectx->final_jump_symbols->insert(instrData.opnds[i]);
            break;
#endif

        default:
            fn_default(instrData, instr);
    }

    return 1;
}

void fn_default(InstrData &instrData, instr_t *instr) {
    Expression *my_expr = NULL;
    OperExpr *oe;
    int i, size = instrData.dst_amt ? instrData.dstopnds[0]->data->get_size() : 0;
    my_expr = oe = instrData.ectx->fwfOperExpr->getFWObj(
            new OperExpr(instrData.opcode, get_opcode_string(instrData.opcode), instrData.opnds, instrData.src_amt, size));

    instrData.flags_usage = instr_get_eflags(instr, DR_QUERY_INCLUDE_ALL);
    /* flags handling */
    flagsCheckRead(EFLAGS_READ_CF, EDGE_DEST_CF, POS_CF, instrData);
    flagsCheckRead(EFLAGS_READ_PF, EDGE_DEST_PF, POS_PF, instrData);
    flagsCheckRead(EFLAGS_READ_AF, EDGE_DEST_AF, POS_AF, instrData);
    flagsCheckRead(EFLAGS_READ_ZF, EDGE_DEST_ZF, POS_ZF, instrData);
    flagsCheckRead(EFLAGS_READ_SF, EDGE_DEST_SF, POS_SF, instrData);
    flagsCheckRead(EFLAGS_READ_DF, EDGE_DEST_DF, POS_DF, instrData);
    flagsCheckRead(EFLAGS_READ_OF, EDGE_DEST_OF, POS_OF, instrData);
    flagsCheckRead(EFLAGS_READ_TF, EDGE_DEST_TF, POS_TF, instrData);
    flagsCheckRead(EFLAGS_READ_IF, EDGE_DEST_IF, POS_IF, instrData);
    flagsCheckRead(EFLAGS_READ_NT, EDGE_DEST_NT, POS_NT, instrData);
    flagsCheckRead(EFLAGS_READ_RF, EDGE_DEST_RF, POS_RF, instrData);

    flagsCheckWrite(EFLAGS_WRITE_CF, EDGE_DEST_CF, my_expr, oe->opnds, instrData);
    flagsCheckWrite(EFLAGS_WRITE_PF, EDGE_DEST_PF, my_expr, oe->opnds, instrData);
    flagsCheckWrite(EFLAGS_WRITE_AF, EDGE_DEST_AF, my_expr, oe->opnds, instrData);
    flagsCheckWrite(EFLAGS_WRITE_ZF, EDGE_DEST_ZF, my_expr, oe->opnds, instrData);
    flagsCheckWrite(EFLAGS_WRITE_SF, EDGE_DEST_SF, my_expr, oe->opnds, instrData);
    flagsCheckWrite(EFLAGS_WRITE_DF, EDGE_DEST_DF, my_expr, oe->opnds, instrData);
    flagsCheckWrite(EFLAGS_WRITE_OF, EDGE_DEST_OF, my_expr, oe->opnds, instrData);
    flagsCheckWrite(EFLAGS_WRITE_TF, EDGE_DEST_TF, my_expr, oe->opnds, instrData);
    flagsCheckWrite(EFLAGS_WRITE_IF, EDGE_DEST_IF, my_expr, oe->opnds, instrData);
    flagsCheckWrite(EFLAGS_WRITE_NT, EDGE_DEST_NT, my_expr, oe->opnds, instrData);
    flagsCheckWrite(EFLAGS_WRITE_RF, EDGE_DEST_RF, my_expr, oe->opnds, instrData);
    map<unsigned long, Edge *>::iterator eit;
    for (i = 0; i < instrData.dst_amt; ++i) {
        Edge *ed = instrData.ectx->fwfEdge->getFWObj(new Edge(i, my_expr));
        instrData.dstopnds[i]->data->add_self(instrData.ectx, instrData.instr_num, ed);
        for (int j = 0; j < instrData.src_amt; ++j)
            ed->linkDependency(oe->opnds[j], instrData.instr_num, instrData.src_rhl[j]); //FIXME: oe->opnds[j] vs opnds[j] SHOULD NOT MATTER.
        for (eit = instrData.memref_rhl_map.begin(); eit != instrData.memref_rhl_map.end(); ++eit)
            ed->linkOpndCalcDependency(eit->second, instrData.instr_num, eit->first);
    }
}

void fn_mov_family(InstrData &instrData, instr_t *instr) {
    Expression *my_expr = instrData.opnds[0]->data;
    map<unsigned long, Edge *>::iterator eit;
    for (int i = 0; i < instrData.dst_amt; ++i) {
        Edge *ed = instrData.ectx->fwfEdge->getFWObj(new Edge(i, my_expr));
        instrData.dstopnds[i]->data->add_self(instrData.ectx, instrData.instr_num, ed);
        for (int j = 0; j < instrData.src_amt; ++j) {
#ifdef DEBUG_SYM
            dr_fprintf(STDERR, "i%lu: Linking movDep from instr %lu\n", instrData.instr_num, instrData.src_rhl[j]);
#endif
            ed->linkMovDependency(instrData.opnds[j], instrData.instr_num, instrData.src_rhl[j]);
        }
        for (eit = instrData.memref_rhl_map.begin(); eit != instrData.memref_rhl_map.end(); ++eit)
            ed->linkOpndCalcDependency(eit->second, instrData.instr_num, eit->first);
    }
}

void fn_push(InstrData &instrData, instr_t *instr) {
    int i;
    map<unsigned long, Edge *>::iterator eit;
    Edge *ed = instrData.opnds[0]; 
    instrData.dstopnds[1]->data->add_self(instrData.ectx, instrData.instr_num, ed);

    ed->linkMovDependency(instrData.opnds[0], instrData.instr_num, instrData.src_rhl[0]);
    for (eit = instrData.memref_rhl_map.begin(); eit != instrData.memref_rhl_map.end(); ++eit)
        ed->linkOpndCalcDependency(eit->second, instrData.instr_num, eit->first);
    instrData.dst_amt = 1;
    instrData.dstopnds[1] = NULL;

    Expression *my_expr;
    OperExpr *oe;
    int size = instrData.dstopnds[0]->data->get_size();
    instrData.opnds[0] = instrData.ectx->fwfEdge->getFWObj(
            new Edge(0, instrData.ectx->fwfImmedExpr->getFWObj(new ImmedExpr((int64_t) -8, 4))));
    my_expr = oe = instrData.ectx->fwfOperExpr->getFWObj(
            new OperExpr(instrData.opcode, get_opcode_string(instrData.opcode), instrData.opnds, instrData.src_amt, size));

    ed = instrData.ectx->fwfEdge->getFWObj(new Edge(0, my_expr));
    instrData.dstopnds[0]->data->add_self(instrData.ectx, instrData.instr_num, ed);

    for (int j = 1; j < instrData.src_amt; ++j)
        ed->linkDependency(oe->opnds[j], instrData.instr_num, instrData.src_rhl[j]); //FIXME: oe->opnds[j] vs opnds[j] SHOULD NOT MATTER.
    for (eit = instrData.memref_rhl_map.begin(); eit != instrData.memref_rhl_map.end(); ++eit)
        ed->linkOpndCalcDependency(eit->second, instrData.instr_num, eit->first);
}

void fn_pop(InstrData &instrData, instr_t *instr) {
    int i;
    Expression *my_expr = instrData.opnds[1]->data;
    map<unsigned long, Edge *>::iterator eit;
    Edge *ed = instrData.ectx->fwfEdge->getFWObj(new Edge(0, my_expr));
    instrData.dstopnds[0]->data->add_self(instrData.ectx, instrData.instr_num, ed);

    ed->linkMovDependency(instrData.opnds[1], instrData.instr_num, instrData.src_rhl[1]);
    for (eit = instrData.memref_rhl_map.begin(); eit != instrData.memref_rhl_map.end(); ++eit)
        ed->linkOpndCalcDependency(eit->second, instrData.instr_num, eit->first);

    OperExpr *oe;
    int size = instrData.dstopnds[1]->data->get_size();
    instrData.opnds[1] = instrData.ectx->fwfEdge->getFWObj(
            new Edge(0, instrData.ectx->fwfImmedExpr->getFWObj(new ImmedExpr((int64_t) 8, 4))));
    my_expr = oe = instrData.ectx->fwfOperExpr->getFWObj(
            new OperExpr(instrData.opcode, get_opcode_string(instrData.opcode), instrData.opnds, instrData.src_amt, size));

    ed = instrData.ectx->fwfEdge->getFWObj(new Edge(0, my_expr));
    instrData.dstopnds[1]->data->add_self(instrData.ectx, instrData.instr_num, ed);
    for (int j = 0; j < instrData.src_amt; ++j)
        ed->linkDependency(oe->opnds[j], instrData.instr_num, instrData.src_rhl[j]); //FIXME: oe->opnds[j] vs opnds[j] SHOULD NOT MATTER.
    for (eit = instrData.memref_rhl_map.begin(); eit != instrData.memref_rhl_map.end(); ++eit)
        ed->linkOpndCalcDependency(eit->second, instrData.instr_num, eit->first);
}

void fn_call(InstrData &instrData, instr_t *instr) {
    int i;
    map<unsigned long, Edge *>::iterator eit;
    Edge *ed = instrData.opnds[0]; 
    instrData.dstopnds[1]->data->add_self(instrData.ectx, instrData.instr_num, ed);

    ed->linkMovDependency(instrData.opnds[0], instrData.instr_num, instrData.src_rhl[0]);
    for (eit = instrData.memref_rhl_map.begin(); eit != instrData.memref_rhl_map.end(); ++eit)
        ed->linkOpndCalcDependency(eit->second, instrData.instr_num, eit->first);
    instrData.dst_amt = 1;
    instrData.dstopnds[1] = NULL;

    Expression *my_expr;
    OperExpr *oe;
    int size = instrData.dstopnds[0]->data->get_size();
    instrData.opnds[0] = instrData.ectx->fwfEdge->getFWObj(
            new Edge(0, instrData.ectx->fwfImmedExpr->getFWObj(new ImmedExpr((int64_t) -8, 4))));
    my_expr = oe = instrData.ectx->fwfOperExpr->getFWObj(
            new OperExpr(instrData.opcode, get_opcode_string(instrData.opcode), instrData.opnds, instrData.src_amt, size));

    ed = instrData.ectx->fwfEdge->getFWObj(new Edge(0, my_expr));
    instrData.dstopnds[0]->data->add_self(instrData.ectx, instrData.instr_num, ed);
    for (int j = 0; j < instrData.src_amt; ++j)
        ed->linkDependency(oe->opnds[j], instrData.instr_num, instrData.src_rhl[j]); //FIXME: oe->opnds[j] vs opnds[j] SHOULD NOT MATTER.
    for (eit = instrData.memref_rhl_map.begin(); eit != instrData.memref_rhl_map.end(); ++eit)
        ed->linkOpndCalcDependency(eit->second, instrData.instr_num, eit->first);
}

void fn_ret(InstrData &instrData, instr_t *instr) {
    int i;
    Expression *my_expr = instrData.opnds[1]->data;
    map<unsigned long, Edge *>::iterator eit;
    Edge *ed;
    OperExpr *oe;
    int size = instrData.dstopnds[0]->data->get_size();
    instrData.opnds[1] = instrData.ectx->fwfEdge->getFWObj(
            new Edge(0, instrData.ectx->fwfImmedExpr->getFWObj(new ImmedExpr((int64_t) 8, 4))));
    my_expr = oe = instrData.ectx->fwfOperExpr->getFWObj(
            new OperExpr(instrData.opcode, get_opcode_string(instrData.opcode), instrData.opnds, instrData.src_amt, size));

    ed = instrData.ectx->fwfEdge->getFWObj(new Edge(0, my_expr));
    instrData.dstopnds[0]->data->add_self(instrData.ectx, instrData.instr_num, ed);
    for (int j = 0; j < instrData.src_amt; ++j)
        ed->linkDependency(oe->opnds[j], instrData.instr_num, instrData.src_rhl[j]); //FIXME: oe->opnds[j] vs opnds[j] SHOULD NOT MATTER.
    for (eit = instrData.memref_rhl_map.begin(); eit != instrData.memref_rhl_map.end(); ++eit)
        ed->linkOpndCalcDependency(eit->second, instrData.instr_num, eit->first);
}
