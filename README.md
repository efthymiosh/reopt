# README #

Dynamic Program Re-optimizer - library implementation for dynamoRIO.

### Contents ###

DynamoRIO client/library source code. The library currently forms a trace between two nop instructions in the client program. The library outputs another, smaller trace, by symbolically executing the initial trace of instructions and eliminating dead code and common subexpressions.

### Building Instructions ###

- Clone repo in folder sm in the samples subfolder of a local DynamoRIO build (e.g. ~/dr/samples/sm)
- Change directory to the samples/ subfolder.
- Append the following lines to CMakeLists.txt:
```
#!cmake
add_sample_client(reopt  "\
./sm/eval_instrument_block.cpp;\
./sm/common/instr_funct.cpp;\
./sm/common/map_manip.cpp;\
./sm/common/helpers.cpp;\
./sm/common/ExecutionContext.cpp;\
./sm/common/visitor/Visitor.cpp;\
./sm/common/visitor/CommonSubExprVisitor.cpp;\
./sm/common/visitor/DepVisitor.cpp;\
./sm/common/visitor/OperCountVisitor.cpp;\
./sm/common/visitor/AddressMapperVisitor.cpp;\
./sm/common/pconst/OffsetNode.cpp;\
./sm/common/sym_tree/sym_tree.cpp;\
./sm/common/sym_tree/Expression.cpp;\
./sm/common/sym_tree/Edge.cpp;\
./sm/common/sym_tree/RegExpr.cpp;\
./sm/common/sym_tree/FlagsBitExpr.cpp;\
./sm/common/sym_tree/RegNameExpr.cpp;\
./sm/common/sym_tree/OperExpr.cpp;\
./sm/common/sym_tree/MemRefExpr.cpp;\
./sm/common/sym_tree/DerefExpr.cpp;\
./sm/common/sym_tree/ImmedExpr.cpp;\
./sm/common/sym_tree/AddrExpr.cpp;\
./sm/common/sym_tree/ITEExpr.cpp;\
"    "")

add_sample_client(test_reopt  "\
./sm/eval_instrument_block_nop.cpp;\
./sm/common/instr_funct.cpp;\
./sm/common/map_manip.cpp;\
./sm/common/helpers.cpp;\
./sm/common/ExecutionContext.cpp;\
./sm/common/visitor/Visitor.cpp;\
./sm/common/visitor/CommonSubExprVisitor.cpp;\
./sm/common/visitor/DepVisitor.cpp;\
./sm/common/visitor/OperCountVisitor.cpp;\
./sm/common/visitor/AddressMapperVisitor.cpp;\
./sm/common/pconst/OffsetNode.cpp;\
./sm/common/sym_tree/sym_tree.cpp;\
./sm/common/sym_tree/Expression.cpp;\
./sm/common/sym_tree/Edge.cpp;\
./sm/common/sym_tree/RegExpr.cpp;\
./sm/common/sym_tree/FlagsBitExpr.cpp;\
./sm/common/sym_tree/RegNameExpr.cpp;\
./sm/common/sym_tree/OperExpr.cpp;\
./sm/common/sym_tree/MemRefExpr.cpp;\
./sm/common/sym_tree/DerefExpr.cpp;\
./sm/common/sym_tree/ImmedExpr.cpp;\
./sm/common/sym_tree/AddrExpr.cpp;\
./sm/common/sym_tree/ITEExpr.cpp;\
"    "")
```
Optionally add these included clients/libraries:
```
#!cmake
add_sample_client(irlift "sm/tools/irlift.cpp;./sm/common/helpers.cpp" "")

add_sample_client(instrcount "./sm/tools/instrcounter.cpp;./sm/common/map_manip.cpp;./sm/common/helpers.cpp"    "")
add_sample_client(instrunused "./sm/tools/instrunused.cpp;./sm/common/map_manip.cpp;./sm/common/helpers.cpp"    "")
```
- If having just cloned, execute:
```
#!bash
cmake .
```
- Build with:
```
#!bash
make reopt
```

- Execute:
```
#!bash
path/to/dr/bin64/drrun -c path/to/dr/samples/bin/libreopt.so X Y -- path/to/program/to/test program arguments
```
where Xth instruction to be executed will be the first instruction to be processed, and the Yth
instruction to be executed will be the last instruction to be processed.
